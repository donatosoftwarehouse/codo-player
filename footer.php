<footer>
    <div class="container top">
        <div class="row">
            <div class="col-md-8">
                <h3 class="no-margin-top">Contact for support</h3>
                <p>Integrate Codo Player into a custom application / monetize videos / tracking / +</p>
                <form name="contactForm" role="form" data-ng-controller="contactCtrl" data-ng-submit="send(contactForm)">
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                <label>Name</label>
                                <input type="text" name="name" size="30" class="form-control" placeholder="Enter name" data-ng-model="name" required />
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <label>Company</label>
                                <input type="text" name="company" class="form-control" placeholder="Enter company" data-ng-model="company" />
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                <label>Email</label>
                                <input type="email" name="email" size="60" class="form-control" placeholder="Enter email" data-ng-model="email" required />
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <label>The sum of 11 and 1</label>
                                <input type="number" name="spam" size="11" maxlength="2" class="form-control" placeholder="11 + 1 = ?" data-ng-model="spam" required />
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Message</label>
                                <textarea name="message" class="form-control" placeholder="Enter message" rows="6" data-ng-model="message" required></textarea>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success" data-ng-disabled="loading">Send</button>
                    <small class="output" data-ng-bind="output"></small>
                </form>
            </div>
            <div class="col-md-2 text-right">
                <h3 class="no-margin-top">Links</h3>
                <ul class="links">

                    <li><a href="/download/">Download</a></li>

                    <li><a href="/account/register/">Register</a></li>
                    <li><a href="/account/login/">Login</a></li>

                    <li><a href="/designer/">Designer</a></li>
                    <li><a href="/plugins/">Plugins</a></li>

                    <li><a href="/support/">Support</a></li>
                    <li><a href="/forum/" target="_blank">Forum</a></li>
                </ul>
            </div>
            <div class="col-md-2 text-right">
                <h3 class="no-margin-top">Follow</h3>
                <ul class="follow">
                    <li><a href="https://www.facebook.com/pages/Codo-Player/244766252323397" target="_blank" class="el-icon-facebook"></a></li>
                    <li><a href="https://plus.google.com/+Codoplayer" target="_blank" class="el-icon-googleplus"></a></li>
                    <li class="no-margin-bottom"><a href="https://twitter.com/codoplayer" target="_blank" class="el-icon-twitter"></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Copyright &copy; 2012 - <?php date_default_timezone_set("Europe/London"); echo date("Y"); ?> <a href="http://www.donatosoftwarehouse.com/" target="_blank">Donato Software House</a></p>
                </div>
                <div class="col-md-6">
                    <p class="pull-right"><a href="/policy/">Terms of Service</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>