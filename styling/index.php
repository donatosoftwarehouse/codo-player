<?php
	$nav = 'styling';

	session_start();

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}
	if(isset($user)) {
		include '../database/database.php';
		$database = database();
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />

	<title>Styling | Codo Player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Design player look via the standard CSS">
	<meta name="copyright" content="Donato Software House" />
	<meta property="og:title" content="Codo Player - A Prime HTML5 FLASH Web Video Player" />
    <meta property="og:image" content="http://codoplayer.com/res/img/codo-player-logo-dark-bg.jpg?v=2" />
    <meta property="og:url" content="http://codoplayer.com/"/>
    <meta property="og:description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!"/>

	<link rel="canonical" href="http://www.codoplayer.com/">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../res/lib/prettify/sunburst.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/common.css" type="text/css" />
    <link rel="stylesheet" href="res/css/base.css" type="text/css" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <?php include_once("../analytics.php") ?>

	<?php include("../header.php"); ?>

    <div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Styling</h1>

                </div>
            </div>
        </div>
    </div>

    <div class="content-1">
        <div class="container">

            <div class="row">
                <div class="col-md-12">

                    <h3 class="no-margin-top">Edit style</h3>

                    <p>Design player look via the standard CSS.</p>

                    <p>Codo Player comes in <strong>"standard"</strong> style by default, located under <strong>"CodoPlayer/styles/"</strong> folder.</p>
                    <p>Stylesheet is easy readable, code is well structured, all selector names are self explanatory.</p>

                    <!-- <h5>To change the buttons color (buttons play, pause, volume, fullscreen)</h5>

<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script>
    CodoPlayer("video.mp4", {
        width: 600,
        height: 338,
        style: "newStyleFolder",
        controls: {
            foreColor: "white",
            backColor: "#454545",
            bufferColor: "#666",
            progressColor: "red"
        }
    })
&lt;/script></pre> -->

                </div>
            </div>

        </div>
    </div>

    <div class="content-2">
        <div class="container">

            <div class="row">
                <div class="col-md-12">

                    <h3 class="no-margin-top">New style</h3>
                    <p>To create a new style use the "standard" style for a reference.</p>
                    <p>For the best practice make a copy of "standard" folder and use it for a new style.</p>

                    <div class="code-block">
                        <p class="lead">To set the path to a new style</p>

<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script>
    CodoPlayer("video.mp4", {
        style: "newStyleFolder"
    })
&lt;/script></pre>

                        <p class="msg"><i class="glyphicon glyphicon-info-sign"></i>Player will map to CSS stylesheet "style.css" in "newStyleFolder" folder, under "CodoPlayer/styles".</p>
                    </div>

                </div>
            </div>

        </div>
    </div>

	<?php include("../footer.php"); ?>

	<script src="../res/lib/jquery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="../res/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../res/lib/angular/angular.min.js" type="text/javascript"></script>
    <script src="../res/lib/prettify/prettify.js" type="text/javascript"></script>
    <script src="../res/js/common.js" type="text/javascript"></script>
    <script src="res/js/base.js" type="text/javascript"></script>

</body>
</html>