<?php
	$nav = 'account';

	session_start();

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}

	if(isset($user)) {
		include '../database/database.php';
		$database = database();
	} else {
		header("location: login");
		return false;
	}
?>

<!DOCTYPE html>
<html lang="en" ng-app="App">
<head>
	<meta charset="utf-8" />
	<meta name="robots" content="noindex, nofollow">

	<title>Account | Codo Player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../res/lib/prettify/sunburst.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/common.css" type="text/css" />
    <link rel="stylesheet" href="res/css/base.css" type="text/css" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body ng-cloak>

	<?php include_once("../analytics.php") ?>

	<?php include("../header.php"); ?>

	<div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo $user ?></h1>
                    <h2>You are now logged in</h2>

                </div>
            </div>
        </div>
    </div>


    <div ng-controller="PurchaseCtrl" ng-show="basket.purchases.length>0">

	    <div class="content-1">
	        <div class="container">

                 <div class="row">
                    <div class="col-md-12">
                        <h3 class="no-margin-top">Purchase history</h3>

                        <h4>Submit host name and download the setup package (.zip)</h4>
                        <p>Please enter a host name of domain the player is about to operate. For example for domain "http://www.example.com", type in "example.com". License is valid for all subdomains of the specified domain.</p>

						<div class="purchase-item" ng-repeat="purchase in basket.purchases" data-name="{{purchase.product}}">

	                    	<h4 class="lead">{{purchase.product}} {{purchase.version}}</h4>
                        	<p class="muted">Domains: {{purchase.quantity}} | Total: {{purchase.price}} {{purchase.currency}} | Time: {{purchase.time}}</p>

                			<ul class="domain-list form-group">
	                            <li class="row" ng-repeat="domain in purchase.domains.list">

                                    <form name="downloadForm" ng-submit="nameChange($parent.$index, $index)">
    									<div class="input-group">

                                            <span class="input-group-addon">{{$index+1}}</span>

    										<input name="name" type="text" class="form-control" value='{{domain.name}}' ng-disabled='domain.time' ng-model="domain.name" ng-show="domain.time" />

    	                                    <input name="name" type="text" class="form-control" value='{{domain.name}}' ng-disabled='domain.time' placeholder="example.com" ng-model="domain.name" ng-hide="domain.time" ng-change="nameChange($parent.$index, $index)" />

    										<span class="input-group-btn">

                                                <a href="#" class="form-control btn btn-primary" data-toggle="modal" ng-click="download($event, purchase.orderref, domain.name, purchase.product)" ng-show="domain.time">Download</a>

                                                <button type="submit" class="form-control btn btn-primary" data-toggle="modal" data-target="#confirm-{{$parent.$index}}-{{$index}}" ng-hide="domain.time">Download</button>

    										</span>

    									</div>
                                    </form>


                                    <!-- Modal -->
                                    <div class="modal fade" id="confirm-{{$parent.$index}}-{{$index}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title no-margin-top" id="myModalLabel">Confirm domain</h3>
                                                </div>
                                                <div class="modal-body">
                                                    <p ng-show="isUrlCorrect">Would you like to confirm the domain?</p>
                                                    <p class="text-danger" ng-hide="isUrlCorrect">The string is not a valid URL (e.g. example.com)</p>
                                                    <h4 class="no-margin-top">{{domain.name}}</h4>
                                                    <p>Please ensure the domain host name is correct. Once confirmed, it can not be amended.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                                    <button class="btn btn-success" data-dismiss="modal" ng-click="confirm($parent.$index, $index, purchase.orderref, domain.name, purchase.product)" ng-disabled="!isUrlCorrect">Confirm &amp; DOWNLOAD</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

	                            </li>
	                        </ul>

	                        <button class="btn btn-success" ng-click="newBlankDomain($index)" ng-hide="purchase.domains.left == 0">Add new domain</button>

	                    </div>

	        			<h4>How does it work?</h4>
	        			<p>Codo Player logic maintains a level of protection against unlicensed use by matching the pre-set host name of the player to a current operating domain.</p>
	                    <p>By entering a host name and pressing on "Download" button, your submitted value is encoded into a new package of Codo Player Pro. It enables the use of your branding for the provided domain only.</p>
	                    <p>Using a copy of Codo Player Pro on a non matching domain will result in player falling back to the state of free version (Codo Player Free).<p>
	                    <p>All player logic and validation is client side, no request is ever sent to server.</p>
	            	</div>
	            </div>

	        </div>

		</div>
	</div>

	<div class="content-2">
		<div class="container">

			<h3 class="no-margin-top">Buy a license</h3>

			<p>Codo Player Pro comes in 3 packages, offering <span class="bold">1</span>, <span class="bold">5</span> and <span class="bold">10</span> (best value) domain licenses that grant you the use of your branding on player canvas.</p>

            <div class="row">
				<div class="col-md-6">

                    <form name="codo-player-pro" class="paypal-form" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" ng-controller="BuyCtrl">
                    	<div class="form-group">
	                        <input type="hidden" name="cmd" value="_s-xclick">
	                        <input type="hidden" name="hosted_button_id" value="6EB3FNP25HG82">
	                        <table>
								<tr>
									<td>
										<input type="hidden" name="on0" value="Package">
										<h4 class="lead no-margin-top">Codo Player Pro v2.1</h4>
									</td>
								</tr>
								<tr>
									<td>
										<select name="os0" class="form-control">
										    <option value="1 domain">1 domain $95.00 USD</option>
										    <option value="5 domains">5 domains $285.00 USD</option>
										    <option value="10 domains">10 domains $475.00 USD</option>
										</select>
									</td>
								</tr>
							</table>
	                        <input type="hidden" name="currency_code" value="USD">
	                        <input type="hidden" name="custom" value="<?php echo $user; ?>-1">
                        </div>
                        <div class="form-group">
                        	<input type="submit" class="btn btn-success" value="Buy now" ng-disabled="!terms" />
							<label class="terms">
								<input type="checkbox" ng-model="terms">
								<small>I accept the terms of <a href="../policy/license/pro" target="_blank">license agreement</a></small>
							</label>
                        </div>
                       	<img src="res/img/paypal.png">
                        <img alt="" class="pixel" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
                    </form>

                </div>

				<div class="col-md-6">
                    <form name="codo-player-advertising" class="paypal-form" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" ng-controller="BuyCtrl2">
                    	<div class="form-group">
	                        <input type="hidden" name="cmd" value="_s-xclick">
	                        <input type="hidden" name="hosted_button_id" value="A7MVJMYAUDFSS">
	                        <table>
	                        	<tr>
	                        		<td>
	                        			<input type="hidden" name="on0" value="Package">
	                        			<h4 class="lead no-margin-top">Advertising Plugin v2.0</h4>
	                        		</td>
	                        	</tr>
	                        	<tr>
	                        		<td>
	                        			<select name="os0" class="form-control">
				                            <option value="1 domain">1 domain $325.00 USD</option>
				                        </select>
	                        		</td>
	                        	</tr>
	                        </table>
	                        <input type="hidden" name="currency_code" value="USD">
	                        <input type="hidden" name="custom" value="<?php echo $user; ?>-1">
                        </div>
                        <div class="form-group">
                        	<input type="submit" class="btn btn-success" value="Buy now" ng-disabled="!terms" />
                        	<label class="terms">
								<input type="checkbox" ng-model="terms">
								<small>I accept the terms of <a href="../policy/license/advertising/" target="_blank">license agreement</a></small>
							</label>
                        </div>
                        <img src="res/img/paypal.png">
                        <img alt="" class="pixel" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
                    </form>

                </div>
            </div>

            <div class="row">
				<div class="col-md-12">
                    <p class="text-success">After a successful payment, please relogin to your Codo Player account to see the purchase available.</p>

					<h4>Cards accepted</h4>
                    <p>Pay by debit or credit card by selecting <strong>"Pay with a debit or credit card"</strong> option on Paypal checkout page.</p>

                    <h4>Refund</h4>
                    <p>A refund is available within 7 days of purchase. Please contact us on email below by explaining the situation and we will be happy to assist. We recommend testing the setup of Codo Player by using a free version first, this is to ensure the product meets your expectations.</p>
                    <p class="text-info">For assistance, please contact <a href="mailto:support@codoplayer.com">support@codoplayer.com</a>.</p></small>
				</div>
			</div>
		</div>
	</div>

	<div class="content-3" ng-controller="AccountCtrl">
        <div class="container">

			<div class="row">
				<div class="col-md-6">

					<h3 class="no-margin-top">Your Account</h3>

					<div class="form-group">
						User: <strong><?php echo $user; ?></strong>
                    </div>
                    <div class="form-group">
						Email: <strong><?php echo $email ?></strong>
					</div>

                    <div class="form-group">
                	   <h4><button class="btn btn-lg btn-primary" data-toggle="button" ng-click="togglePwdForm()">Change password</button></h4>
                    </div>

                	<form ng-show="pwdForm" ng-submit="send()">
						<div class="form-group">
                            <label>New password</label>
                        	<input class="form-control" name="password" type="text" placeholder="Enter new password" ng-model="password" required>
                        </div>
                        <div class="form-group">
	                        <button type="submit" class="btn btn-sm btn-primary" ng-disabled="changePwdForm.password.$error.required">Change</button>
	                        <small>{{output}}</small>
                        </div>
                    </form>

                    <div class="form-group">
                        <h4><a href="logout" class="btn btn-lg btn-danger">Logout</a></h4>
                    </div>

				</div>
			</div>

		</div>
	</div>

	<?php include("../footer.php"); ?>

	<script src="../res/lib/jquery/jquery-1.11.0.min.js"></script>
    <script src="../res/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="../res/lib/angular/angular.min.js" type="text/javascript"></script>
	<script src="../res/js/common.js" type="text/javascript"></script>
    <script src="res/js/controllers.js" type="text/javascript"></script>

	<?php
		if($user && $ident) {
			echo "<script src='res/js/account.js?user=".$user."&ident=".$ident."' type='text/javascript'></script>";
		}
	?>

</body>
</html>