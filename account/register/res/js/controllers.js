function RegisterCtrl($scope, service, $timeout) {
    function Rnd() {
        return Math.floor(Math.random() * 10);
    }
    $scope.loading = false;
    $scope.var1 = Rnd();
    $scope.var2 = Rnd();
    $scope.send = function(form) {
    	if (form.$invalid) return;
    	$scope.loading = true;
        $scope.output = "Please wait...";
        service.contact("res/php/register/", "POST", {
            user: $scope.user,
            email: $scope.email,
            var1: $scope.var1,
            var2: $scope.var2,
            answer: $scope.answer
        }).then(function(response) {
            $scope.output = response.message;
            $timeout(function() {
                $scope.loading = false;
                $scope.output = null;
                if (!response.success) return;
                $scope.user = null;
                $scope.email = null;
                $scope.var1 = Rnd();
    			$scope.var2 = Rnd();
    			$scope.answer = null;
            }, 5000);
        });
    }
};