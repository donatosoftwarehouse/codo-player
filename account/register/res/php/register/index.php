<?php

include '../../../../../database/database.php';
$database = database();

function doReturn($status, $message) {
    $obj = new stdClass();
    $obj->success = $status;
    $obj->message = $message;
    return json_encode($obj);
}

function getRandomString($len) {
  $length = $len;
  $passwordRandomString = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
  $newPW = "";
  srand();
  for($x=0; $x < $length; $x++)  { $newPW .= substr($passwordRandomString,rand(0,62),1);  }
  return $newPW;
}

//$username long is used on purpose
$username = trim(strip_tags($_REQUEST['user']));
$email = trim(strip_tags($_REQUEST['email']));
$var1 = trim(strip_tags($_REQUEST['var1']));
$var2 = trim(strip_tags($_REQUEST['var2']));
$answer = trim(strip_tags($_REQUEST['answer']));
$syspassword = getRandomString(10);

if(strlen($username) > 50 || strlen($email) > 50 || strlen($var1) > 50 || strlen($var2) > 50 || strlen($answer) > 50 || strlen($syspassword) > 50) {
  echo doReturn(false, "Error. Max 50 characters per input.");
  return false;
}

if($username == '') {
  echo doReturn(false, "Error. Enter username.");
  return false;
}

if(strlen($username) < 6 || strlen($username) > 50){
  echo doReturn(false, "Error. Username too short.");
  return false;
}

if(!preg_match("/^([0-9a-z])+$/i", ($username = trim($username)))){
  echo doReturn(false, "Error. Incorrect username format.");
  return false;
}

$query = "select user from users where user='$username'";
$result = mysql_query($query);
if(mysql_num_rows($result) > 0) {
  echo doReturn(false, "Error. Username ".$username." is already in use.");
  return false;
}

if($email == '') {
  echo doReturn(false, "Error. Enter your email.");
  return false;
}

if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email)){
  echo doReturn(false, "Error. Incorrect email format.");
  return false;
}

$query = "select email from users where email='$email'";
$result = mysql_query($query);
if(mysql_num_rows($result) > 0) {
  echo doReturn(false, "Error. Email ".$email." is already in use.");
  return false;
}

$result = $var1 + $var2;
if($answer != $result) {
  echo doReturn(false, "Error. The sum does not match.");
  return false;
}

$syspasswordMd5 = md5($syspassword);

$to = "";
$subject = "";
$message="";
$headers="";

$to  = $email;

$subject = $username.", your new Codo Player account";

$message = '<html>
<body>
<p style="font-family: Tahoma;">
<span style="font-size: 20px; color:#454545; font-weight: bold;">Dear '.$username.'</span>
<br>
<br>
<span style="font-size: 14px; color:#454545; font-weight: normal;">
Here are the login details for your new Codo Player account:
</span><br><br>

<span style="font-size: 14px; color:#454545; font-style: italian;">
Username: '.$username.'<br/>
Password: <span style="font-weight: bold;">'.$syspassword.'</span> (can change once logged in)
</span>
<br><br>

<span style="font-size: 14px; color:#454545; font-weight: normal;">
  <a href="http://www.codoplayer.com/account/login/">CLICK TO LOGIN</a><br/>
  <span style="color:#666666; font-style: italian;">http://www.codoplayer.com/account/login/</span>
</span>
<br><br>

 <span style="font-size: 14px; color:#454545; font-weight: normal;">
  Codo Player - Quality HTML5 / Flash video player
  <br/><br/>
  <a href="http://www.codoplayer.com/" target="_blank"><img src="http://www.codoplayer.com/res/img/logo.png" /></a>
  </span>
  <br><br>

<span style="font-size: 11px; color:#454545; font-weight: normal;">
You are receiving this email because you registered on <a href="http://www.codoplayer.com/">codoplayer.com</a> website.<br>Please do not reply to this automatic email service, instead, for enquiries and support contact support@codoplayer.com
</span>
<br><br>

</p>

</body>
</html>';

$headers = "From: Codo Player <support@codoplayer.com>\n";
$headers .= "Reply-To: <support@codoplayer.com>\n";
$headers .= "Return-Path: <support@codoplayer.com>\n";
$headers .= "Envelope-from: <support@codoplayer.com>\n";
$headers .= "Content-Type: text/html; charset=UTF-8\n";
$headers .= "MIME-Version: 1.0\n";

if(mail($to, $subject, $message, $headers)) {
  $ident = getRandomString(40);
  $date_created = date("Y-m-d");
  $result = mysql_query("INSERT INTO users (ident, user, password, email, date_created, active) VALUES ('$ident', '$username', '$syspasswordMd5', '$email', '$date_created', '1')");

  define('IN_PHPBB',true);
  $phpbb_root_path = "../../../../../forum/";
  $phpEx = "php";
   include($phpbb_root_path . "common.php");
   include($phpbb_root_path . "includes/functions_user.php");

   $user_row = array(
      "username" => $username,
      "user_password" => $syspasswordMd5,
      "user_email" => $email,
      "group_id" => "2",
      "user_type" => "0"
   );

  $err = user_add($user_row);

  echo doReturn(true, "Registration successful! Please check you email.");
} else {
  echo doReturn(false, "Error! Check your network connection");
}

?>