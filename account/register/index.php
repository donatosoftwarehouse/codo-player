<?php
	$nav = 'register';

	session_start();

	if(isset($_REQUEST['details'])) {
    	$details = trim(strip_tags($_REQUEST['details']));
	}

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}
	if(isset($user)) {
		header("location: ../");
	}
?>

<!DOCTYPE html>
<html lang="en" ng-app="App">
<head>
	<meta charset="utf-8" />

	<title>Register | Codo Player</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Register to buy a license or post to forum">
    <meta name="author" content="Donato Software House">
    <meta property="og:title" content="Codo Player - A Prime HTML5 FLASH Web Video Player" />
    <meta property="og:image" content="http://codoplayer.com/res/img/codo-player-logo-dark-bg.jpg?v=2" />
    <meta property="og:url" content="http://codoplayer.com/"/>
    <meta property="og:description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!"/>

    <link rel="canonical" href="http://www.codoplayer.com/">
    <link rel="shortcut icon" href="../../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../../res/css/common.css" type="text/css" />
    <link rel="stylesheet" href="res/css/base.css" type="text/css" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<?php include_once("../../analytics.php") ?>

	<?php include("../../header.php"); ?>

	<div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Register</h1>
                    <h2>Buy a license or post to forum</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="content-1">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="no-margin-top">Fill out the form</h3>

					<form name="registerForm" role="form" class="register-form" ng-controller="RegisterCtrl" ng-submit="send(registerForm)">
						<div class="form-group">
							<label>Username</label>
							<input name="user" type="text" placeholder="Enter username" class="form-control" ng-model="user" required />
						</div>
						<div class="form-group">
							<label>Email</label>
							<input name="email" type="email" placeholder="Enter email" class="form-control" ng-model="email" required />
						</div>
						<div class="form-group form-inline">
							<label>The sum of {{var1}} and {{var2}}</label><br/>
							<input type="number" size="2" name="math" maxlength="2" class="form-control" placeholder="{{var1}} + {{var2}} = ?" ng-model="answer" required />
						</div>
						<div class="form-group half-margin-top">
							<button class="btn btn-lg btn-success" data-ng-disabled="loading">Register</button>
							<small class="output" data-ng-bind="output"></small>
						</div>
					</form>

					<p>Once registered, a confirmation is sent to the email provided with a new temporary password to login.</p>
                </div>

                <div class="col-md-6 text-center">
                	<h4>Have an account already?</h4>
					<a href="../login">Login</a>
                </div>
            </div>
        </div>
    </div>

	<?php include("../../footer.php"); ?>

	<script src="../../res/lib/jquery/jquery-1.11.0.min.js"></script>
    <script src="../../res/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../res/lib/angular/angular.min.js" type="text/javascript"></script>
    <script src="../../res/js/common.js"></script>
    <script src="res/js/controllers.js" type="text/javascript"></script>

</body>
</html>