define(['angular',
        'jquery'
    ],
    function(angular,
        $) {
        // Force strict coding
        'use strict';

        return angular.module('myApp.controllers.newController', ['myApp.services'])
        /**
         *   Whats New controller
         */
        .controller('NewCtrl', ['$scope',
                'SkyService',
                '$rootScope',
                'NavDomain',
                function($scope,
                         SkyService,
                         $rootScope,
                         NavDomain) {
                    // Scope Propertires
                    $scope.results;
                    $rootScope.$$phase || $rootScope.$apply();

                    // Get scope data
                    SkyService.get('/broadcasts/top/15').then(function() {
                        $scope.results = SkyService.data();
                        //console.log("New Data ",$scope.results);
                    },function(error){
                        NavDomain.navigateNext("/error");
                    });

                }
            ])
    });