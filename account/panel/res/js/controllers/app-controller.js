define(["angular",
        "jquery"
    ],
    function(angular,
        $) {

        "use strict";

        return angular.module("app.controllers.appController", [])

            .controller("AppCtrl", ["$scope",
                "$rootScope",
                function(
                    $scope,
                    $rootScope) {

                    $scope.appTitle = "Sky Share CMS";

                }
            ]);

    });