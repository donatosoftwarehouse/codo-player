require.config({

    // Library paths
    paths: {
        jquery: "../../../../res/lib/jquery/jquery-1.11.0.min",
        angular: "../../../../res/lib/angular/angular.min",
        controllers: "controllers",
        appController: "controllers/app-controller"
    },

    // Root location
    baseUrl: "res/js/",

    // Load sequence
    shim: {
        "angular" : {"exports" : "angular"}
    },

    // Load priority
    priority: [
        "angular"
    ]

});

require([
    "jquery",
    "angular",
    "app"
], function($, angular, app) {

    "use strict";

    $(function() {

        var $html = $("html").removeClass("no-js").addClass('ng-app');
        angular.bootstrap($html, [app.name]);

    });

});