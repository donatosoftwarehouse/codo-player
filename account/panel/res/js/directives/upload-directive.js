/**
 * @author Shane Seward
 *
 * Main application directives.
 * Re-usable components and DOM manipulation
 *
 * It is important to keep all DOM manipulation in Directives
 * and NOT in the Controllers.
 *
 * The 'link()' method fires after the element is rendered.
 * It should be used for DOM manipulation - you are provided with the target element and a list of all attributes
 *
 * The 'compile()' method fires prior to the element being rendered.
 * It can be used to alter the structure of the DOM
 */
define(['angular',
		'services',
		'jquery'
], function(angular,
	services,
	$) {

	// Force strict coding
	'use strict';

	angular.module('myApp.directives', ['myApp.services'])

	/**
	 *  Renders Application version in the text.
	 */
		.directive('skyVersion', ['version',
		function(version) {
			return function(scope, elm, attrs) {
				elm.text(version);
			};
		}
	])

	/**
	 *  Applied to a <li> tag.
	 *  This will dispatch an event notifying the DOM tree all list items have been rendered.
	 */
		.directive('skyFinishRender', function($rootScope) {
		return {
			restrict: 'A', // A stands for attribute
			link: function(scope, element, attr) {
				if (scope.$last === true) {
					$rootScope.$broadcast("lastItemRrendered");
				}
			}
		}
	})



	/**
	 *  Simpe directive that animates back to top of page
	 */
		.directive('skyToTop', function($rootScope) {
		return {
			restrict: 'A',
			scope: {},
			link: function(scope, element, attrs) {
				// Capture DOM element
				var aTag = angular.element(element);
				// Listent to click event on element
				aTag.bind('click', handleClick);

				/**
				 * Handle element click event
				 * Animate page to top
				 * @param e DOM event
				 */

				function handleClick(e) {
					e.preventDefault();
					$rootScope.$broadcast("backToTop");
				}
			}
		};
	})


	/**
	 * Run FB function to render likes and extra stuff
	 */

	.directive('renderFacebook', ['$timeout', function(timer) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var likesArr = [];
				var scrollReady = false;
				scope.$on('ViewportScroll', function() {
					if(scrollReady) {
						ViewportScroll();
					}
				})
				function ViewportScroll() {
					$('.fb-like:in-viewport').each(function() {
						var like = $(this);
						if(likesArr.indexOf(like.attr('id')) == -1) {
							likesArr.push(like.attr('id'));
							FB.XFBML.parse($(this).parent()[0]);
							if($('.comment-wrapper').length) FB.XFBML.parse($('.comment-wrapper')[0]);
							scope.$$phase || scope.$apply();
						}
					})
				};

				timer(function() {
					scrollReady = true;
					ViewportScroll();
				}, 2000)

			}
		};
	}])

	/**
	 * Run FB function to render likes and extra stuff
	 */

	.directive('lazySrc', ['$timeout', function(timer) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {

				var imgElement, src, errorSrc;

				scope.$watch(attrs.lazySrc, function(value) {
					imgElement = angular.element(element);
					src = attrs['lazySrc'];
					errorSrc =  attrs['errorImage'];
					imgElement.attr('src', errorSrc);
				});

				function ViewportScroll() {
					$(element).parent().find('.programme-img:in-viewport').attr('src', src);
				};

				scope.$on('ViewportScroll', function(){
					if(imgElement.attr('src') == src) return;
					ViewportScroll()
				})

				timer(ViewportScroll, 1000);

			}
		};
	}])

	/**
	 * Collapse/Expand content
	 */

	.directive('skyCollapse', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {


				scope.$watch(attrs.skyCollapse, function(value) {
					if (value) {
						jQuery(element).slideUp(500);
					} else {
						jQuery(element).slideDown(500);
					}
				});

			}
		};
	})

	/**
	 * Collapse/Expand content
	 */
		.directive('skyPopover', function() {
			return {
				restrict: 'A',
				link: function(scope, element, attrs) {

					var result;

					scope.$watch('result', function(value) {
						if(scope.result)
						{
							result = scope.$eval('result');
							scope.$watch("result."+attrs.skyPopover, function(value) {
								if (value) {
									var top = $(element).parent().parent().find('.actions').length ? $(element).parent().parent().find('.actions').position().top : $(element).parent().parent().find('.actions-programme').position().top;
									jQuery(element).css('top', top-301);
									jQuery(element).fadeIn(100);
								} else {
									jQuery(element).fadeOut(100);
								}
							});
						}
					});

					// Listent to click event on element
					scope.$on('closeAllPopovers', function() {
						if(result) result[attrs.skyPopover] = false
					})


				}
			};
		})

	/**
	 * Collapse/Expand content
	 */
		.directive('skyPopoverZindex', function() {
			return {
				restrict: 'A',
				link: function(scope, element, attrs) {

					var z;
					function setZindex()
					{
						z = (scope.result.showRecord||scope.result.showShare) ? 2000 : 1000-scope.$index;
						$(element).css("z-index",z);
					}

					scope.$watch('result', function(value) {
						if(scope.result)
						{
							scope.$watch("result.showRecord", function(value) {
								setZindex();
							});
							scope.$watch("result.showShare", function(value) {
								setZindex();
							});
						}
					});

					setZindex();

				}
			};
		})

	/**
	 * Wrap Div content in 'Sky Share' panel
	 */
	.directive('skyPanelContainer', function() {
		return {
			restrict: 'A',
			compile: function( element, attributes, transclude ) {
				var useBottomHighlight = (attributes.skyPanelContainer == 'true') ? true : false;
				var preHtml = '<div class="panel"><div class="panel-top"></div><div class="panel-content"><div class="panel-hl-top"></div><div class="panel-content-inner">';
				var postHtml = '</div></div><div class="panel-bottom"></div>';
				postHtml += (useBottomHighlight) ? '<div class="panel-hl-bottom"></div></div>' : '</div>';
				$(element).html(preHtml + $(element).html() +  postHtml);
			}
		};
	})

	/**
	 * Record popover content only
	 */
	.directive('skyRecordPopoverContent', function() {
		return {
			restrict: 'A',
			templateUrl: '/mobile/partials/record-popover.html'
		};
	})

	/**
	 * Share popover content only
	 */
	.directive('skySharePopoverContent', function() {
		return {
			restrict: 'A',
			templateUrl: '/mobile/partials/share-popover.html'
		};
	})

	/**
	 * Iscroll initiate and manage
	 */
	.directive('ngIscroll', function ()
	{
		return {
			replace: false,
			restrict: 'A',
			link: function (scope, element, attr)
			{
				$(window).scroll(function() {
				    clearTimeout($.data(this, 'scrollTimer'));
				    $.data(this, 'scrollTimer', setTimeout(function() {
				        scope.onScrollEnd();
				    }, 250));
				});

				scope.$on('lastItemRrendered', function(){
					checkScrollIndex();
				});

				// Do we have a scoll index history?
				// Do we yet have a list item rendered to scroll to
				function checkScrollIndex(){
					if(scope.navDomain.scrollIndex>0)
					{
						// Look for item in repeater at scroll history
						// Don't scroll to the you'll love list
						var item = $(element).find('ul:not(.youlove)').find('li[ng-repeat]')[scope.navDomain.scrollIndex];
						// Only jump if have an item
						if(item)
						{
							// Clear the scroll history
							scope.navDomain.scrollIndex = -1;
							setTimeout(function(){
								$('html, body').animate({
							        scrollTop: $(item).offset().top-20
							    }, 1000);
							},500)
						}
					}
				}
			}
		};
	})

	/**
	 * Listen to device orientation change
	 */
	.directive('orientationChange', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {

				// Flag to show orientation has changed
				var orientationchanged = false;
				var hv = 640 * window.screen.height / window.screen.width;

				function effectiveDeviceWidth() {
					var sw = window.screen.width;
					var sh = window.screen.height;

					var deviceWidth;

					if(isPortrait())
					{
						deviceWidth = (sh>sw)?sw:sh;
					}
					else
					{
						deviceWidth = (sw>sh)?sw:sh;
					}
					if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio && (window.chrome===undefined)) {
						deviceWidth = deviceWidth / window.devicePixelRatio;
					}
					return deviceWidth;
				}

				function getScaleW(w)
				{
					var dw = effectiveDeviceWidth();
					var sc = dw/w;
					sc = (sc<=1)?sc:1;
					return sc.toFixed(2);
				}

				function isPortrait()
				{
					return ($(window).width() > $(window).height())? false : true;
				}

				function SetMeta() {
					if(isPortrait())
					{
						scope.isLandscape = false;
						document.querySelector("meta[name=viewport]").setAttribute("content", "target-densityDpi=device-dpi, initial-scale="+getScaleW(640)+", minimum-scale="+getScaleW(640)+", maximum-scale="+getScaleW(640)+", width=640");
					}
					else
					{
						scope.isLandscape = true;
						document.querySelector("meta[name=viewport]").setAttribute("content", "target-densityDpi=device-dpi, initial-scale="+getScaleW(hv)+", minimum-scale="+getScaleW(hv)+", maximum-scale="+getScaleW(hv)+", width=" + hv);
					}
					scope.resized();
				};

				// orientationchange event fired first
				// Use to flag orientation has changed
				$(window).bind("orientationchange", function(){
					orientationchanged = true;
					SetMeta();

					// temp fix until chrome bug is fixed
					setTimeout(function() {
						orientationchanged = false;
					}, 500)
				});
				// Resize change
				// Use for cross-device orientation detection
				$(window).bind("resize", function(){
					// Only if orientation has swapped
					// Not if keyboard has opened
					if(orientationchanged)
					{
						// Reset flag
						orientationchanged = false;
						SetMeta();
					}
				});
				// For some reason iPad requires a landscape viewport initially !?!?
				document.querySelector("meta[name=viewport]").setAttribute("content", "target-densityDpi=device-dpi, initial-scale="+getScaleW(hv)+", minimum-scale="+getScaleW(hv)+", maximum-scale="+getScaleW(hv)+", width=" + hv);

				// Trigger upfront to init
				$(window).trigger('orientationchange');
				$(window).trigger('resize');

				// Android default browser will not register orientation changes during screen 'blur'
				// We need to check on 'focus'
				$(window).bind('focus', function() {
					$(window).trigger('orientationchange');
					$(window).trigger('resize');
				});


			}
		};
	})

	/**
	 * Wrap Div content in a template for a scrolling View with top navigation
	 * @param (boolean) include a back button
	 */
	.directive('skyScrollView', function() {
		return {
			restrict: 'A',
			compile: function( element, attributes, transclude ) {
				var preHtml = '<div id="wrapper" ng-iscroll="wrapper" ng-iscroll-form="true"><div id="scroller"><div class="page-menu-bar"><div class="home-button" on-tap="navDomain.navigate(\'/new\')" ></div><div class="settingsToggle" on-tap="navDomain.navigate(\'/settings\')" ><div class="icon" ng-class="{active:primaryPath == \'settings\'}"></div></div>';
				if(attributes.skyScrollView == 'true') {
					preHtml += '<div class="history-back" on-tap="navDomain.historyBack()" ng-hide="navDomain.history<1"><div class="icon"></div></div>';
				}
				preHtml += '</div><div class="page-content" render-facebook>';
				var postHtml = '</div></div></div>';
				$(element).html(preHtml + $(element).html() +  postHtml);
			}
		};
	})

	/**
	 * Listen to video events
	 */
	.directive('skyVideo', function($rootScope) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {

				var firstClick = true;

				scope.$watch(attrs.skyVideo, function(value) {
					if (value) {
						if(!firstClick) { 
							element[0].play();
							$(element).attr('controls', true);
						}
					}
					else
					{
						if(!firstClick) {
							element[0].pause();
							$(element).attr('controls', false);
						}
					}
				});

				$(element).on('play',function(){
					scope.playing = true;
					firstClick = false;
                    scope.$$phase || scope.$apply();
				})
				$(element).on('ended',function(){
					scope.playing = false;
                    scope.$$phase || scope.$apply();
					scope.stopVideo();
                    scope.share(scope.result)
				})
				$(element).on('pause',function(){
					scope.playing = false;
                    scope.$$phase || scope.$apply();
				})
			}
		};
	})

	.directive('onTap', function () {
		return function (scope, element, attrs) {
			return $(element).bind('click', function(e) {
				scope.$apply(attrs['onTap']);
			})
		};
	})

	/** Preload images **/

	.directive('preloadImages', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				scope.$watch('preloadImages', function() {
					angular.forEach(scope.preloadImages, function(value, key){
						var img = new Image();
						img.src = value.url;
					});
				}, true)
			}
		};
	})

	/** Spinner and Modal **/
	.directive('spinnerLoading', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {

				scope.$on("$routeChangeStart", function(event, next, current) {
					$(element).show();
				});

				scope.$on("$routeChangeSuccess", function(event, next, current) {
					setTimeout(function() {
						$(element).fadeOut();
					}, 1000)
				});

			}
		};
	})

	/** Add a fallback image for image load errors **/
	.directive('errorImage', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var imgElement = angular.element(element);
				imgElement.bind('error', function() {
					scope.$apply(function() {
						imgElement.attr('src', attrs['errorImage']);
					});
				});
			}
		};
	})

	.directive('checkForLogin', function($rootScope, $location) {
		return {
			restrict: 'A', // A stands for attribute
			link: function(scope, element, attr) {
				scope.checkSignIn();
			}
		}
	})

	.directive('bugFixChrome', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var imgElement = angular.element(element);
				$(window).bind('resize', function() {
					if(window.innerHeight < 400) {
						$(element).hide();
					} else {
						$(element).show();
					}
				});
			}
		};
	})

});