define(["angular",
    "appController"],
    function (angular,
              appController)
    {

        "use strict";

        return angular.module("app.controllers", ["app.controllers.appController"]);

    });