/**
 *   @author Donatas Cereska
 *   @date   01-08-2014
 */

define("view", ["services"], function(services) {

    // Array of game types and all data associated
    var gameTypesArr = [];

    return {
        renderGamesView: function(xml) {

            // Data ready, remove the loader
            $(".loader").remove();

            // Splitting functionality for readability
            this.renderMenu(xml);
            this.renderGameTypes(xml);

        },

        renderMenu: function(xml) {

            // Loop through the types and buil the menu
            var typesArr = $(xml).find("gametype");
            $(typesArr).each(function(i, el) {

                var $el = $(el);
                var title = $el.find("name").text();
                var $games = $el.find("item");

                // Build LIs
                $(".categories").append("<li><a href='#" + title + "'>" + title + "</a></li>");

                // Fill up the human readable array to loop easier through the items
                gameTypesArr.push({
                    title: title,
                    games: $games
                });

            });

        },

        renderGameTypes: function(xml) {

            // Fill the type group with belonging games
            $(gameTypesArr).each(function(i, obj) {

                // Each type has H4 title
                var $h4 = $(".games").append("<h3>" + obj.title + "</h3>");

                // And its own UL
                var $ul = $("<ul />");

                // Fill UL with games LI
                $(obj.games).each(function(i, el) {

                    var title = $(el).attr("gname");
                    var imgUrl = $(el).attr("g_img");
                    var link = $(el).attr("g_link");
                    var jpDriver = $(el).attr("jpdriver");

                    // Render game canvas
                    var $li = $("<li />");
                    var $section = $("<section />");
                    var $a = $("<a />", {
                        href: link,
                        name: obj.title
                    });

                    // Append title and image
                    $a.append("<h4>" + title + "</h4>");
                    $a.append("<img src='http:" + imgUrl + "' />");

                    // Add Jackpot value if available
                    if(jpDriver) {
                        var $p = $("<p />").addClass("jackpot");
                        require("services").getGameJackpot(jpDriver, $a, $p);
                    }

                    // At last append all to UL
                    $section.append($a);
                    $li.append($section);
                    $ul.append($li);

                });

                // And finally append UL to a "games" DIV in a sequence
                $(".games").append($ul)

            });

        }

    }

});