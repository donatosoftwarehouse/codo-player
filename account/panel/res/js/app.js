define([
    "angular",
    "controllers",
    "directives"
    ], function(
        angular,
        controllers,
        directives
    ) {

    "use strict";

    return angular.module("app", [
        "app.controllers",
        "app.directives"
    ]);
});