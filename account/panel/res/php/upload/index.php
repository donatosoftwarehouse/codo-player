<?php

include('image_check.php');

if($_SERVER['REQUEST_METHOD'] == "POST") {

    $name = $_FILES['file']['name'];
    $size = $_FILES['file']['size'];
    $tmp = $_FILES['file']['tmp_name'];
    $ext = getExtension($name);

    if(strlen($name) == 0) {
        Output(false, "Please select a file.");
        return false;
    }

    if(!in_array($ext, $valid_formats)) {
        Output(false, "Invalid file format.");
        return false;
    }

    if($size >= (1024*2048)) {
        Output(false, "File size too large. Max 2MB.");
        return false;
    }

    // include('s3_config.php');

    // //Rename image name.
    // $actual_image_name = time().".".$ext;

    // if($s3->putObjectFile($tmp, $bucket , $actual_image_name, S3::ACL_PUBLIC_READ) ) {
    //     $output["success"] = true;
    //     $output["message"] = "S3 Upload Successful.";
    //     $s3file='http://'.$bucket.'.s3.amazonaws.com/'.$actual_image_name;
    //     echo "<img src='$s3file' style='max-width:400px'/><br/>";
    //     echo '<b>S3 File URL:</b>'.$s3file;
    // } else {
    //     $output["message"] = "S3 Upload Fail.";
    // }

}

function Output($success, $message) {

    $success = $success or false;
    $message = $message or "Unknown error";
    $output = array(
        "success" => $success,
        "message" => $message
    );
    echo json_encode($output);

}

?>

