<?php

function getExtension($str) {

    $i = strrpos($str,".");
    if (!$i) { return ""; }

    $l = strlen($str) - $i;
    $ext = substr($str,$i+1,$l);
    return $ext;

}

$valid_formats = array("mp4", "MP4", "webm", "WEBM","jpg", "png", "gif","jpeg","PNG","JPG","JPEG","GIF");

?>