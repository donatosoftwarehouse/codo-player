<?php
	$nav = 'panel';

	session_start();

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}

	if(isset($user)) {
		include '../../database/database.php';
		$database = database();
	} else {
		header("location: login");
		return false;
	}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="robots" content="noindex, nofollow">

	<title>Panel | Codo Player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="shortcut icon" href="../../favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="../../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../../res/lib/prettify/sunburst.css" type="text/css" />
    <link rel="stylesheet" href="../../res/css/common.css" type="text/css" />
    <link rel="stylesheet" href="res/css/base.css" type="text/css" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script data-main="res/js/main" src="../../res/lib/require/require.js"></script>
</head>

<body ng-cloak ng-controller="AppCtrl">

	<!--<?php include_once("../../analytics.php") ?>-->

	<?php include("../../header.php"); ?>

	<div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo $user ?></h1>
                    <h2>Your media basket</h2>

                </div>
            </div>
        </div>
    </div>

    <div class="content-1">
        <div class="container">

             <div class="row">
                <div class="col-md-12">
                    <h3 class="no-margin-top">Upload new</h3>

                    <form name="uploadForm" ng-submit="submit(uploadForm)">
                        <input type="file" name="file" onchange="submit(this.files)" />
                        <input type="submit" value="Upload" />
                    </form>

                    <div class="server-msg"></div>

            	</div>
            </div>

        </div>
	</div>

	<div class="content-2">
		<div class="container">

			<h3 class="no-margin-top">Library</h3>

            <div class="row">
				<div class="col-md-6">

                    <!-- <div ng-view></div> -->

                </div>

				<div class="col-md-6">


                </div>
            </div>

            <div class="row">
				<div class="col-md-12">

				</div>
			</div>
		</div>
	</div>

	<div class="content-3">
        <div class="container">

            <h3 class="no-margin-top">Something else</h3>

			<div class="row">
				<div class="col-md-6">



				</div>
			</div>

		</div>
	</div>

	<!--<?php include("../../footer.php"); ?>-->

	<!-- <script src="../../res/lib/jquery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="../../res/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../res/lib/angular/angular.min.js" type="text/javascript"></script>
	<script src="../../res/js/common.js" type="text/javascript"></script>
    <script src="res/js/controllers.js" type="text/javascript"></script>

	<?php
		if($user && $ident) {
			echo "<script src='res/js/account.js?user=".$user."&ident=".$ident."' type='text/javascript'></script>";
		}
	?> -->

</body>
</html>