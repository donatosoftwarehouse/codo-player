function PurchaseCtrl($scope, $http) {

	$scope.user = GetUser().user;
	$scope.ident = GetUser().ident;

	$scope.isUrlCorrect = false;
	$scope.basket = {};
	$http({method: 'GET', url: 'res/php/getpurchases/?user=' + $scope.user + '&ident=' + $scope.ident}).
	success(function(data, status, headers, config) {
	  	$scope.basket = basket = data;
	}).
	error(function(data, status, headers, config) {});

	$scope.download = function(e, o, d, name) {
		e.preventDefault();
		var pkg;
		if(name == "Codo Player Pro") {
			pkg = "createpkg";
		} else if(name == "Advertising Plugin") {
			pkg = "createadpkg";
		}

		location.href = "res/php/" + pkg + "/?user=" + $scope.user + "&ident=" + $scope.ident + "&orderref=" + o + "&name=" + d;
	}

	$scope.newBlankDomain = function(index) {
		if($scope.basket.purchases[index].domains.left > 0) {
			$scope.basket.purchases[index].domains.left--;
			$scope.basket.purchases[index].domains.list.push({
				name: ""
			});
		}
	}

	$scope.nameChange = function(pIndex, index) {

		$scope.isUrlCorrect = /^(?!:\/\/)([a-zA-Z0-9]+\.)?[a-zA-Z0-9][a-zA-Z0-9-]+\.[a-zA-Z]{2,6}?$/i.test($scope.basket.purchases[pIndex].domains.list[index].name);

	}

	$scope.confirm = function(pIndex, index, o, d, name) {
		var urlEnd = 'user=' + $scope.user + '&ident=' + $scope.ident + "&orderref=" + o + "&name=" + d;
		var pkg;
		if(name == "Codo Player Pro") {
			pkg = "createpkg";
		} else if(name == "Advertising Plugin") {
			pkg = "createadpkg";
		}
		$http({method: 'GET', url: 'res/php/' + pkg + '/addDomain.php?' + urlEnd}).
		success(function(data, status, headers, config) {
			if(data) {
				if(data.split(':')[0] == "success") {
					$scope.basket.purchases[pIndex].domains.list[index].time = "ready";
					window.top.location.href = 'res/php/' + pkg + '/?' + urlEnd;
				}
			}
		})
	}

};

function BuyCtrl($scope) {

	$scope.user = GetUser().user;
	$scope.ident = GetUser().ident;

	var paypalBuyForm = document.forms["codo-player-pro"];
	if(paypalBuyForm) {
		$(paypalBuyForm.os0).change(function() {
			paypalBuyForm.custom.value = $scope.user + "-" + this.options[this.options.selectedIndex].value.split(' ')[0];
		});
	}

};

function BuyCtrl2($scope) {

	$scope.user = GetUser().user;
	$scope.ident = GetUser().ident;

	var paypalBuyForm = document.forms["codo-player-advertising"];
	if(paypalBuyForm) {
		$(paypalBuyForm.os0).change(function() {
			paypalBuyForm.custom.value = $scope.user + "-" + this.options[this.options.selectedIndex].value.split(' ')[0];
		});
	}

};

function AccountCtrl($scope, $http) {

	$scope.user = GetUser().user;
	$scope.ident = GetUser().ident;

	$scope.changeDisabled = false;
	$scope.send = function() {
		$scope.output = "Please wait...";
		$http({method: 'GET', url: 'res/php/changepwd/?user=' + $scope.user + '&ident=' + $scope.ident + '&newPwd=' + $scope.password}).
		success(function(data, status, headers, config) {
			$scope.classN = (data.search('success') > 0) ? 'text-success' : 'text-error';
			$scope.output = data.split(':')[1];
		}).
		error(function(data, status, headers, config) {
			$scope.classN = "text-error";
			$scope.output = "Error. Try again";
		});
	}

	$scope.pwdForm = false;
	$scope.togglePwdForm = function() {
		$scope.pwdForm = $scope.pwdForm ? false : true;
		if(!$scope.pwdForm) {
			$scope.password = '';
			$scope.output = '';
		}
	};
};

function GetUser() {
	var account = {}
	var documentScriptsArr = document.scripts;
	for(var i = 0; i < documentScriptsArr.length; i++) {
		if(documentScriptsArr[i].attributes.src) {
			if(documentScriptsArr[i].attributes.src.value.search("account.js") > -1) {
				var url = documentScriptsArr[i].attributes.src.value;
				account.user = url.split("?")[1].split("&")[0].split("=")[1];
				account.ident = url.split("?")[1].split("&")[1].split("=")[1];
			}
		}
	}
	return account;
}