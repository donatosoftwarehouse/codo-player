<?php

include '../../../../database/database.php';
$database = database();

$user = trim(strip_tags($_REQUEST['user']));
$ident = trim(strip_tags($_REQUEST['ident']));
$orderref = trim(strip_tags($_REQUEST['orderref']));
$name = trim(strip_tags($_REQUEST['name']));

$sql="SELECT * FROM users WHERE user='$user' AND ident='$ident'";
$result=mysql_query($sql);
if(!mysql_num_rows($result)) {
   echo "error:Error";
   return false;
}

function is_valid_domain_name($domain_name) {
    $pieces = explode(".",$domain_name);
    foreach($pieces as $piece) {
      if(!preg_match('/^[a-z\d][a-z\d-]{0,62}$/i', $piece) || preg_match('/-$/', $piece)) {
          return false;
      }
    }
    return true;
}

function LetterToCode($letter) {
  $utf8Character = $letter;
  list(, $ord) = unpack('N', mb_convert_encoding($utf8Character, 'UCS-4BE', 'UTF-8'));
  return $ord;
}

function mb_str_split($string) {
    return preg_split('/(?<!^)(?!$)/u', $string );
}

if(!is_valid_domain_name($name)) {
  echo "error:Please enter a valid domain name";
  return false;
}

while($row = mysql_fetch_array($result))  {

  $version = "2.0";
  $domainCode = "";
  $codeArr = mb_str_split($name);
  foreach(mb_str_split($name) as $val) {
    $domainCode .=LetterToCode($val).",";
  }
  $domainCode = rtrim($domainCode, ",");

  $file_folder = "advertising/";
  $str = file_get_contents($file_folder."codo-player-advertising.js");
  $str = str_replace("\"{{LICENSED_DOMAIN}}\"", $name, $str);
  $str = str_replace("\"{{DOMAIN_MARKER}}\"", $domainCode, $str);
  file_put_contents("temp/codo-player-advertising-".$version."-[".$name."].js", $str);

  if(extension_loaded('zip')) {
    $zip = new ZipArchive();
    $zip_name = "advertising.zip";
    if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE) {
      $error .= "* Sorry ZIP creation failed at this time";
    }
    $zip->addFile("temp/codo-player-advertising-".$version."-[".$name."].js", $file_folder."codo-player-advertising.js");
    $zip->addFile($file_folder."cors.swf");
    $zip->addFile($file_folder."VPAID.swf");
    $zip->addFile($file_folder."/LICENSE.txt");
    $zip->addFile($file_folder."/README.txt");
    $zip->addFile($file_folder."styles/style.css");
    $zip->close();

    if(file_exists($zip_name)) {
      header('Content-type: application/zip');
      header('Content-Disposition: attachment; filename="'.$zip_name.'"');
      readfile($zip_name);
      unlink("temp/codo-player-advertising-".$version."-[".$name."].js");
      unlink($zip_name);
    }
  }

}
?>