<?php
  
  include '../../../../database/database.php'; 
  $database = database();

  $user = trim(strip_tags($_REQUEST['user']));
  $ident = trim(strip_tags($_REQUEST['ident']));

  $arr =array();

  $sql="SELECT * FROM orders WHERE user='$user' AND status='Completed' ORDER BY time DESC";
  $result=mysql_query($sql);
  if($result) {
    $count=mysql_num_rows($result);

    if($count > 0) {

      $arr = array(
        "service" => "getpurchases",
        "username" => $user,
        "purchases" => array()
      );

      while($row = mysql_fetch_array($result))  {
        $purchase = array(
          "orderref" => $row['orderref'],
          "quantity" => $row['quantity'],
          "price" => $row['price'],
          "currency" => $row['currency'],
          "time" => $row['time'],
          "product" => $row['product'],
          "version" => $row['version'],
          "domains" => array(
            "left" => 0,
            "list" => array()
          )
        );

        $orderRef = $purchase["orderref"];

        $sql2="SELECT * FROM domains WHERE user='$user' AND orderref='$orderRef'";
        $result2=mysql_query($sql2);
        $count2=mysql_num_rows($result2);

        if($count > 0) {
          while($row2 = mysql_fetch_array($result2))  {

            $domain = array(
              "name" => $row2['name'],
              "time" => $row2['time']
            );

            array_push($purchase['domains']['list'], $domain);

          }
        }

        if($purchase['quantity'] == 'Unlimited') {
          $purchase['quantity'] = 1000;
        }

        $purchase['domains']['left'] = $purchase['quantity'] - $count2;

        array_push($arr['purchases'], $purchase);

      }
    }
  }

  /* --------- */

  header('Content-Type: application/json; charset=utf8');
  header('Access-Control-Allow-Origin: http://www.codoplayer.com/');
  header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

  echo json_encode($arr);
?>