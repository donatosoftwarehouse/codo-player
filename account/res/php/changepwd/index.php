<?php

include '../../../../database/database.php';
$database = database();

$user = trim(strip_tags($_REQUEST['user']));
$ident = trim(strip_tags($_REQUEST['ident']));
$newPwd = trim(strip_tags($_REQUEST['newPwd']));

if(strlen($newPwd) < 6 || strlen($newPwd) > 50){
  echo "error:Password too short (min 6 letters required)";
  return false;
}

if(!preg_match("/^([0-9a-z])+$/i", ($newPwd = trim($newPwd)))){
  echo "error:Incorrect password format (letters and numbers only)";
  return false;
}

$sql="SELECT * FROM users WHERE user='$user' and ident='$ident'";
$result=mysql_query($sql);
while($row = mysql_fetch_array($result))  {
	$email = $row["email"];
}

$to = "";
$subject = "";
$message="";
$headers="";

$to  = $email;

$subject = "Password changed";

$message = '<html>
<body>
<p style="font-family: Tahoma;">
<span style="font-size: 20px; color:#454545; font-weight: bold;">Dear '.$user.'</span>
<br>
<br>
<span style="font-size: 14px; color:#454545; font-weight: normal;">
You have recently requested a password change.<br/>
Here are a new login details for your Codo Player account:
</span><br><br>

<span style="font-size: 14px; color:#454545; font-style: italian;">
Username: '.$user.'<br/>
New password: <span style="font-weight: bold;">'.$newPwd.'</span>
</span>
<br><br>

<span style="font-size: 14px; color:#454545; font-weight: normal;">
  <a href="http://www.codoplayer.com/account/login/">CLICK TO LOGIN</a><br/>
  <span style="color:#666666; font-style: italian;">http://www.codoplayer.com/account/login/</span>
</span>
<br><br>

<span style="font-size: 14px; color:#454545; font-weight: normal;">
Codo Player - Quality HTML5 / Flash video player
<br/><br/>
<a href="http://www.codoplayer.com/" target="_blank"><img src="http://www.codoplayer.com/res/img/logo.png" /></a>
</span>
<br><br>

<span style="font-size: 11px; color:#454545; font-weight: normal;">
You are receiving this email because you have registered on <a href="http://www.codoplayer.com/">codoplayer.com</a> website.<br>Please do not reply to this automatic email service, instead, for enquiries and support contact support@codoplayer.com
</span>
<br><br>

</p>

</body>
</html>';

$headers = "From: Codo Player <support@codoplayer.com>\n";
$headers .= "Reply-To: <support@codoplayer.com>\n";
$headers .= "Return-Path: <support@codoplayer.com>\n";
$headers .= "Envelope-from: <support@codoplayer.com>\n";
$headers .= "Content-Type: text/html; charset=UTF-8\n";
$headers .= "MIME-Version: 1.0\n";

if(mail($to, $subject, $message, $headers)) {
  $newPwdMd5 = md5($newPwd);
	$result = mysql_query("UPDATE users SET password='$newPwdMd5' WHERE user='$user' and ident='$ident'");

  include '../../../../database/database_forum.php';
  $database2 = database2();
  $result2 = mysql_query("UPDATE phpbb_users SET user_password='$newPwdMd5' WHERE username='$user'");

  echo "success:Password successfully changed (email reminder sent)";
} else {
  echo "error:Failed! Check your network connection";
  return false;
}

?>