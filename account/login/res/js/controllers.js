function LoginRetrieveCtrl($scope, $http) {

	$scope.send = function($event) {
		$event.preventDefault();
		$scope.output = "Please wait...";
		$http({method: 'GET', url: 'res/php/retrievelogin/?name=' + $scope.name + '&email=' + $scope.email + '&company=' + $scope.company + '&message=' + $scope.message}).
		success(function(data, status, headers, config) {
			$scope.classN = (data.search('success') > 0) ? 'text-success' : 'text-error';
			$scope.output = data.split(':')[1];
		}).
		error(function(data, status, headers, config) {
			$scope.classN = "text-error";
			$scope.output = "Error. Try again";
		});
	}

	$scope.retrieveForm = false;
	$scope.toggleForm = function($event) {
		$event.preventDefault();
		$scope.retrieveForm = $scope.retrieveForm ? false : true;
		if(!$scope.retrieveForm) {
			$scope.email = '';
			$scope.output = '';
		}
	}

};