<?php

include '../../../../../database/database.php';
$database = database();

function getRandomString($len) {
  $length = $len;
  $passwordRandomString = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
  $newPW = "";
  srand();
  for($x=0; $x < $length; $x++)  { $newPW .= substr($passwordRandomString,rand(0,62),1);  }
  return $newPW;
}

$email = trim(strip_tags($_REQUEST['email']));

if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email)){
  echo "error:Incorrect email format";
  return false;
}

$sql="SELECT * FROM users WHERE email='$email'";
$result=mysql_query($sql);
$count=mysql_num_rows($result);

if($count == 0) {
  echo "error:Email does not exist";
  return false;
} else {

  while($row = mysql_fetch_array($result))  {
    $user = $row["user"];
    $ident = $row["ident"];
    $email = $row["email"];
  }

  $syspassword = getRandomString(10);

  $to = "";
  $subject = "";
  $message="";
  $headers="";

  $to  = $email;

  $subject = "Password reminder";

  $message = '<html>
  <body>
  <p style="font-family: Tahoma;">
  <span style="font-size: 20px; color:#454545; font-weight: bold;">Dear '.$user.'</span>
  <br>
  <br>
  <span style="font-size: 14px; color:#454545; font-weight: normal;">
  You have recently requested a password reminder.<br/>
  Here are a new login details for your Codo Player account:
  </span><br><br>

  <span style="font-size: 14px; color:#454545; font-style: italian;">
  Username: '.$user.'<br/>
  Temporary password: <span style="font-weight: bold;">'.$syspassword.'</span> (can change once logged in)
  </span>
  <br><br>

  <span style="font-size: 14px; color:#454545; font-weight: normal;">
    <a href="http://www.codoplayer.com/account/login/">CLICK TO LOGIN</a><br/>
    <span style="color:#666666; font-style: italian;">http://www.codoplayer.com/account/login/</span>
  </span>
  <br><br>

  <span style="font-size: 14px; color:#454545; font-weight: normal;">
  Codo Player - Quality HTML5 / Flash video player
  <br/><br/>
  <a href="http://www.codoplayer.com/" target="_blank"><img src="http://www.codoplayer.com/res/img/logo.png" /></a>
  </span>
  <br><br>

  <span style="font-size: 11px; color:#454545; font-weight: normal;">
  You are receiving this email because you have registered on <a href="http://www.codoplayer.com/">codoplayer.com</a> website.<br>Please do not reply to this automatic email service, instead, for enquiries and support contact support@codoplayer.com
  </span>
  <br><br>

  </p>

  </body>
  </html>';

  $headers = "From: Codo Player <support@codoplayer.com>\n";
  $headers .= "Reply-To: <support@codoplayer.com>\n";
  $headers .= "Return-Path: <support@codoplayer.com>\n";
  $headers .= "Envelope-from: <support@codoplayer.com>\n";
  $headers .= "Content-Type: text/html; charset=UTF-8\n";
  $headers .= "MIME-Version: 1.0\n";

  if(mail($to, $subject, $message, $headers)) {
    $syspasswordMd5 = md5($syspassword);
    $result = mysql_query("UPDATE users SET password='$syspasswordMd5' WHERE user='$user' and ident='$ident'");
    echo "success:Reminder successfully sent";
  } else {
    echo "error:Failed! Check your network connection";
    return false;
  }

}

?>