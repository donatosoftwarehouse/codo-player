<?php 
session_start();
session_destroy();
define('IN_PHPBB',true);
$phpbb_root_path = "../../forum/";
$phpEx = "php";
include($phpbb_root_path . "common.php"); 
include($phpbb_root_path . "includes/functions_user.php"); 

// End session management
$user->session_kill();
header("location: ../login");
?>