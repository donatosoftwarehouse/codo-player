<!DOCTYPE html>
<html lang="en" ng-app="App">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Codo Player - A Prime HTML5 FLASH Web Video Player</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!">
        <meta name="author" content="Donato Software House">
        <meta property="og:title" content="Codo Player - A Prime HTML5 FLASH Web Video Player" />
        <meta property="og:image" content="http://codoplayer.com/res/img/codo-player-logo-dark-bg.jpg?v=2" />
        <meta property="og:url" content="http://codoplayer.com/"/>
        <meta property="og:description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!"/>

        <link rel="canonical" href="http://www.codoplayer.com/">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="res/css/elusive-webfont.css" type="text/css" />
        <link rel="stylesheet" href="res/css/common.css" type="text/css" />
        <link rel="stylesheet" href="res/css/base.css" type="text/css" />
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">
                    <br/>
                    <a class="logo" href="/"><img src="res/img/logo.png" alt="Codo Player" /></a>
                    <h5>Codo Player site is temporary down for maintenance.<br/>Please come back later.</h5>
                </div>

            </div>
        </div>

        <script src="res/lib/jquery/jquery-1.11.0.min.js"></script>
        <script src="res/lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>