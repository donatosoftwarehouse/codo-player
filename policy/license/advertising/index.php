<?php
	$nav = 'policy-advertising';

	session_start();

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}
	if(isset($user)) {
		include '../../../database/database.php';
		$database = database();
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="robots" content="noindex, nofollow">

	<title>Codo Player Advertising Plugin license</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="canonical" href="http://www.codoplayer.com/">
    <link rel="shortcut icon" href="../../../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../../../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../../../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../../../res/lib/prettify/sunburst.css" type="text/css" />
    <link rel="stylesheet" href="../../../res/css/common.css" type="text/css" />
</head>

<body>

	<?php include_once("../../../analytics.php") ?>

	<?php include("../../../header.php"); ?>

	<div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Codo Player Advertising Plugin</h1>
                    <h2>Permits advertising with Codo Player</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="content-1">
        <div class="container">

			<div class="row">
				<div class="col-md-12">

					<h3 class="no-margin-top">Software License Agreement</h3>

					<h5>Notice</h5>
					<p>YOU MUST READ AND AGREE TO THE TERMS OF THIS SOFTWARE LICENSE AGREEMENT BEFORE PURCHASING CODO PLAYER ADVERTISING PLUGIN. BY PURCHASING, DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU ARE AGREEING TO BE BOUND BY THE TERMS AND CONDITIONS OF THIS SOFTWARE LICENSE AGREEMENT. IF YOU DO NOT ACCEPT THE TERMS AND CONDITIONS OF THIS AGREEMENT, DO NOT PURCHASE, DOWNLOAD, INSTALL OR USE THIS SOFTWARE.</p>

					<h5>Definitions</h5>
					<p>For the purpose of this agreement, the following terms shall have the following meanings:</p>
					<p>
						<ul>
							<li>1. Licensor – Donato Software House Ltd.</li>
							<li>2. Software – Codo Player Advertising Plugin.</li>
							<li>3. License – Software License Agreement (this document).</li>
							<li>4. Licensee – Software user, exercising rights under this License.</li>
							<li>5. Documentation – A set of instructions on how to use the Software (see codoplayer.com).</li>
							<li>6. Branding – A material forming corporate visual identity (image logo or text).</li>
						</ul>
					</p>

					<h5>Parties</h5>
					<p>This is an agreement between the Licensor and Licensee in which the Licensor grants Licensee certain rights in the Software, subject to the terms and conditions of this agreement. This agreement is entered at the moment of Software license purchase.</p>

					<h5>Witnesseth</h5>
					<p>Licensor is engaged in the business of designing and developing computer software and has created and developed a software package "Codo Player Advertising Plugin" that is intended to serve as a web player for your online media content.</p>

					<h5>Ownership</h5>
					<p>Licensor is the owner of all intellectual property rights of the Software.</p>

					<h5>License Grant</h5>
					<p>Licensor hereby grants Licensee, a non-exclusive, non-assignable and non-transferable right to setup and exercise the use of Software on terms of Documentation, under a host (internet domain name), provided by Licensee, registered with Licensor. No right is being conveyed to Licensee to setup and use the Software under any other host, not registered with Licensor.</p>
					<p>Licensor grants Licensee the right to add custom Branding on Software canvas.</p>

					<p>Licensor grants Licensee the right to adapt the Software by amending the source only if the source code is kept attributed back to Software.</p>

					<p>In case of adaption, security logic implemented into Software must not be affected, altered or removed in any way and must be functional at all times.</p>
					<p>Licensee is prohibited from using the Software in any manner other than as described above.</p>

					<h5>Termination</h5>
					<p>Licensor may terminate this License at any time in the case where:</p>
					<p>
						<ul>
							<li>1. Licensee is in breach of any of this License's terms and conditions.</li>
							<li>2. Licensee entered into any arrangement which caused Licensor to be unable to enforce his rights under this License.</li>
						</ul>
					</p>

					<h5>No warranty</h5>
					<p>There is no warranty for the Software, to the extent permitted by applicable law. The Licensor provides the program "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the Software is with Licensee. Should the Software prove defective, Licensee assume the cost of all necessary servicing, repair or correction.</p>

					<h5>Limitation of Liability</h5>
					<p>In no event unless required by applicable law will the Licensor be liable to Licensee for damages, including any general, special, incidental or consequential, indirect, punitive damages arising out of the use or inability to use the Software (including but not limited to loss of data or data being rendered inaccurate or losses sustained by Licensee or third parties including third party claims of infringement of intellectual property rights, copyright or trademark).</p>

					<h5>Indemnity</h5>
					<p>Licensee hereby warrants to indemnify Licensor, its officers, directors, agents, and employees for all damages, expenses, losses and costs (including attorney fees) as well as for any third-party claim for infringement of its intellectual property rights incurred in regards to Licensee’s use of the Software.</p>

					<h5>Severability</h5>
					<p>If any provision hereof is held invalid or unenforceable by a court of competent jurisdiction, such invalidity shall not affect the validity or operation of any other provision and such invalid provision shall be deemed to be severed from the Agreement.</p>

					<h5>Complete Agreement</h5>
					<p>This License constitutes the entire agreement between the parties in regards of use of the Software. No one other than Licensor has the right to change or modify this License.</p>

					<h5>Governing Law, Jurisdiction</h5>
					<p>This License shall be governed by the laws of United Kingdom without regard to principles of conflict of laws.</p>

					<h5>Waiver</h5>
					<p>No waiver shall be deemed as a waiver of any prior or subsequent default of the same or other provisions of this Agreement.</p>

					<p>END OF SOFTWARE LICENSE AGREEMENT</p>

				</div>
			</div>

		</div>
	</div>

	<?php include("../../../footer.php"); ?>

</body>
</html>