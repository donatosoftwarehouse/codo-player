<?php
	$nav = 'policy';

	session_start();

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}
	if(isset($user)) {
		include '../database/database.php';
		$database = database();
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="robots" content="noindex, nofollow">

	<title>Terms of Service | Codo Player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="canonical" href="http://www.codoplayer.com/">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../res/lib/prettify/sunburst.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/common.css" type="text/css" />
</head>

<body>

	<?php include_once("../analytics.php") ?>

	<?php include("../header.php"); ?>

	<div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Terms of Service</h1>
                    <h2>Important information about licensing</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="content-1">
        <div class="container">

			<div class="row">
				<div class="col-md-12">

					<h3 class="no-margin-top">Please choose the license</h3>
					<ul>
						<li><a href="license/free">Codo Player Free</a></li>
						<li><a href="license/pro">Codo Player Pro</a></li>
						<li><a href="license/advertising">Codo Player Advertising Plugin</a></li>
					</ul>

				</div>
			</div>

		</div>
	</div>

	<?php include("../footer.php"); ?>

</body>
</html>