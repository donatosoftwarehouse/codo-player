<header>
    <div class="container text-center">
        <div class="row">
            <div class="col-md-3">
                <a href="/" class="logo"></a>
            </div>
            <div class="col-md-6">
                <nav>
                    <ul class="navigation">
                        <li class="<?php if(isset($nav)) if($nav=='setup') echo 'active' ?>"><a href="/setup/">Setup</a></li>
                        <li class="<?php if(isset($nav)) if($nav=='configuration') echo 'active' ?>"><a href="/configuration/">Config</a></li>
                        <li class="<?php if(isset($nav)) if($nav=='styling') echo 'active' ?>"><a href="/styling/">Styling</a></li>
                        <li class="<?php if(isset($nav)) if($nav=='api') echo 'active' ?>"><a href="/api/">API</a></li>
                        <li class="<?php if(isset($nav)) if($nav=='plugins') echo 'active' ?>"><a href="/plugins/">Plugins</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3">
                <div class="btn-group action">
                    <a href="/designer/" title="Designer" class="btn btn-danger"><span class="el-icon-pencil"></span></a>
                    <a href="/download/" class="btn btn-success">Download v2.1</a>
                    <a href="/account/login/" title="Login" class="btn btn-primary"><span class="el-icon-key"></span></a>
                </div>
            </div>
        </div>
    </div>
</header>