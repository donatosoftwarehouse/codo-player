<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="robots" content="noindex, nofollow">

	<title>Background plugin example | Codo Player</title>

	<link rel="icon" href="../../../favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="../../../resources/styles/common.css" type="text/css" />
	<link rel="stylesheet" href="resources/styles/base.css" type="text/css" />

</head>

<body>

<?php include_once("../../../analytics.php") ?>

<div class="wrapper">

	<div class="content">
		<div class="header-shadow"></div>

		<div class="inner">

			<script src="../../../CodoPlayerPro/CodoPlayer.js" type="text/javascript"></script>
			<script type="text/javascript">
			    CodoPlayer({src:["relative://../../../media/redsea.webm", "relative://../../../media/redsea.mp4"]}, {
					volume: 0,
			        plugins: {
						background: true
			        },
					id: 'player-background'
			    })
			</script>

			<div class="content-wrap">
				<h3 class="special">Background plugin example</h3>
				<div class="h3-special-content text-center">

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod ultricies dictum. Nam lobortis luctus nulla, ac molestie libero euismod in. Suspendisse urna mi, fringilla eu interdum at, pretium faucibus purus. Donec venenatis mollis dui vitae iaculis. Vivamus malesuada imperdiet lacinia. Nam ultricies sapien suscipit risus congue porta. In sed quam tortor. Praesent ut dolor aliquet velit sodales congue at sit amet dui. Phasellus mauris est, sagittis ut commodo a, mollis eu orci. Ut volutpat risus vitae quam faucibus scelerisque. Suspendisse mattis posuere mauris, ac semper tortor malesuada porta. Aenean vehicula mauris vitae metus semper a tempus mi venenatis. Phasellus metus ante, tempus nec imperdiet at, laoreet ac nisi. Pellentesque sapien neque, ultrices a ultricies eu, rhoncus vel libero. Integer ornare bibendum dolor, tincidunt eleifend nunc ullamcorper dignissim.</p>

					<p>Aenean consequat facilisis mauris, non sodales purus posuere ac. Phasellus vel turpis ac sem eleifend blandit sit amet eu dui. Mauris ac quam tellus. Nam leo massa, feugiat quis condimentum non, condimentum eu turpis. Nulla vitae velit sed est viverra sagittis. Morbi purus turpis, bibendum id venenatis a, tincidunt ut augue. Vivamus eu erat nec lectus dictum imperdiet. Nullam eu urna id enim laoreet fringilla. Nunc volutpat condimentum facilisis. Etiam ut nisi justo, eu tincidunt est. Donec cursus purus sit amet felis gravida in tincidunt sem auctor.</p>

					<p>Cras aliquet auctor urna non lacinia. Suspendisse condimentum justo at nisi rutrum id ultricies nulla rutrum. Nullam neque nunc, condimentum sed tempus a, venenatis id est. Proin dictum tincidunt sem sed egestas. Aliquam id massa in nisi iaculis aliquet. Nulla facilisi. Morbi dolor dui, vehicula id tristique in, egestas vitae magna. Donec mattis vulputate sem, at rhoncus arcu convallis a.</p>

					<p>Maecenas scelerisque elit vel nunc rutrum vitae varius tortor pharetra. Aenean bibendum, nulla at sodales aliquam, turpis risus egestas leo, nec cursus ipsum nulla in libero. Quisque ultrices fringilla magna, in eleifend erat consectetur quis. Curabitur sit amet arcu nulla, at rhoncus nibh. In sed nibh nulla, nec dictum ligula. Ut pellentesque, urna et ullamcorper lobortis, nisi arcu accumsan nisi, vitae sagittis metus arcu nec elit. Phasellus faucibus rhoncus lorem rutrum scelerisque. Vestibulum volutpat nisi eget tortor porttitor at malesuada turpis mollis. Fusce a dignissim magna. Donec sit amet orci augue, id tristique ipsum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In hac habitasse platea dictumst. Aliquam erat volutpat. Etiam vitae eros sit amet lacus lacinia cursus. Cras gravida pulvinar erat nec congue. Donec in lorem erat, ac posuere turpis.</p>

					<p>Sed eget porta libero. Ut dui lectus, sodales nec tristique non, tristique vel diam. Aliquam hendrerit dignissim nisl ut commodo. Ut in pellentesque velit. Aenean vitae lectus in leo tristique tempor id nec libero. Sed lacinia vestibulum velit ac sagittis. Quisque viverra malesuada adipiscing. Duis tincidunt blandit enim vel mollis. Phasellus in turpis metus. Proin at dui turpis, vel mollis enim. Duis ut lectus in odio sodales luctus. Phasellus scelerisque laoreet nisl, vel imperdiet neque ullamcorper vel. In in nisl turpis, et tempus augue. Nunc sit amet dui at elit elementum vestibulum.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod ultricies dictum. Nam lobortis luctus nulla, ac molestie libero euismod in. Suspendisse urna mi, fringilla eu interdum at, pretium faucibus purus. Donec venenatis mollis dui vitae iaculis. Vivamus malesuada imperdiet lacinia. Nam ultricies sapien suscipit risus congue porta. In sed quam tortor. Praesent ut dolor aliquet velit sodales congue at sit amet dui. Phasellus mauris est, sagittis ut commodo a, mollis eu orci. Ut volutpat risus vitae quam faucibus scelerisque. Suspendisse mattis posuere mauris, ac semper tortor malesuada porta. Aenean vehicula mauris vitae metus semper a tempus mi venenatis. Phasellus metus ante, tempus nec imperdiet at, laoreet ac nisi. Pellentesque sapien neque, ultrices a ultricies eu, rhoncus vel libero. Integer ornare bibendum dolor, tincidunt eleifend nunc ullamcorper dignissim.</p>

				</div>
			</div>

		</div>
	</div>

</div>

</body>
</html>