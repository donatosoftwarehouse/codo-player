<?php
	$nav = 'plugins';

	session_start();

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}
	if(isset($user)) {
		include '../database/database.php';
		$database = database();
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />

	<title>Plugins | Codo Player</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Power up the player with extra functionality">
    <meta name="copyright" content="Donato Software House" />
	<meta property="og:title" content="Codo Player - A Prime HTML5 FLASH Web Video Player" />
    <meta property="og:image" content="http://codoplayer.com/res/img/codo-player-logo-dark-bg.jpg?v=2" />
    <meta property="og:url" content="http://codoplayer.com/"/>
    <meta property="og:description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!"/>

    <link rel="canonical" href="http://www.codoplayer.com/">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../res/lib/prettify/sunburst.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/common.css" type="text/css" />
    <link rel="stylesheet" href="res/css/base.css" type="text/css" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<?php include_once("../analytics.php") ?>

	<?php include("../header.php"); ?>

	<div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>Plugins</h1>
                    <h2>Power up the player with extra functionality</h2>
                </div>
            </div>
        </div>
    </div>

	<div class="content-1">
    	<div class="container">
            <div class="row">
                <div class="col-md-12">

					<h3 class="no-margin-top">Advertising Plugin (VAST, VPAID)</h3>

					<p>Monetise videos with advertising, using a standard VAST/VPAID template.</p>
					<p>Featuring pre-roll, mid-roll, post-roll, events beaconing, companion ads, skip button and custom CSS styling.</p>
					<p><strong>Offering a free technical support for a basic plugin setup!</strong></p>
					<p><a href="../account/register/" class="btn btn-success btn-lg" onclick="var self = this; _gaq.push(['_trackEvent', 'Advertising Plugin', 'Download', self.href]); setTimeout(function() { document.location.href = self.href; }, 100); return false;">Buy now ($325)</a>&nbsp;&nbsp;&nbsp;&nbsp;<small>Download the setup package (.zip) and extract it to CodoPlayer/plugins folder.</small></p>


						<h4>Full setup example</h4>

<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script type="text/javascript">
CodoPlayer("video.mp4", {
    plugins: {
        advertising: {
            title: "Advertisement",
            timerText: "Your video will play in %time% s.", // %time% replaced with counter
            servers: [{
                apiAddress: "" // Ads server url returning VAST .xml
            }],
            schedule: [{
                position: "pre-roll"
            }, {
                position: "mid-roll",
                startTime: "00:00:15"
            }, {
                position: "post-roll"
            }],
            skipAd: {
                enabled: true,
                timeout: 5,
                text: "Skip"
            },
            fallback: function() {
                // If Ad Blocker enabled or otherwise advertising can not be served
            },
            logging: true
        }
    }
})
&lt;/script>
</pre>

						<h4>Add companion ads</h4>
						<h5>Insert &lt;div&gt; tags with special class names (referenced below). Plugin will stack companion ads within elements of that class name.</h5>

<pre class="prettyprint">
&lt;div class="codo-player-ad-companion-0">&lt;/div>
&lt;div class="codo-player-ad-companion-1">&lt;/div>
&lt;div class="codo-player-ad-companion-2">&lt;/div>
&lt;div class="codo-player-ad-companion-3">&lt;/div>
...
</pre>

                    <p></p>
					<p>For more information, please visit <a href="http://www.iab.net/guidelines/508676/digitalvideo/vsuite/vast" target="_blank">IAB guidelines</a>.</p>


                    <h3 class="special">Share</h3>
                    <p>Add a toolbar element containing Facebook and Twitter sharing buttons.</p>

                    <p><a href="share/download/" class="btn btn-success btn-lg" onclick="var self = this; _gaq.push(['_trackEvent', 'Share (plugin)', 'Download', self.href]); setTimeout(function() { document.location.href = self.href; }, 100); return false;">Download</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small class="muted">Download the setup package (.zip) and extract it to Codo Player plugins folder.</small></p>

                    <h5>To enable plugin</h5>

<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script type="text/javascript">
    CodoPlayer("video.mp4", {
        plugins: {
            share: true
        }
    })
&lt;/script>
</pre>

                    <h3 class="special">Codo Player WordPress plugin</h3>
                    <p>Get Codo Player for your WordPress website with an easy to use designer tool.</p>

                    <p><a href="http://wordpress.org/plugins/codo-player/" target="_blank" class="btn btn-success btn-lg" onclick="var self = this; _gaq.push(['_trackEvent', 'WordPress (plugin)', 'Download', self.href]);">Download</a></p>

				</div>

			</div>
		</div>
	</div>

	<?php include("../footer.php"); ?>

	<script src="../res/lib/jquery/jquery-1.11.0.min.js"></script>
    <script src="../res/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="../res/lib/angular/angular.min.js" type="text/javascript"></script>
    <script src="../res/lib/prettify/prettify.js"></script>
    <script src="../res/js/common.js"></script>
    <script src="res/js/base.js" type="text/javascript"></script>

</body>
</html>