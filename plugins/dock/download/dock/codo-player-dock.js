/*
	Codo Player 'dock' plugin
	Copyright (C) 2012-2013 Donato Software House
*/

(function() {
	function IsInView(el) {
		var top = el.offsetTop;
		var left = el.offsetLeft;
		var width = el.offsetWidth;
		var height = el.offsetHeight;
		while(el.offsetParent) {
			el = el.offsetParent;
			top += el.offsetTop;
			left += el.offsetLeft;
		}
		return (
			top < (window.pageYOffset + window.innerHeight) &&
			left < (window.pageXOffset + window.innerWidth) &&
			(top + height) > window.pageYOffset &&
			(left + width) > window.pageXOffset
		);
	};
	var state;
	var file = "codo-player-dock.js";
	Codo().link(Codo().getScriptTag(file).src.split("?")[0].replace(file, "") + "styles/style.css");
	for(var i = 0; i < CodoPlayerAPI.length; i++) {
		if (CodoPlayerAPI[i].plugins && CodoPlayerAPI[i].plugins.dock) {
			var api = CodoPlayerAPI[i];
			Codo(window).on("scroll", function(e) {
				if(!IsInView(api.DOM.parent)) {
					if(!state) {
						Codo(api.DOM.parent).addClass("codo-player-dockable");
						state = true;
					}
				} else {
					if(state) {
						Codo(api.DOM.parent).removeClass("codo-player-dockable");
						state = false;
					}
				}
			})
		}
	};
})();
