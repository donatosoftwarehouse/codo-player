<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="robots" content="noindex, nofollow">

	<title>Share plugin example | Codo Player</title>

	<link rel="icon" href="../../../favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="../../../resources/styles/common.css" type="text/css" />
	<link rel="stylesheet" href="resources/styles/base.css" type="text/css" />

</head>

<body>

<?php include_once("../../../analytics.php") ?>

<div class="wrapper">

	<div class="content">
		<div class="header-shadow"></div>

		<div class="inner text-center">

			<h2 class="lead">Share plugin example</h2>

			<script src="../../../CodoPlayerPro/CodoPlayer.js" type="text/javascript"></script>
			<script type="text/javascript">
			    CodoPlayer({src:["relative://../../../media/redsea.webm", "relative://../../../media/redsea.mp4"]}, {
					width: 600,
					height: 338,
			        plugins: {
						share: true
			        }
			    })
			</script>


		</div>

	</div>

</div>


</body>
</html>