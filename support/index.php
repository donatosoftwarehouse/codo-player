<?php
	$nav = 'support';

	session_start();

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}

	if(isset($user)) {
		include '../database/database.php';
		$database = database();
	}
?>

<!DOCTYPE html>
<html lang="en" ng-app="App">
<head>
	<meta charset="utf-8" />
	<title>Support | Codo Player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="For technical support, please visit Codo Player forum page, like us on Facebook or contact directly by filling out a form.">
	<meta name="copyright" content="Donato Software House" />

	<meta property="og:title" content="Codo Player - A Prime HTML5 FLASH Web Video Player" />
    <meta property="og:image" content="http://codoplayer.com/res/img/codo-player-logo-dark-bg.jpg?v=2" />
    <meta property="og:url" content="http://codoplayer.com/"/>
    <meta property="og:description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!"/>

    <link rel="canonical" href="http://www.codoplayer.com/">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../res/lib/prettify/sunburst.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/common.css" type="text/css" />
    <link rel="stylesheet" href="res/css/base.css" type="text/css" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <?php include_once("../analytics.php") ?>

	<?php include("../header.php"); ?>

    <div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Support</h1>
                    <h2>Get to know more about the player</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="content-1">
    	<div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h3 class="no-margin-top">Forum</h3>
                    <p>For all technical questions visit forum.<br/>
                    Being frequently reviewed, it offers a good opportunity to get certain answers.</p>
                    <h4 class="marginTop20"><a href="../forum" target="_blank">Visit forum</a></h4>
                    <p class="note">Posting to forum requires <a href="../account/register/">registration</a>.</p>

                </div>
            </div>

    	</div>
    </div>

    <div class="content-2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="no-margin-top">Follow Codo Player!</h3>
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=278688035537722";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-like-box" data-href="https://www.facebook.com/pages/Codo-Player/244766252323397?ref=ts&amp;fref=ts" data-width="100%" data-show-faces="true" data-stream="true" data-show-border="false" data-header="true"></div>

                </div>
            </div>
        </div>
    </div>

	<?php include("../footer.php"); ?>

	<script src="../res/lib/jquery/jquery-1.11.0.min.js"></script>
    <script src="../res/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="../res/lib/angular/angular.min.js" type="text/javascript"></script>
    <script src="../res/lib/prettify/prettify.js"></script>
    <script src="../res/js/common.js"></script>
    <script src="res/js/base.js" type="text/javascript"></script>

</body>
</html>