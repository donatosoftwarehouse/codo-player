﻿$(function() {
    $(".nav-tabs .inline").on("click", function() {
        _gaq.push(['_trackEvent', 'Inline', 'Setup', this.href]);
    });
    $(".nav-tabs .dynamic").on("click", function() {
        _gaq.push(['_trackEvent', 'Dynamic', 'Setup', this.href]);
    });
    $(".prettyprint").removeClass("prettyprinted");
    prettyPrint();
})