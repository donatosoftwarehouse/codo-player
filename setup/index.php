<?php
	$nav = 'setup';

	session_start();

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}
	if(isset($user)) {
		include '../database/database.php';
		$database = database();
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Setup | Codo Player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Learn how to setup Codo Player using a JavaScript snippet">
    <meta name="author" content="Donato Software House">
    <meta property="og:title" content="Codo Player - A Prime HTML5 FLASH Web Video Player" />
    <meta property="og:image" content="http://codoplayer.com/res/img/codo-player-logo-dark-bg.jpg?v=2" />
    <meta property="og:url" content="http://codoplayer.com/"/>
    <meta property="og:description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!"/>

    <link rel="canonical" href="http://www.codoplayer.com/">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../res/lib/prettify/sunburst.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/common.css" type="text/css" />
    <link rel="stylesheet" href="res/css/base.css" type="text/css" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<?php include_once("../analytics.php") ?>

	<?php include ("../header.php"); ?>

    <div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Setup</h1>
                    <h2>Follow instructions to embed the player manually</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="content-1">
        <div class="container">

			<div class="row">
				<div class="col-md-12">
					<h3 class="no-margin-top">1. Download</h3>
					<ol type="A">
						<li>Please <a href="../download/">Download Codo Player</a> setup package.</li>
						<li>Extract the package (.zip) into any folder of the <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="" data-original-title="When developing locally, use local web server environment ('localhost')"><abbr title="">website's environment</abbr></span> the player is about to operate.</li>
					</ol>
                    <p class="no-margin-bottom"><a href="../download/"><img id="extract" src="res/img/files.png"></a></p>

					<h3>2. Embed</h3>
                    <ol type="A" class="no-margin-bottom">
                        <li>Copy the snippet example (see below) &amp; paste it anywhere within the page's &lt;body&gt; tag.</li>
                        <Li>Edit the &lt;script&gt; "src" attribute to point to "CodoPlayer.js" file on your file system.</li>
                        <li>Replace the "video.mp4" string value with the path to media source.</li>
                    </ol>

                    <h3>3. Play</h3>
                    <strong>Setup complete!</strong> Save the code &amp; refresh the browser.
                </div>
            </div>

        </div>
    </div>

    <div class="content-2">
       <div class="container">

            <div class="row">
                <div class="col-md-12">

                    <h3 class="no-margin-top">Snippet examples</h3>

                     <p>Use the examples as a starting point for a new setup.</p>

                    <div class="panel-group panel-group-lists collapse in" id="accordion1" style="height: auto;">

                        <div class="panel">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                    Cross browser (HTML5/FLASH) using a single ".mp4"
                                </a>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse in">
                                <div class="panel-body">
<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script type="text/javascript">
    CodoPlayer("video.mp4")
&lt;/script>
</pre>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
                                    Cross browser (HTML5/FLASH) using multiple formats
                                </a>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" style="height: auto;">
                              <div class="panel-body">
<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script type="text/javascript">
    CodoPlayer([["video.webm", "video.ogg", "video.mp4"]]) // Note ".mp4" positioned at the end of array
&lt;/script>
</pre>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
                                    Player with custom width &amp; height
                                </a>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse">
                                <div class="panel-body">
<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script type="text/javascript">
    CodoPlayer("video.mp4", {
        width: 600,
        height: 338
    })
&lt;/script>
</pre>

                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">
                                    Youtube
                                </a>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse">
                                <div class="panel-body">
<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script>
    CodoPlayer("youtube video id", {  // for example: Ddi2TBnzdPo
        engine: "youtube",
        width: 600,
        height: 338
    })
&lt;/script></pre>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseEight">
                                    RTMP streaming (All Flash browsers)
                                </a>
                            </div>
                            <div id="collapseEight" class="panel-collapse collapse">
                                <div class="panel-body">
<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script>
    CodoPlayer("mp4:video.mp4", {
        width: 600,
        height: 338,
        rtmp: "rtmp://url_to_RTMP_server"
    })
&lt;/script></pre>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseNine">
                                    HTTP streaming (Some HTML5 browsers)
                                </a>
                            </div>
                            <div id="collapseNine" class="panel-collapse collapse">
                                <div class="panel-body">
<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script>
    CodoPlayer("http://domain/path/playlist.m3u8", {
        width: 600,
        height: 338
    })
&lt;/script></pre>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTen">
                                    Playlist of mixed format clips with autoplay and loop enabled
                                </a>
                            </div>
                            <div id="collapseTen" class="panel-collapse collapse">
                                <div class="panel-body">
<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script>
    CodoPlayer([
        "video.flv", // Flash video
        {
            src: ["video2.webm", "video2.ogv", "video2.mp4"], // HTML5 video
            title: "My video title"
        },
        "audio.mp3" // HTML5 or Flash audio, depending on browser capabilities
    ], {
        width: 600,
        height: 338,
        playlist: true,
        autoplay: true,
        loop: true
    })
&lt;/script></pre>
                                </div>
                            </div>
                        </div>

					</div>

                    <h3>Dynamic preload</h3>

                    <p>Use this method to load the player dynamically, whenever required.</p>
                    <h4>1. HTML</h4>
                    <h5>Paste the script loader within the &lt;header&gt; tag.</h5>
<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
</pre>

                    <h5>Paste the parent HTML element within the &lt;body&gt; tag.</h5>
<pre class="prettyprint">
&lt;div class="my-player">&lt;/div>
</pre>

                    <h4>2. JavaScript</h4>
                    <h5>Paste the Codo Player snippet code within the JavaScript file, and execute it when DOM content is loaded.</h5>

<pre class="prettyprint">
CodoPlayer("video.mp4", {
    width: 600,
    height: 338
}, ".my-player") // ID/CLASS given to the parent Codo Player HTML element
</pre>

                    <h3>Playlist</h3>

                    <p>Display a list of clips combined into a playlist</p>

                    <h5>To enable playlist</h5>

<pre class="prettyprint">
CodoPlayer(["video.mp4", "video2.mp4", "video3.mp4"], {
    width: 600,
    height: 338,
    playlist: true
})
</pre>

                    <h3>HD Toggle</h3>

                    <p>Display SD/HD toggle button in the control bar</p>
                    <h5>To enable HD toggle</h5>

<pre class="prettyprint">
CodoPlayer({
    src: "videoSD.mp4",
    srcHD: "videoHD.mp4"
}, {
    width: 600,
    height: 338,
    priority: "srcHD" // Video starts in HD
})
</pre>

                    <h3>Cuepoints</h3>

                    <p>Register cuepoints and trigger them on specific time</p>
                    <h5>To set the cuepoint</h5>

<pre class="prettyprint">
var player = CodoPlayer("video.mp4", {
    width: 600,
    height: 338,
    cuepoints: [5, 10] // Trigger will fire at 5 and 10 seconds
})

player.media.onCuepoint(function() {
    console.log("Cuepoint fired")
})
</pre>

                </div>
            </div>

        </div>
    </div>

    <div class="content-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h3 class="no-margin-top">Format support table</h3>
                    <div class="panel panel-default table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>HTML5 Video/Audio</th>
                                    <th class="text-center">MP4/H.264</th>
                                    <th class="text-center">WebM</th>
                                    <th class="text-center">Ogv/Ogg</th>
                                    <th class="text-center">MP3</th>
                                    <th class="text-center">WAV</th>
                                    <th class="text-center">AAC</th>
                                    <th class="text-center">Ogg</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Chrome 4+</td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                </tr>
                                <tr>
                                    <td>Firefox 3.5+</td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span> (29)</td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                </tr>
                                <tr>
                                    <td>IE 9+</td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td>Safari 4+</td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td>iOS Safari 3.2+</td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td>Android Browser 2.3+</td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                </tr>
                                <tr>
                                    <td>Opera 10.5+</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="panel panel-default table-responsive no-margin-bottom">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>FLASH Video/Audio</th>
                                    <th class="text-center">MP4/H.264</th>
                                    <th class="text-center">FLV</th>
                                    <th class="text-center">MP3</th>
                                    <th class="text-center">WAV</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>All Flash browsers</td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <h3>Note</h3>
                    <h5>The use of absolute URL for the source of clip is recommended.</h5>

<pre class="prettyprint">
CodoPlayer("http://example.com/video.mp4")
</pre>
                    <h5>For relative, the URL must begin with <strong>"relative://"</strong> prefix then the path to media comes relative to "CodoPlayer" directory on the system.</h5>

<pre class="prettyprint">
CodoPlayer("relative://video.mp4")
</pre>

                    <h3>Get more of Codo Player</h3>
                    <p>Setup instructions define the minimal approach of embedding the player.</p>
                    <p>To customise the look and functionality, proceed further to <a href="../configuration/">Configuration</a> &amp; <a href="../styling/">Styling</a> &amp; <a href="../api/">API</a> pages.</p>

                </div>
            </div>
        </div>
    </div>

	<?php include ("../footer.php"); ?>

    <script src="../res/lib/jquery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="../res/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../res/lib/angular/angular.min.js" type="text/javascript"></script>
    <script src="../res/lib/prettify/prettify.js" type="text/javascript"></script>
    <script src="../res/js/common.js" type="text/javascript"></script>
    <script src="res/js/base.js" type="text/javascript"></script>

</body>
</html>
