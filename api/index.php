<?php
	$nav = 'api';

	session_start();

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}
	if(isset($user)) {
		include '../database/database.php';
		$database = database();
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />

	<title>API | Codo Player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Control player behaviour with JavaScript">
	<meta name="copyright" content="Donato Software House" />

	<meta property="og:title" content="Codo Player - A Prime HTML5 FLASH Web Video Player" />
    <meta property="og:image" content="http://codoplayer.com/res/img/codo-player-logo-dark-bg.jpg?v=2" />
    <meta property="og:url" content="http://codoplayer.com/"/>
    <meta property="og:description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!"/>

    <link rel="canonical" href="http://www.codoplayer.com/">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../res/lib/prettify/sunburst.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/common.css" type="text/css" />
    <link rel="stylesheet" href="res/css/base.css" type="text/css" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<?php include_once("../analytics.php") ?>


	<?php include("../header.php"); ?>

	<div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>API</h1>
                    <h2>Control player behaviour with JavaScript</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="content-1">
        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h3 class="no-margin-top">Access interface</h3>

                    <p>Simply assign a new "CodoPlayer" instance to a variable.</p>

                    <div class="code-block">

                        <p class="lead">To access API</p>
<p>
<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script>
    var player = CodoPlayer("video.mp4", {
        onReady: function(player) {
            console.log(player);  // API
        }
    })
    console.log(player); // API
&lt;/script></pre>
</p>

                    </div>

                    <h3>Player API</h3>

                    <div class="code-block">
                        <p class="lead">Methods to control the player</p>

<pre class="prettyprint">
player.play("video.mp4")
player.pause()
player.resize(400, 225)
player.destroy()
</pre>

                    </div>

                    <h3>Media API</h3>

                    <div class="code-block">
                        <p class="lead">Methods to control the media and obtain clip information</p>

<pre class="prettyprint">
player.media.play()
player.media.pause()
player.media.toggle()
player.media.isPlaying()
player.media.getCurrentTime()
player.media.setCurrentTime(20)
player.media.getDuration()
player.media.getVolume()
player.media.setVolume(80)
player.media.toggleFullScreen()
player.media.enterFullScreen()
player.media.exitFullScreen()
player.media.getParent()
</pre>

                    </div>

                    <h3>Playlist API</h3>

                    <div class="code-block">
                        <p class="lead">Methods to control the playlist</p>

<pre class="prettyprint">
player.playlist.set(mediaObj)
player.playlist.next(index)
player.playlist.getCurrentClip()
</pre>

                    </div>

                    <h3>Event callbacks</h3>

                    <div class="code-block">
                        <p class="lead">Callbacks fired on certain event trigger</p>

<pre class="prettyprint">
player.media.onBeforeLoad(function() {
    console.log("onBeforeLoad");
})
player.media.onLoad(function() {
    console.log("onLoad");
})
player.media.onMetaData(function(metaObj) {
    console.log("onMetaData");
})
player.media.onPlay(function(position) {
    console.log("onPlay: " + position);
})
player.media.onPause(function(position) {
    console.log("onPause: " + position);
})
player.media.onEnd(function() {
    console.log("onEnd");
})
player.media.onBuffer(function(buffer) {
    console.log("onBuffer: " + buffer);
})
player.media.onProgress(function(progress) {
    console.log("onProgress: " + progress);
})
player.media.onSeekStart(function(position) {
    console.log("onSeekStart: " + position);
})
player.media.onSeekEnd(function(position) {
    console.log("onSeekEnd: " + position);
})
player.media.onVolumeChange(function(newVol) {
    console.log("onVolumeChange: " + newVol);
})
player.media.onFullScreenEnter(function() {
    console.log("onFullScreenEnter");
})
player.media.onFullScreenExit(function() {
    console.log("onFullScreenExit");
})
player.media.onError(function() {
    console.log("onError");
})
player.media.onClipBegin(function() {
    console.log("onClipBegin");
})
player.media.onClipEnd(function() {
    console.log("onClipEnd");
})
player.media.onClipFirstQuarter(function() {
    console.log("onClipFirstQuarter");
})
player.media.onClipSecondQuarter(function() {
    console.log("onClipSecondQuarter");
})
player.media.onClipThirdQuarter(function() {
    console.log("onClipThirdQuarter");
})
player.media.onCuepoint(function() {
    console.log("onCuepoint");
})
</pre>

                    </div>

                </div>
            </div>

        </div>
    </div>

	<?php include("../footer.php"); ?>

	<script src="../res/lib/jquery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="../res/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../res/lib/angular/angular.min.js" type="text/javascript"></script>
    <script src="../res/lib/prettify/prettify.js" type="text/javascript"></script>
    <script src="../res/js/common.js" type="text/javascript"></script>
    <script src="res/js/base.js" type="text/javascript"></script>

</body>
</html>