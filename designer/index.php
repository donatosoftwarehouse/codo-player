<?php
$nav = 'designer';

session_start();

if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
}
if (isset($_SESSION['ident'])) {
    $ident = $_SESSION['ident'];
}
if (isset($_SESSION['email'])) {
    $email = $_SESSION['email'];
}
if (isset($user)) {
    include '../database/database.php';
    $database = database();
}
?>

<!DOCTYPE html>
<html lang="en" ng-app="App">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Designer | Codo Player</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Setup the player via the visual interface tool.">
        <meta name="author" content="Donato Software House">
        <meta property="og:title" content="Codo Player - A Prime HTML5 FLASH Web Video Player" />
        <meta property="og:image" content="http://codoplayer.com/res/img/codo-player-logo-dark-bg.jpg?v=2" />
        <meta property="og:url" content="http://codoplayer.com/"/>
        <meta property="og:description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!"/>

        <link rel="canonical" href="http://www.codoplayer.com/">
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="../res/css/elusive-webfont.css" type="text/css" />
        <link rel="stylesheet" href="../res/lib/prettify/sunburst.css" type="text/css" />
        <link rel="stylesheet" href="../res/css/common.css" type="text/css" />
        <link rel="stylesheet" href="res/css/base.css" type="text/css" />

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body ng-controller="DesignerCtrl">

        <?php include_once ("../analytics.php") ?>

        <?php include ("../header.php"); ?>

        <div class="content-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>Designer</h1>
                        <h2>Setup the player via the visual interface tool</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-1 player">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                        <script src="../CodoPlayerPro/CodoPlayer.js" type="text/javascript"></script>
                        <div id="designer-player"></div>
                 </div>

                 <div class="col-md-4">
                    <div class="panel-group panel-group-lists collapse in" id="accordion2">
    	              <div class="panel">
    	                <div class="panel-heading">
    	                  <h4 class="panel-title no-margin-top">
    	                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFour" class="">
    	                     Settings
    	                    </a>
    	                  </h4>
    	                </div>
    	                <div id="collapseFour" class="panel-collapse collapse in" style="height: auto;">
    	                   <div class="panel-body">

                                <label>
                                    <input type="checkbox" ng-model="currSettings.responsive" ng-change="SetResponsive()" /> Responsive
                                </label>

                                <form>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p>Width
                                            <input type="number" class="form-control" ng-model="currSettings.width" ng-change="SetWidth()" ng-blur="SetPlayer()" min="10" max="940" ng-disabled="currSettings.responsive" />
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Height
                                            <input type="number" class="form-control" ng-model="currSettings.height" ng-change="SetHeight()" ng-blur="SetPlayer()" min="10" max="940" ng-disabled="currSettings.responsive" />
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Volume
                                            <input type="number" class="form-control" ng-model="currSettings.volume" ng-change="SetVolume()" ng-blur="SetPlayer()" min="0" max="100" />
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <p>Preload
                                            <select class="form-control" ng-model="currSettings.preload" ng-change="SetPlayer()">
                                                <option ng-selected="true">true</option>
                                                <option>false</option>
                                            </select>
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Autoplay
                                            <select class="form-control" ng-model="currSettings.autoplay" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.autoplay == true">true</option>
                                                <option ng-selected="currSettings.autoplay == false">false</option>
                                            </select>
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Loop
                                            <select class="form-control" ng-model="currSettings.loop" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.loop == true">true</option>
                                                <option ng-selected="currSettings.loop == false">false</option>
                                            </select>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <p>Playlist
                                            <select class="form-control" ng-model="currSettings.playlist" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.playlist == true">true</option>
                                                <option ng-selected="currSettings.playlist == false">false</option>
                                            </select>
                                            </p>
                                        </div>
                                        <!-- <div class="col-md-4">
                                            <p>Style
                                            <select class="form-control" ng-model="currSettings.style" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.style == standard">standard</option>
                                                <option ng-selected="currSettings.style == mini">mini</option>
                                            </select>
                                            </p>
                                        </div> -->
                                    </div>

                                </form>

    		                  </div>
    		                </div>
    		              </div>

    		              <div class="panel">
    		                <div class="panel-heading">
    		                  <h4 class="panel-title no-margin-top">
    		                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFive" class="collapsed">
    		                      Controls
    		                    </a>
    		                  </h4>
    		                </div>
    		                <div id="collapseFive" class="panel-collapse collapse" style="height: auto;">
    		                  <div class="panel-body">
                                <form>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p>Show
                                            <select class="form-control" ng-model="currSettings.controls.show" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.controls.show == auto">auto</option>
                                                <option ng-selected="currSettings.controls.show == always">always</option>
                                                <option ng-selected="currSettings.controls.show == never">never</option>
                                            </select>
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Play
                                            <select class="form-control" ng-model="currSettings.controls.play" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.controls.play == true">true</option>
                                                <option ng-selected="currSettings.controls.play == false">false</option>
                                            </select>
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Overlay
                                            <select class="form-control" ng-model="currSettings.controls.playBtn" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.controls.playBtn == true">true</option>
                                                <option ng-selected="currSettings.controls.playBtn == false">false</option>
                                            </select>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p>Seek
                                            <select class="form-control" ng-model="currSettings.controls.seek" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.controls.seek == true">true</option>
                                                <option ng-selected="currSettings.controls.seek == false">false</option>
                                            </select>
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Seeking
                                            <select class="form-control" ng-model="currSettings.controls.seeking" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.controls.seeking == true">true</option>
                                                <option ng-selected="currSettings.controls.seeking == false">false</option>
                                            </select>
                                            </p>
                                        </div>
                                       <!--  <div class="col-md-4">
                                            <p>Volume
                                            <select class="form-control" ng-model="currSettings.controls.volume" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.controls.volume == horizontal">horizontal</option>
                                                <option ng-selected="currSettings.controls.volume == vertical">vertical</option>
                                                <option ng-selected="currSettings.controls.volume == false">false</option>
                                            </select>
                                            </p>
                                        </div> -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p>Fullscreen
                                            <select class="form-control" ng-model="currSettings.controls.fullscreen" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.controls.fullscreen == true">true</option>
                                                <option ng-selected="currSettings.controls.fullscreen == false">false</option>
                                            </select>
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Title
                                            <select class="form-control" ng-model="currSettings.controls.title" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.controls.title == true">true</option>
                                                <option ng-selected="currSettings.controls.title == false">false</option>
                                            </select>
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Time
                                            <select class="form-control" ng-model="currSettings.controls.time" ng-change="SetPlayer()">
                                                <option ng-selected="currSettings.controls.time == true">true</option>
                                                <option ng-selected="currSettings.controls.time == false">false</option>
                                            </select>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p>Loading
                                            <input type="text" class="form-control" ng-model="currSettings.controls.loadingText" ng-change="SetPlayer()">
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Hide (s)
                                            <input type="text" class="form-control" ng-model="currSettings.controls.hideDelay" ng-change="SetPlayer()">
                                            </p>
                                        </div>
                                        <!-- <div class="col-md-4">
                                            <p>Forecolor
                                            <input type="text" class="form-control color {hash:true, onImmediateChange:'updateInfo(this);'} ng-pristine ng-valid" ng-model="currSettings.controls.foreColor" ng-change="SetPlayer()" autocomplete="off" style="background-image: none; background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);">
                                            </p>
                                        </div> -->
                                    </div>
                                    <!-- <div class="row">
                                        <div class="col-md-4">
                                            <p>Backcolor
                                            <input type="text" class="form-control color {hash:true, onImmediateChange:'updateInfo(this);'} ng-pristine ng-valid" ng-model="currSettings.controls.backColor" ng-change="SetPlayer()" autocomplete="off" style="background-image: none; background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);">
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Progress
                                            <input type="text" class="form-control color {hash:true, onImmediateChange:'updateInfo(this);'} ng-pristine ng-valid" ng-model="currSettings.controls.progressColor" ng-change="SetPlayer()" autocomplete="off" style="background-image: none; background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);">
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>Buffer
                                            <input type="text" class="form-control color {hash:true, onImmediateChange:'updateInfo(this);'} ng-pristine ng-valid" ng-model="currSettings.controls.bufferColor" ng-change="SetPlayer()" autocomplete="off" style="background-image: none; background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);">
                                            </p>
                                        </div>
                                    </div> -->

                                    </form>

    		                  </div>
    		                </div>
    		              </div>
    		              <div class="panel">
    		                <div class="panel-heading">
    		                  <h4 class="panel-title no-margin-top">
    		                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseSix" class="collapsed">
    		                      Media
    		                    </a>
    		                  </h4>
    		                </div>
    		                <div id="collapseSix" class="panel-collapse collapse" style="height: 0px;">
    		                  <div class="panel-body">

                                <form>
        		                    <div class="row controls" ng-repeat="clip in media">
                                        <div class="col-md-12">
                                            <p>Source
                                                <input type="text" class="form-control" placeholder="Source" ng-model="clip.src" class="source">
                                            </p>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Poster
                                                <input type="text" class="form-control" placeholder="Poster" ng-model="clip.poster" class="poster">
                                            </p>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Title
                                                <input type="text" class="form-control" placeholder="Title" ng-model="clip.title" class="title">
                                            </p>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Engine
                                                <select class="form-control" ng-model="clip.engine" ng-change="SetPlayer()">
                                                    <option value="auto" selected>auto</option>
                                                    <option value="html5">HTML5</option>
                                                    <option value="flash">FLASH</option>
                                                    <option value="youtube">YOUTUBE</option>
                                                </select>
                                            </p>
                                        </div>
                                        <div class="col-md-12">
                                            <p>
                                                <button class="btn btn-success" ng-click="SubmitClip($index)">Save</button>
                                                <button class="btn btn-danger" ng-click="RemoveClip($index)">X</button>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <a class="btn btn-primary btn-lg" ng-click="AddClip()">Add new clip</a>
                                        </div>
                                    </div>
                                </form>

    		                  </div>
    		                </div>
    		              </div>
    		            </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="content-2">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <h3 class="no-margin-top">Snippet</h3>
                        <ol>
                            <li><a href="../download/" target="_blank">Download Codo Player</a> setup package.</li>
                            <li>Copy the snippet code (below) &amp; paste it anywhere within the page's &lt;body&gt; tag.</li>
                            <li>Edit the &lt;script&gt; "src" attribute to point to "CodoPlayer.js" on your file system.</li>
                        </ol>

                        <p>
<pre class="prettyprint"><div ng-bind-template="{{playerSnippet}}"></div></pre>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <?php include ("../footer.php"); ?>

        <script src="../res/lib/jquery/jquery-1.11.0.min.js"></script>
        <script src="../res/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="../res/lib/angular/angular.min.js" type="text/javascript"></script>
        <script src="../res/lib/prettify/prettify.js"></script>
        <script src="../res/lib/picker/jscolor.js" type="text/javascript"></script>
        <script src="../res/js/common.js"></script>
        <script src="res/js/controllers.js" type="text/javascript"></script></body>
    </body>
</html>