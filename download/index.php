<?php
	$nav = 'download';

	session_start();

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}
	if(isset($user)) {
		include '../database/database.php';
		$database = database();
	}
?>

<!DOCTYPE html>
<html lang="en" ng-app="App">
<head>
	<meta charset="utf-8" />

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Download | Codo Player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Download Codo Player FREE or buy a license for a PRO version.">
    <meta name="author" content="Donato Software House">
    <meta property="og:title" content="Codo Player - A Prime HTML5 FLASH web video player" />
    <meta property="og:image" content="http://codoplayer.com/res/images/codo-player-logo-dark-bg.jpg?v=2" />
    <meta property="og:url" content="http://codoplayer.com/"/>
    <meta property="og:description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!"/>

    <link rel="canonical" href="http://www.codoplayer.com/">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/common.css" type="text/css" />
    <link rel="stylesheet" href="res/css/base.css" type="text/css" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <?php include_once("../analytics.php") ?>

	<?php include("../header.php"); ?>

     <div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Download</h1>
                    <h2>Choose Free vs. Pro (allows custom branding)</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="content-1 text-center">
        <div class="container">
            <div class="row">

                <div class="col-md-4 free">

                    <h3 class="no-margin-top">Codo Player Free</h3>
                    <h4 class="price lead">FREE</h4>
                    <small><span>for a lifetime</span></small>

                    <hr/>

                    <small>
                    <ul>
                        <li>All <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="" data-html="true" data-original-title="Config, styling, API, playlist, HD toggle, cuepoints, keyboard control"><abbr title="">standard</abbr></span> features</li>
                        <li>For non-commercial use only</li>
                        <li>Carries Codo Player logo</li>
                        <li>Custom branding not permitted</li>
                    </ul>
                    </small>

                    <hr/>

                    <p>
                        <a href="CodoPlayerFree/" class="btn btn-danger" onclick="var self = this; _gaq.push(['_trackEvent', 'Codo Player Free', 'Download', self.href]); setTimeout(function() { document.location.href = self.href; }, 100); return false;"><span class="el-icon-download-alt"></span> Download</a>
                    </p>

                    <p>
                        <a href="../policy/license/free" target="_blank"><small>GPL type license</small></a>
                    </p>

                </div>

                <div class="col-md-4 pro">

                    <h3 class="no-margin-top">Codo Player Pro</h3>
                    <h4 class="price lead">$95</h4>
                    <small><span>per domain</span></small>

                    <!-- <img src="res/img/support_badge.png" alt="Instant technical support" class="great-support"/> -->

                    <hr/>

                    <small>
                    <ul>
                        <li>All <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="" data-html="true" data-original-title="Config, styling, API, playlist, HD toggle, cuepoints, keyboard control"><abbr title="">standard</abbr></span> &amp; <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="" data-html="true" data-original-title="Custom loader animation, your own watermark"><abbr title="">PRO features</abbr></span></li>
                        <li>For commercial use</li>
                        <li>Codo Player logo removed</li>
                        <li>Custom branding permitted</li>
                    </ul>
                    </small>


                    <hr/>

                    <p>
                        <a href="../account/register/" class="btn btn-success btn-lg" onclick="var self = this; _gaq.push(['_trackEvent', 'Codo Player Pro', 'Buy', self.href]); setTimeout(function() { document.location.href = self.href; }, 100); return false;"><span class="el-icon-download-alt"></span> Get a Pro license</a>
                    </p>

                    <p>
                        <a href="../policy/license/pro" target="_blank"><small>Commercial license</small></a>
                    </p>

                </div>

                <div class="col-md-4 vast">

                    <h3 class="no-margin-top">Advertising Plugin</h3>
                    <h4 class="price lead">$325</h4>
                    <small><span class="subprice">per domain</span></small>

                    <hr/>

                    <small>
                    <ul>
                        <li>VAST, VPAID templates support</li>
                        <li>Pre-roll, mid-roll, post-roll</li>
                        <li>Events beaconing</li>
                        <li>Custom CSS styling</li>
                    </ul>
                    </small>

                    <hr/>

                    <p>
                        <a href="../account/register/" class="btn btn-primary" onclick="var self = this; _gaq.push(['_trackEvent', 'Codo Player Advertising Plugin', 'Buy', self.href]); setTimeout(function() { document.location.href = self.href; }, 100); return false;"><span class="el-icon-download-alt"></span> Start Ads</a>
                    </p>

                    <p>
                        <a href="../policy/license/advertising/" target="_blank"><small>Commercial license</small></a>
                    </p>

                </div>

            </div>

        </div>
    </div>


    <div class="content-2 text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="no-margin-top">Codo Player Free</h3>

                    <small><p>(player in a loading state)</p></small>

                    <script src="../CodoPlayerPro/CodoPlayer.js" type="text/javascript"></script>
                    <script>
                        CodoPlayer([{
                            title:"Video title"
                        }], {
                            width: 450,
                            height: 253
                        });
                    </script>

                    <p>Codo Player logo must stay visible on loading and the use of custom branding is <strong>not</strong> permitted.</p>

                </div>
                <div class="col-md-6">
                    <h3 class="no-margin-top">Codo Player Pro</h3>

                    <small><p>(player in a loading state)</p></small>

                    <script>
                        CodoPlayer([{
                            title:"Video title"
                        }], {
                            width: 450,
                            height: 253,
                            logo: "../CodoPlayerPro/logo.png"
                        });
                    </script>

                    <p>Codo Player logo is removed and the use of custom branding is permitted.</p>

                </div>
            </div>

        </div>
    </div>

	<?php include("../footer.php"); ?>

	<script src="../res/lib/jquery/jquery-1.11.0.min.js"></script>
    <script src="../res/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="../res/lib/angular/angular.min.js" type="text/javascript"></script>
    <script src="../res/js/common.js"></script>
    <script src="res/js/base.js"></script>
</body>
</html>