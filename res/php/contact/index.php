<?php

function doReturn($status, $message) {
    $obj = new stdClass();
    $obj->success = $status;
    $obj->message = $message;
    return json_encode($obj);
}

function died($error) {
    echo doReturn(false, $error);
    die();
}

if(isset($_GET['email'])) {

    // validation expected data exists

    if(!isset($_GET['name']) ||

        !isset($_GET['email']) ||

        !isset($_GET['message']) ||

        !isset($_GET['spam'])) {

            died('Please fill out the form fields.');

    }

    $name = $_GET['name']; // required

    $company = $_GET['company']; // not required

    $email = $_GET['email']; // required

    $message = $_GET['message']; // required

    $spam = $_GET['spam']; // required



    $error_message = "";

    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

    if(!preg_match($email_exp,$email)) {

        $error_message .= 'Error. Email format incorrect.';
        died($error_message);

    }

    $string_exp = "/^[A-Za-z .'-]+$/";

    if(!preg_match($string_exp,$name)) {

        $error_message .= 'Error. Name format incorrect.';
        died($error_message);

    }

    if(strlen($message) < 2) {

        $error_message .= 'Error. Please provide a message.';
        died($error_message);

    }

    if($spam != 12) {

        $error_message .= 'Error. The sum does not match.';
        died($error_message);

    }

    function clean_string($string) {

        $bad = array("content-type","bcc:","to:","cc:","href");

        return str_replace($bad,"",$string);

    }

    $email_to = "support@codoplayer.com";
    $email_subject = "New message from ".$name;

    $emailHTML = '<html>
        <body>
            <p>Name: <strong>'.$name.'</strong></p>
            <p>Company: '.$company.'</p>
            <p>Email: '.$email.'</p>
            <p>'.$message.'</p>
            <p>---</p>
        </body>
    </html>';


    $headers = "From: Codo Player <support@codoplayer.com>\n";
    $headers .= "Reply-To: <support@codoplayer.com>\n";
    $headers .= "Return-Path: <support@codoplayer.com>\n";
    $headers .= "Envelope-from: <support@codoplayer.com>\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\n";
    $headers .= "MIME-Version: 1.0\n";

    @mail($email_to, $email_subject, $emailHTML, $headers);

    echo doReturn(true, "Message sent! We will get back soon. Thank you.");

} else {

    echo doReturn(false, "The email service is temporary down. Please try again.");

}

?>