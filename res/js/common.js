﻿var app = app || angular.module('App', []);
app.factory('designer', function() {
    return {};
});
app.factory("service", function($http) {
    return {
        contact: function(url, method, data) {
            return $http({
                url: url,
                method: method || "GET",
                params: data
            }).
            then(function(result) {
                return result.data;
            });
        }
    }
});
app.controller("contactCtrl", function($scope, $http, service, $timeout) {
    $scope.send = function(form) {
        if (form.$invalid) return;
        $scope.loading = true;
        $scope.output = "Please wait...";
        service.contact("/res/php/contact/", "POST", {
            name: $scope.name,
            company: $scope.company,
            email: $scope.email,
            message: $scope.message,
            spam: $scope.spam
        }).then(function(response) {
            $scope.output = response.message;
            $timeout(function() {
                $scope.loading = false;
                $scope.output = null;
                if (!response.success) return;
                $scope.name = null;
                $scope.company = null;
                $scope.email = null;
                $scope.message = null;
                $scope.spam = null;
            }, 5000);
        });
    }
});

if(window.console) {

    console.log("%c\n  _____       _        _____ _                  \n |     |___ _| |___   |  _  | |___ _ _ ___ ___  \n |   --| . | . | . |  |   __| | .'| | | -_|  _| \n |_____|___|___|___|  |__|  |_|__,|_  |___|_|   \n                                   ___|         ", "background: #333; color: white;");

    console.log("%c                                                                      \n Thoroughly thought through, no hassle, professional web media player \n                                                                      ", "background: red; color: white;");

}