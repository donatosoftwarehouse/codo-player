﻿$(function() {

    if(Codo().isMobile()) {
        $(".intro .player").css("margin-top", "20px");
        return;
    }

    var wH = $(window).height(),
        headerH = 100;
    $(".intro").css("min-height", wH - headerH);
    var rowHeight = $(".intro .row").height();
    $(".intro .player").css("margin-top", (wH - rowHeight) / 2 - headerH + "px");
    $(".intro .el-icon-chevron-down").click(function(e) {
        $('html, body').animate({
            scrollTop: $("header").offset().top
        }, 500);
    });
    $(".test-tag").submit(function(e) {
        e.preventDefault();
        setTimeout(function() {
            location.href = "?tag=" + encodeURIComponent($(".tag-url").val());
        }, 100);
    });
});