<?php
    $nav = 'home';

    session_start();

    if(isset($_SESSION['user'])) {
        $user = $_SESSION['user'];
    }
    if(isset($_SESSION['ident'])) {
        $ident = $_SESSION['ident'];
    }
    if(isset($_SESSION['email'])) {
        $email = $_SESSION['email'];
    }
    if(isset($user)) {
        include 'database/database.php';
        $database = database();
    }
?>
<!DOCTYPE html>
<html lang="en" ng-app="App">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Codo Player - A Prime HTML5 FLASH Web Video Player</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!">
        <meta name="author" content="Donato Software House">
        <meta property="og:title" content="Codo Player - A Prime HTML5 FLASH Web Video Player" />
        <meta property="og:image" content="http://codoplayer.com/res/img/codo-player-logo-dark-bg.jpg?v=2" />
        <meta property="og:url" content="http://codoplayer.com/"/>
        <meta property="og:description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!"/>

        <link rel="canonical" href="http://www.codoplayer.com/">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="res/css/elusive-webfont.css" type="text/css" />
        <link rel="stylesheet" href="res/css/common.css" type="text/css" />
        <link rel="stylesheet" href="res/css/base.css" type="text/css" />

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Scripts stay at the top to remove the flicker -->
        <script src="res/lib/jquery/jquery-1.11.0.min.js"></script>
        <script src="res/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="res/lib/angular/angular.min.js" type="text/javascript"></script>
        <script src="res/js/common.js"></script>
        <script src="res/js/base.js"></script>

    </head>
    <body>

        <?php include_once("analytics.php") ?>

        <div class="intro">

            <!-- <video class="video-bg" src="media/space-waves.mp4" loop autoplay></video> -->
            <!-- <div class="video-shade"></div> -->

            <div class="container">
                <div class="row player">

                    <div class="col-md-12">

                        <div id="player" style="max-width: 940px; max-height: 300px; margin: 0 auto;">
                            <form class="test-tag">
                                <div class="form-group form-inline" id="tag-form">
                                    <input type="text" name="tag" size="50" class="form-control input-sm tag-url" id="tag-url" placeholder="Test your VAST / VPAID tag (.xml)" data-ng-model="tag" required />
                                    <button type="submit" class="btn btn-sm btn-default" onclick="var self = this; _gaq.push(['_trackEvent', 'VAST tag test', document.getElementById('tag-url').value, self.href]);">Go!</button>
                                    <a href="plugins/" class="btn btn-sm btn-success">Get Advertising Plugin</a>
                                </div>
                            </form>

                            <script src="CodoPlayerPro/CodoPlayer.js" type="text/javascript"></script>
                            <script type="text/javascript">

                                var advertising;

                                <?php
                                    if(isset($_REQUEST['tag'])) {
                                        $tag = $_REQUEST['tag'];
                                        $link=preg_replace('/^(http(s)?)?:?\/*/u','http$2://',trim($tag));
                                        $link= htmlspecialchars($link, 11,'UTF-8',true);

                                        echo "advertising = {
                                            skipAd: {
                                                enabled: true,
                                                timeout: 3,
                                                text: 'Skip'
                                            },
                                            servers: [{
                                                apiAddress : '$tag'
                                            }],
                                            schedule: [{
                                                position: 'pre-roll'
                                            }]
                                        }";

                                    }
                                ?>

                                var player = CodoPlayer([{
                                    title: "Unique quality and outstanding look",
                                    src: ["http://codoplayer.com/media/takingSD.webm", "http://codoplayer.com/media/takingSD.mp4"],
                                    srcHD: ["http://codoplayer.com/media/takingHD.webm", "http://codoplayer.com/media/takingHD.mp4"],
                                    poster: "media/taking.jpg"
                                },{
                                    title: "Self-hosted, but easily maintained",
                                    src: ["http://codoplayer.com/media/landing.webm", "http://codoplayer.com/media/landing.mp4"]
                                }], {
                                    height: 300,
                                    priority: "srcHD",
                                    preload: false,
                                    autoplay: "<?php if(isset($_REQUEST['tag'])) { echo true; } else { echo false; } ?>",
                                    logo: "CodoPlayerPro/logo.png",
                                    ratio: [3, .96],
                                    onReady: function(player) {
                                        if(player.media.onLoad) {
                                            player.media.onLoad(function() {
                                                _gaq.push(['_trackEvent', player.playlist.getCurrentClip().title, 'onLoad']);
                                            })
                                            player.media.onError(function(error) {
                                                _gaq.push(['_trackEvent', player.playlist.getCurrentClip().title, 'onError']);
                                            });
                                        }
                                    },
                                    plugins: {
                                        advertising: advertising,
                                        share: true
                                    },
                                    controls: {
                                        show: "never"
                                    }
                                });
                            </script>
                        </div>

                    </div>

                </div>

                <div class="row title text-center">
                    <div class="col-md-12">
                        <h1>A Prime HTML5 FLASH Web Video Player</h1>
                    </div>
                </div>
            </div>

            <div class="el-icon-chevron-down text-center"></div>
        </div>
        <!-- end of Intro -->

        <?php include("header.php"); ?>


        <div class="content-2">
            <div class="container intro2">
                <div class="row">
                    <div class="col-md-6">
                        <div class="blockquote-box first clearfix">
                            <div class="square pull-left">
                                <span class="el-icon-flag"></span>
                            </div>
                            <h4>HTML5 FLASH video audio</h4>
                            <p>Supports all major web media formats via the HTML5 with a seamless fallback to FLASH</p>
                        </div>
                        <div class="blockquote-box blockquote-primary clearfix">
                            <div class="square pull-left">
                                <span class="el-icon-photo"></span>
                            </div>
                            <h4>Cross browser/device</h4>
                            <p>Works well with desktop, tablet and mobile to achieve the maximum audience (IE8+)</p>
                        </div>
                        <div class="blockquote-box blockquote-success clearfix">
                            <div class="square pull-left">
                                <span class="el-icon-resize-full"></span>
                            </div>
                            <h4>Responsive</h4>
                            <p>Auto adjusts the size to fit the parent element, therefore looks great on any layout</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="blockquote-box blockquote-info clearfix">
                            <div class="square pull-left">
                                <span class="el-icon-cogs"></span>
                            </div>
                            <h4>Unified Interface &amp; API</h4>
                            <p>Combines multiple platforms (HTML5/FLASH), provides a unified interface for programming</p>
                        </div>
                        <div class="blockquote-box blockquote-danger clearfix">
                            <div class="square pull-left">
                                <span class="el-icon-list"></span>
                            </div>
                            <h4>Playlist</h4>
                            <p>Stores clips in a playlist for an inline play and provides API for clip programming</p>
                        </div>
                        <div class="blockquote-box blockquote-warning clearfix">
                            <div class="square pull-left">
                                <span class="el-icon-asterisk"></span>
                            </div>
                            <h4>Plugins</h4>
                            <p>Offers a set existing helper tools and an ability to create your custom modules</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--
        <div class="content-1 text-center">
            <div class="container intro2">
                <div class="row">
                    <div class="col-md-4 box-1">

                        <h3 class="no-margin-top">Audio podcast</h3>

                        <div id="example-player-1" style="width: 300px; margin: 0 auto 40px;"></div>

                        <script type="text/javascript">
                            CodoPlayer([{
                                title: "Audio podcast",
                                src: "http://localhost/~donato_c/codoplayer/media/podcast.mp3",
                                poster: "media/podcast.jpg"
                            }], {
                                preload: false
                            }, "#example-player-1");
                        </script>

                    </div>

                    <div class="col-md-4 box-2">

                        <h3 class="no-margin-top">Video playlist</h3>

                        <div id="example-player-2" style="width: 340px; margin: 0 auto 40px;"></div>

                        <script type="text/javascript">
                            CodoPlayer([{
                                title:"Video title 1",
                                src: ["http://codoplayer.com/media/takingSD.webm", "http://codoplayer.com/media/takingSD.mp4"],
                                srcHD: ["http://codoplayer.com/media/takingHD.webm", "http://codoplayer.com/media/takingHD.mp4"],
                                poster: "media/taking.jpg"
                            },{
                                title:"Video title 1",
                                src: ["http://codoplayer.com/media/takingSD.webm", "http://codoplayer.com/media/takingSD.mp4"],
                                srcHD: ["http://codoplayer.com/media/takingHD.webm", "http://codoplayer.com/media/takingHD.mp4"],
                                poster: "media/taking.jpg"
                            },{
                                title:"",
                                src: ["http://codoplayer.com/media/takingSD.webm", "http://codoplayer.com/media/takingSD.mp4"],
                                srcHD: ["http://codoplayer.com/media/takingHD.webm", "http://codoplayer.com/media/takingHD.mp4"],
                                poster: "media/taking.jpg"
                            }], {
                                preload: false,
                                playlist: true
                            }, "#example-player-2");
                        </script>

                    </div>

                    <div class="col-md-4 box-3">

                        <h3 class="no-margin-top">Multiple players</h3>

                        <div id="example-player-3" style="width: 300px; margin: 0 auto 40px;"></div>

                        <script type="text/javascript">
                            CodoPlayer([{
                                title:"Self-hosted",
                                src: ["http://codoplayer.com/media/takingSD.webm", "http://codoplayer.com/media/takingSD.mp4"],
                                srcHD: ["http://codoplayer.com/media/takingHD.webm", "http://codoplayer.com/media/takingHD.mp4"],
                                poster: "media/taking.jpg"
                            }], {
                                preload: false
                            }, "#example-player-3");
                        </script>

                    </div>
                </div>
            </div>
        </div>
        -->

        <div class="content-3">
            <div class="container intro3">
                <div class="row featurette">
                    <div class="col-md-7">
                        <h2 class="featurette-heading">Lightweight. <span class="text-muted">~50KB only</span></h2>
                        <p class="lead">Library independent, contains into a tiny single script file. Requires a minimal count of HTTP requests. Perfect for a high traffic websites caring about the load performance.</p>
                    </div>
                </div>

                <div class="row featurette">
                    <div class="col-md-5">
                        <p class="image pull-left">&#9820;</p>
                    </div>
                    <div class="col-md-7">
                        <h2 class="featurette-heading">White labelled. <span class="text-muted">Highly customisable</span></h2>
                        <p class="lead">Comes in a minimal and clear looking style to suit every occasion. A good starting point that can be easily extended via the CSS.</p>
                    </div>
                </div>

                <div class="row featurette">
                    <div class="col-md-7">
                        <h2 class="featurette-heading">Programmable. <span class="text-muted">Rich API</span></h2>
                        <p class="lead">Provides a rich API with settings, methods, cuepoints and callbacks to fit the player to any business model by creating additional logic around it.</p>
                    </div>
                </div>
            </div>
        </div>

        <?php include("footer.php"); ?>

    </body>
</html>