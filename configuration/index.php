<?php
	$nav = 'configuration';

	session_start();

	if(isset($_SESSION['user'])) {
    	$user = $_SESSION['user'];
	}
	if(isset($_SESSION['ident'])) {
    	$ident = $_SESSION['ident'];
	}
	if(isset($_SESSION['email'])) {
    	$email = $_SESSION['email'];
	}
	if(isset($user)) {
		include '../database/database.php';
		$database = database();
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />

	<title>Config | Codo Player</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Configure Codo Player by presetting size, autoplay, loop, volume, image poster and many more.">
	<meta name="copyright" content="Donato Software House" />
	<meta property="og:title" content="Codo Player - A Prime HTML5 FLASH Web Video Player" />
    <meta property="og:image" content="http://codoplayer.com/res/img/codo-player-logo-dark-bg.jpg?v=2" />
    <meta property="og:url" content="http://codoplayer.com/"/>
    <meta property="og:description" content="Easy to setup, highly configurable, cross browser and plays on desktop, tablet and mobile. Now with a powerful VAST advertising plugin to monetise your video content!"/>

	<link rel="canonical" href="http://www.codoplayer.com/">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../res/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/elusive-webfont.css" type="text/css" />
    <link rel="stylesheet" href="../res/lib/prettify/sunburst.css" type="text/css" />
    <link rel="stylesheet" href="../res/css/common.css" type="text/css" />
    <link rel="stylesheet" href="res/css/base.css" type="text/css" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<?php include_once("../analytics.php") ?>

	<?php include("../header.php"); ?>

	<div class="content-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>Config</h1>
                    <h2>Lever the settings for a custom player setup</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="content-1">
       	<div class="container">

       		<div class="row">
				<div class="col-md-12">

					<h3 class="no-margin-top">Snippet structure</h3>

					<p>To configure the player it is essential to understand the elements containing the snippet.</p>
					<h5>Player snippet consists of 2 &lt;script&gt; tags</h5>

<pre class="prettyprint">
&lt;script src="CodoPlayer.js" type="text/javascript">&lt;/script>
&lt;script>
CodoPlayer("video.mp4", {
	width: 600,
	height: 338
})
&lt;/script>
</pre>
					<blockquote>
						<small>First &lt;script&gt; tag defines a link to "CodoPlayer.js" script &amp; must always stay above the player caller method.</small>
						<small>Second &lt;script&gt; tag defines a caller method to initiate the player with content &amp; settings defined.</small>
					</blockquote>

					<h5>Player constructor method accepts 2 parameters</h5>

<pre class="prettyprint">
CodoPlayer( mediaObj , settingsObj )
</pre>

					<blockquote>
						<small>settingsObj is optional (settings default).</small>
					</blockquote>
				</div>
			</div>

		</div>
	</div>

	<div class="content-2">
       	<div class="container">

			<div class="row">
				<div class="col-md-6">

					<h3 class="no-margin-top">Media object (mediaObj)</h3>

					<p>Defines the source URL to media and configures title and poster associated with clip.</p>

					<h4>String</h4>

					<h5>The basic media object setup can be a string</h5>

<pre class="prettyprint">
CodoPlayer("video.mp4")
</pre>

					<h5>To make the playlist of clips - an array of strings</h5>

<pre class="prettyprint">
CodoPlayer(["video.mp4", "video2.mp4", "video3.mp4"])
</pre>

					<h4>Object</h4>
					<h5>For a single clip - an object literal</h5>

<pre class="prettyprint">
CodoPlayer({
	title: "My video",
	src: "video.mp4"
})
</pre>

					<h5>For a playlist of clips - an array of object literals</h5>
<pre class="prettyprint">
CodoPlayer([{
	title: "My video 1",
	src: "video.mp4"
},
{
	title: "My video 2",
	src: "video2.mp4"
}])
</pre>
					<h4>Mixed</h4>

					<h5>For a playlist of clips with first clip as string, second clip as object literal</h5>
<pre class="prettyprint">
CodoPlayer([
	"video.mp4",
{
	title: "My video 2",
	src: "video2.mp4"
}])
</pre>

					<h4>All properties of media object</h4>

<pre class="prettyprint">
CodoPlayer({
	src: ["video.webm", "video.ogv", "video.mp4"],
	title: "My video title",
	poster: "image.jpg",
	engine: "auto",
	cuepoints: [5, 10, 15]
})
</pre>

				</div>

				<div class="col-md-6">

					<h3 class="no-margin-top">Settings object (settingsObj)</h3>

					<p>This object defines player initial properties (width, height, autoplay, volume, +)</p>

					<h5>To customise simply add a new key to a settings object</h5>

<pre class="prettyprint">
CodoPlayer("video.mp4", {
	width: 600,
	height: 338
})
</pre>

					<h4>All properties of settings object</h4>

<pre class="prettyprint">
CodoPlayer('video.mp4', {
	width: 600,
	height: 338,
	preload: true,
	autoplay: false,
	loop: false,
	volume: 80,
	engine: 'auto' / 'html5' / 'flash',
	playlist: true,
	logo: "path_to_image.png", // (PRO)
	loader: "path_to_image.gif", // (PRO)
	controls: true,
	id: "player-1"
})
</pre>

					<h4>Control bar object</h4>
					<h5>Player settings object accepts the following properties for control bar manipulation.</h5>

<pre class="prettyprint">
CodoPlayer('video.mp4', {
	width: 600,
	height: 338,
	controls: {
		show: 'auto' / 'always' / 'never',
		hideDelay: 5,
		play: true,
		playBtn: true,
		seek: true,
		volume: false / 'horizontal' / 'vertical',
		fullscreen: true,
		title: true,
		time: true
	}
})
</pre>

				</div>
			</div>

		</div>
	</div>

	<div class="content-3">
       	<div class="container">

			<div class="row">
				<div class="col-md-12">

					<h3 class="no-margin-top">Complete setup example</h3>

<pre class="prettyprint">
CodoPlayer([{
	src: ["video1.webm", "video1.ogv", "video1.mp4"],
	title: "My video title 1",
	poster: "image1.jpg",
	engine: "auto",
	cuepoints: [5, 10, 15]
},
{
	src: ["video2.webm", "video2.ogv", "video2.mp4"],
	title: "My video title 2",
	poster: "image2.jpg",
	engine: "auto",
	cuepoints: [5, 10, 15]
},
{
	src: ["video3.webm", "video3.ogv", "video3.mp4"],
	title: "My video title 3",
	poster: "image3.jpg",
	engine: "auto",
	cuepoints: [5, 10, 15]
}], {
	width: 600,
	height: 338,
	preload: true,
	autoplay: false,
	loop: false,
	volume: 80,
	engine: "auto",
	playlist: true,
	logo: "path_to_image.png", // PRO version only
	loader: "path_to_image.gif", // PRO version only
	controls: {
		show: "auto",
		hideDelay: 5,
		play: true,
		playBtn: true,
		seek: true,
		volume: true,
		fullscreen: true,
		title: true,
		time: true
	},
	id: "player-1"
})
</pre>

				</div>
			</div>

		</div>
	</div>

	<?php include("../footer.php"); ?>

	<script src="../res/lib/jquery/jquery-1.11.0.min.js"></script>
    <script src="../res/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="../res/lib/angular/angular.min.js" type="text/javascript"></script>
    <script src="../res/lib/prettify/prettify.js"></script>
    <script src="../res/js/common.js"></script>
    <script src="res/js/base.js" type="text/javascript"></script>

</body>
</html>