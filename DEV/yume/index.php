<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Codo Player &amp; Yume</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <!-- <link href="" rel="stylesheet"> -->
    </head>
    <body>

        <h1>Codo Player &amp; Yume</h1>

        <script src="CodoPlayerPro-2.1/CodoPlayer.js" type="text/javascript"></script>
        <script type="text/javascript">

            CodoPlayer("CodoPlayerPro-2.1/example/video.mp4", {
                width: 600,
                height: 338,
                plugins: {
                    advertising: {
                        title: "Advertisement",
                        timerText: "Your video will play in %time% s.", // %time% replaced with counter
                        servers: [{
                            apiAddress: "http://shadowcdn-01.yumenetworks.com/ym/1B3uA91O2152/1349/HifvrHol/vpaid_as3.xml" // Ads server url returning VAST .xml
                        }],
                        schedule: [{
                            position: "pre-roll"
                        }],
                        skipAd: {
                            enabled: true,
                            timeout: 5,
                            text: "Skip"
                        },
                        fallback: function() {
                            // If Ad Blocker enabled or otherwise advertising can not be served
                        },
                        logging: true
                    }
                }
            });

        </script>

    </body>
</html>