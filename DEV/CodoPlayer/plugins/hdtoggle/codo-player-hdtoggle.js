/*
	Codo Player 'hdtoggle' plugin
	Copyright (C) 2012-2013 Donato Software House
*/

(function() {
	var file = "codo-player-hdtoggle.js";
	Codo().link(Codo().getScriptTag(file).src.split("?")[0].replace(file, "") + "styles/style.css");
	for(var i = 0; i < CodoPlayerAPI.length; i++) {
		if (CodoPlayerAPI[i].plugins && CodoPlayerAPI[i].plugins.hdtoggle) {
			var api = CodoPlayerAPI[i];
			var wrap = Codo(api.DOM.parent).add({el: "div", class: "codo-player-share-wrap"});

			console.log(api)
			    // HD toggle
    var HDToggle = function() {

        var on = false,
            inited = false,
            hdBtn,
            timeStamp;

        	api.media.onLoad(function() {
        		console.log("jo")
        	})

        function Toggle() {
            if (!on) {
                hdBtn.style.fontWeight = "bold";
                system.playlist.toggleHD();
                on = true;
            } else {
                hdBtn.style.fontWeight = "normal";
                system.playlist.toggleHD();
                on = false;
            }
        }

        hdBtn = Codo(controlDiv).add({
            el: "div",
            id: system.id + "-controls-hd-button",
            className: system.className + "-controls-hd-button",
            innerHTML: "HD"
        });
        Codo(hdBtn).on("click", function(e) {
            Toggle();
        });

    }();

		}
	};
})();
