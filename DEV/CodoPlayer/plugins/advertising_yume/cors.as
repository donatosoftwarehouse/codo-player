/*
    Codo Player
    codoplayer.com

    Copyright (C) Donato Software House
*/

package {
    import flash.events.*;
    import flash.display.*;
    import flash.ui.*;
    import flash.media.*;
    import flash.net.*;
    import flash.external.*;
    import adobe.utils.*;
    import flash.accessibility.*;
    import flash.errors.*;
    import flash.filters.*;
    import flash.geom.*;
    import flash.printing.*;
    import flash.system.*;
    import flash.text.*;
    import flash.utils.*;
    import flash.xml.*;
    import flash.display.Sprite;

    import flash.system.Security;
    import flash.external.ExternalInterface;

    public class cors extends MovieClip {

        private var instance:String;
        private var vast:String;

        public function cors():void {

            Security.allowInsecureDomain("*");
            Security.allowDomain("*");

            instance = LoaderInfo(this.root.loaderInfo).parameters.instance;
            vast = LoaderInfo(this.root.loaderInfo).parameters.vast;

            var xmlString:URLRequest = new URLRequest(vast);
            var xmlLoader:URLLoader = new URLLoader(xmlString);
            xmlLoader.addEventListener("complete", init);

            function init(event:Event):void {
                ExternalInterface.call("CodoPlayerAPI[" + instance + "].plugins.advertising.system.loadXML", instance, xmlLoader.data);
            }

        };

    }
}