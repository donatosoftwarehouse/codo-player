package {

    import flash.events.*;
    import flash.display.*;
    import flash.ui.*;
    import flash.media.*;
    import flash.net.*;
    import flash.external.*;
    import adobe.utils.*;
    import flash.accessibility.*;
    import flash.errors.*;
    import flash.filters.*;
    import flash.geom.*;
    import flash.printing.*;
    import flash.system.*;
    import flash.text.*;
    import flash.utils.*;

    [SWF(backgroundColor="0x000000")]
    public class VPAID extends MovieClip {

        private var instance:String;
        private var logging:String;

        private var yume_ad_mc : MovieClip;
        private var yume_par_obj : Object;

        public function VPAID():void {

            Security.allowInsecureDomain("*");
            Security.allowDomain("*");

            instance = LoaderInfo(this.root.loaderInfo).parameters.instance;
            logging = LoaderInfo(this.root.loaderInfo).parameters.logging;

            stage.align = StageAlign.TOP_LEFT;
            stage.scaleMode = StageScaleMode.NO_SCALE;

            function InitAd(w:Number, h:Number, d:String, params:String, vol:Number, handShakeVersion:String):void {

                // Loader
                var vpaidLoader:Loader = new Loader();
                vpaidLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadCompleteHandler);
                var vpaidContainerURLRequest:URLRequest = new URLRequest(d);
                vpaidLoader.load(vpaidContainerURLRequest);

                //~~~~~ Grab VPAID <AdParameters> as a String ~~~~~//
                var vpaidAdParameters:String = params;

                // Set Up VPAID Ad //

                var vpaidAd:Object;

                //~~~~~ Create Strings for VPAID ~~~~~//
                var vpaidCreativeData:String;
                var vpaidEnvironmentVars:String;

                //~~~~~ Set Width and Height for VPAID Container ~~~~~//
                var vpaidWidth:Number = w;
                var vpaidHeight:Number = h;

                function loadCompleteHandler(e:Event):void {

                    var loaderInfo:LoaderInfo = e.target as LoaderInfo;
                    yume_ad_mc = e.target.content;

                    yume_ad_mc.yume_ad.addEventListener("ad_playing",ad_playing);
                    yume_ad_mc.yume_ad.addEventListener("ad_absent",ad_absent);
                    yume_ad_mc.yume_ad.addEventListener("ad_completed",ad_absent);
                    yume_ad_mc.yume_ad.addEventListener("image_closed",image_closed);
                    yume_ad_mc.yume_ad.addEventListener("ad_closed",ad_closed);
                    yume_ad_mc.yume_ad.addEventListener("ad_reopened",ad_reopened);
                    yume_ad_mc.yume_ad.addEventListener("ad_clicked2site",ad_clicked2site);
                    yume_ad_mc.yume_ad.addEventListener("ad_clicked2video",ad_clicked2video);
                    yume_ad_mc.yume_ad.addEventListener("pip_video_playing",pip_video_playing);
                    yume_ad_mc.yume_ad.addEventListener("pip_video_completed",pip_video_completed);
                    yume_ad_mc.yume_ad.addEventListener("ad_error",ad_error);

                    addChild(yume_ad_mc);

                    ExternalInterface.call("Codo().log", "addChild");

                    yume_init_ad();

                    ExternalInterface.call("Codo().log", "yume_init_ad");

                }

                function yume_init_ad() : void
                    {

                        yume_par_obj = new Object();

                        // parameters needed for the YuMe Network Library to make request for a preroll ad
                        yume_par_obj.parent_mc = yume_ad_mc;

                        yume_par_obj.ad_type = "before_content";

                        yume_par_obj.playlist = 'http://plg4.yumenetworks.com/dynamic_preroll_playlist.vast2xml?domain=1089ajnjCrjp&version=v2&placement_id=210944&advertisement_id=101164'; // this.root is a URL
                        yume_par_obj.update_interval = 1000;
             // sets the interval in milliseconds that the YuMe Ad Player will issue the "ad playing" event
                        yume_par_obj.ad_volume = 80;                       // [0-100]


                        yume_par_obj.fullscreen_x = 0;
                        yume_par_obj.fullscreen_y = 0;
                        yume_par_obj.fullscreen_width = 1024;
            // If you know the size beforehand, set these parameters.
                        yume_par_obj.fullscreen_height = 710;
            // Otherwise, set them at fullscreen_width=1024 and fullscreen_height=710

                        yume_ad_mc.yume_ad.start_ad(yume_par_obj);
                    }

                function adLoadedHandler(alEvent:Event):void{
                    if(logging == "true") ExternalInterface.call("Codo().log", "adLoadedHandler");
                    vpaidAd.startAd();
                    vpaidAd.adVolume = vol/100;
                }

                function adStoppedHandler(asEvent:Event):void{
                    if(logging == "true") ExternalInterface.call("Codo().log", "adStoppedHandler");
                    removeChild(vpaidAd as DisplayObject);
                    ExternalInterface.call("CodoPlayerAPI[" + instance + "].plugins.advertising.VPAIDRemove()");
                }

                function adErrorHandler(aeEvent:Event):void{
                    if(logging == "true") ExternalInterface.call("Codo().log", "adErrorHandler");
                    //trace("AdError: " + Object(aeEvent).message);
                    removeChild(vpaidAd as DisplayObject);
                    ExternalInterface.call("CodoPlayerAPI[" + instance + "].plugins.advertising.VPAIDErrorRemove()");
                }

                function adClickHandler(url:String, id:String, playerEvent:Boolean):void{
                    if(logging == "true") ExternalInterface.call("Codo().log", "adClickHandler");
                    ExternalInterface.call("console.log", "adClickHandler");
                }

                function adLogHandler(id:String):void{
                    if(logging == "true") ExternalInterface.call("Codo().log", "adLogHandler");
                }

            }

            if(logging == "true") ExternalInterface.call("Codo().log", "VPAID started");

            ExternalInterface.addCallback("initAd", InitAd);
            ExternalInterface.call("CodoPlayerAPI[" + instance + "].plugins.advertising.VPAIDLoaded()");

        }

        private function ad_absent( e:Event ):void {
            ExternalInterface.call("Codo().log", "ad_absent");
        }

        private function ad_error( e:Event ):void {
            ExternalInterface.call("Codo().log", "ad_error");
        }

        private function ad_completed( e:Event ):void {
            ExternalInterface.call("Codo().log", "ad_completed");
        }

        private function ad_playing( e:Event ):void {
            ExternalInterface.call("Codo().log", "ad_playing");
        }

        private function ad_present( e:Event ):void {
            ExternalInterface.call("Codo().log", "ad_present");
        }

        private function image_closed( e:Event ):void {
            ExternalInterface.call("Codo().log", "image_closed");
        }

        private function ad_closed( e:Event ):void {
            ExternalInterface.call("Codo().log", "ad_closed");
        }

        private function ad_reopened( e:Event ):void {
            ExternalInterface.call("Codo().log", "ad_reopened");
        }

        private function ad_clicked2site( e:Event ):void {
            ExternalInterface.call("Codo().log", "ad_clicked2site");
        }

        private function ad_clicked2video( e:Event ):void {
            ExternalInterface.call("Codo().log", "ad_clicked2video");
        }

        private function pip_video_playing( e:Event ):void {
            ExternalInterface.call("Codo().log", "pip_video_playing");
        }

        private function pip_video_completed( e:Event ):void {
            ExternalInterface.call("Codo().log", "pip_video_completed");
        }

    }



}