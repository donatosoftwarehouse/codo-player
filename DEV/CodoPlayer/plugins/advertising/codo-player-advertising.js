(function() {

    "use strict";

    function GetUrl(tag) {
        var data = null;
        if (tag.data && tag.data.replace(/^\s+/, "").replace(/\s+$/, "").length > 0) {
            data = tag.data.replace(/^\s+/, "").replace(/\s+$/, "");
        } else if (tag.childNodes[0] && tag.childNodes[0].data && tag.childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "").length > 0) {
            data = tag.childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "");
        }
        return data;
    }

    function Xml2Doc(string) {

        if (!string)
            return;

        var message = "", xmlDoc;
        if (window.DOMParser) { // all browsers, except IE before version 9
            var parser = new DOMParser();
            try {
                xmlDoc = parser.parseFromString(string, "text/xml");
            } catch (e) {
                Codo().log("Xml2Doc: XML parsing error.")
                return;
            }
        } else { // Internet Explorer before version 9
            if (typeof(ActiveXObject) == "undefined") {
                Codo().log("Xml2Doc: Cannot create XMLDocument object")
                return;
            }
            ids = ["Msxml2.DOMDocument.6.0", "Msxml2.DOMDocument.5.0", "Msxml2.DOMDocument.4.0", "Msxml2.DOMDocument.3.0", "MSXML2.DOMDocument", "MSXML.DOMDocument"];
            for (var i = 0, il = ids.length; i < il; ++i) {
                try {
                    xmlDoc = new ActiveXObject(ids[i]);
                    break;
                } catch (e) {}
            }
            if (!xmlDoc) {
                Codo().log("Xml2Doc: Cannot create XMLDocument object")
                return;
            }
            xmlDoc.loadXML(string);

            if (xmlDoc.parseError && xmlDoc.parseError.errorCode !== 0) {
                Codo().log("Xml2Doc: XML Parsing Error: " + xmlDoc.parseError.reason + " at line " + xmlDoc.parseError.line + " at position " + xmlDoc.parseError.linepos)
                return;
            } else {
                if (xmlDoc.documentElement) {
                    if (xmlDoc.documentElement.nodeName == "parsererror") {
                        Codo().log("Xml2Doc: " + xmlDoc.documentElement.childNodes[0].nodeValue)
                    }
                } else {
                    Codo().log("Xml2Doc: XML Parsing Error")
                }
            }
        }

        return xmlDoc;
    }

    var Advertising = function(api) {

        var advertising, system, click, position;

        function Click() {

            var timerText = api.plugins.advertising.timerText || "Your video will play in %time% s.";
            var text = (api.plugins.advertising.skipAd && api.plugins.advertising.skipAd.text) ? api.plugins.advertising.skipAd.text : "Skip";

            var click = Codo(api.DOM.containerScreen).add({
                el: "div",
                id: api.id + "-ad-click-space",
                className: api.className + "-ad-click-space",
                style: "position: absolute; top: 0; left: 0; width: 100%; height: 100%; display: none;",
            });
            Codo(click).on("click", function(e) {
                if (system.currentSlot && system.currentSlot.link) {
                    api.media.pause();
                    window.top.open(system.currentSlot.link, "_blank");
                    Controller.onClick();
                }
            });
            var timer = Codo(click).add({
                el: "p",
                id: api.id + "-ad-timer",
                className: api.className + "-ad-timer"
            });
            var skip = Codo(click).add({
                el: "div",
                id: api.id + "-ad-skip-button",
                className: api.className + "-ad-skip-btn",
                style: "display: none",
                innerHTML: text
            });
            Codo(skip).on("click", function(e) {
                Controller.onSkip();
            });

            function SetTimeLeft() {
                if (advertising.isAd) {
                    var secs = Math.round(api.media.getDuration() - api.media.getCurrentTime());
                    if(secs) timer.innerHTML = timerText.replace("%time%", secs);
                }
            }

            return {

                show: function() {
                    if(api.plugins.advertising.type == "VAST") {
                        api.DOM.overlay.style.display = "none";
                        click.style.display = "block";
                    }
                },

                hide: function() {
                    api.DOM.overlay.style.display = "block";
                    click.style.display = "none";
                    this.hideSkip();
                },

                setTimerText: function(text) {
                    timerText = text;
                },

                setTimeLeft: function() {
                    SetTimeLeft();
                },

                showSkip: function() {
                    skip.style.display = "block";
                },

                hideSkip: function() {
                    skip.style.display = "none";
                },

                setText: function(text) {
                    text = text;
                }

            };

        };



        /*
            NONLINEAR AD
        */

        var NonLinear = function() {

            var
                firstSkip = false,
                adParent,
                adHeader,
                adHeaderText,
                closeBtn,
                aTag,
                adTag;

            function Init(slot) {

                var extensionsArr = slot.extensions.getElementsByTagName("Extension");

                api.plugins.advertising.type = "NonLinear";
                var width = slot.NonLinear.getAttribute("width"),
                height = slot.NonLinear.getAttribute("height");

                if(width < 300 || height < 200) {
                    width = "auto";
                    height = "auto";
                }

                width = (width <= api.settings.currentWidth) ? width : api.settings.currentWidth;
                height = (height <= api.settings.currentHeight) ? height : api.settings.currentHeight;

                adParent = Codo(api.DOM.container).add({
                    el: "div",
                    className: api.className + "-ad-nonlinear-parent",
                    style: "position: absolute; top: " + (api.settings.currentHeight / 2 - height / 2) + "px; left: " + (api.settings.currentWidth / 2 - width / 2) + "px; width: " + width + "px; height: " + height + "px; display: none;"
                });

                if(extensionsArr && extensionsArr[0].getElementsByTagName("ConversionUrl")) {
                    var ConversionImgTag = Codo(adParent).add({
                        el: "img",
                        src: extensionsArr[0].getElementsByTagName("ConversionUrl")[0].childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, ""),
                        style: "position: absolute;"
                    });
                }

                adHeader = Codo(adParent).add({
                    el: "div",
                    className: api.className + "-ad-nonlinear-header"
                });

                adHeaderText = Codo(adHeader).add({
                    el: "a",
                    className: api.className + "-ad-nonlinear-header",
                    href: extensionsArr[0].getElementsByTagName("attribution_url")[0].getAttribute("data"),
                    target: "_blank",
                    innerHTML: extensionsArr[0].getElementsByTagName("attribution_text")[0].getAttribute("data") || "Advertisement"
                });

                closeBtn = Codo(adHeader).add({
                    el: "div",
                    className: api.className + "-ad-nonlinear-close-btn",
                    innerHTML: "&#10005;"
                });

                aTag = Codo(adParent).add({
                    el: "a",
                    href: slot.NonLinear.getElementsByTagName("NonLinearClickThrough")[0].childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, ""),
                    target: "_blank"
                });

                switch(slot.mime) {

                    case "image/jpeg":
                    case "image/gif":
                    case "image/png":
                        adTag = Codo(aTag).add({
                            el: "img",
                            id: api.id + "-ad-nonlinear",
                            src: slot.source,
                            width: width,
                            height: height
                        });
                    break;

                    case "application/x-shockwave-flash":
                        adTag = Codo(aTag).add({
                            el: "div",
                            id: api.id + "-ad-nonlinear",
                            innerHTML: "<object width='" + width + "' height='" + height + "' type='application/x-shockwave-flash' data='" + slot.source + "'><param name='movie' value='" + slot.source + "'><param name='quality' value='high'><param name='allowScriptAccess' value='always'><param name='wmode' value='transparent'><param name='swliveconnect' value='true'></object>"
                        });
                    break;

                    case "text":

                        adTag = Codo(aTag).add({
                            el: "div",
                            id: api.id + "-ad-nonlinear",
                            style: "padding: 10px;"
                        });

                        adParent.style.height = "auto";

                        for (var i = 0; i < extensionsArr[1].getElementsByTagName("*").length; i++) {
                            adTag.innerHTML += extensionsArr[1].getElementsByTagName("*")[i].innerHTML
                        };

                    break;

                }

                Codo(closeBtn).on("click", function(e) {
                    NonLinear.destroy(this);
                });

            }

            return {

                setFirstSkip: function(bool) {

                    firstSkip = bool;

                },

                getFirstSkip: function() {

                    return firstSkip;

                },

                init: function(slot) {

                    Init(slot);

                },

                show: function() {

                    if(adParent) adParent.style.display = "block";

                },

                hide: function(that) {

                    if(that.parentNode.parentNode) that.parentNode.parentNode.style.display = "none";

                },

                destroy: function() {
                    position = undefined;
                    if(adParent) adParent = Codo(adParent).remove();

                }

            }

        }();

        var Controller = function() {

            var skipAd = {}, clickReady;

            return {
                init: function() {
                    this.initURL();
                    this.initPlayerEvents();
                },
                initURL: function(url) {

                    var self = this;

                    if (!advertising || !advertising.schedule || !advertising.schedule.length || !advertising.servers || !advertising.servers.length) {
                        //this.removeCover();
                        return;
                    }

                    skipAd = {
                        enabled: (advertising.skipAd && advertising.skipAd.enabled) ? advertising.skipAd.enabled : false,
                        timeout: (advertising.skipAd && advertising.skipAd.timeout) ? advertising.skipAd.timeout : 5
                    };

                    system.tempTime = 0;
                    system.adList = [];
                    system.mainTrack = api.playlist.getCurrentClip();
                    system.currentSlot = null;
                    system.source = url || advertising.servers[0].apiAddress;
                    system.muted = api.settings.volume ? false : true;

                    advertising.isAd = advertising.clipAfterAd = false;

                    Codo().load({
                        url: system.source,
                        contentType: "text/xml;charset=UTF-8"
                    }, function(response, obj) {

                        var api = obj.api,
                        system = obj.system;

                        if (response != "error") {
                            self.constructAdList(response);
                        } else {

                            if(Codo().isFlash()) {
                                Codo(api.DOM.parent).add({
                                    el: "div",
                                    style: "position: absolute",
                                    innerHTML: "<object name='cors' width='1' height='1' type='application/x-shockwave-flash' data='" + api.system.rootPath + "plugins/advertising/cors.swf'><param name='movie' value='" + api.system.rootPath + "plugins/advertising/cors.swf'><param name='quality' value='high'><param name='allowScriptAccess' value='always'><param name='wmode' value='transparent'><param name='swliveconnect' value='true'><param name='flashVars' value='instance=" + api.instance + "&vast=" + escape(system.source) + "'></object>"
                                });
                            } else {
                                self.adsError();
                            }
                        }

                    }, {
                        api: api,
                        system: system
                    });
                },
                initXML: function(xml) {
                    this.constructAdList(xml);
                },

                adsError: function() {
                    var self = this;
                    var src = system.source;
                    var protocol = location.protocol,
                        hostname = location.hostname,
                        exRegex = RegExp(protocol + "//" + hostname),
                        query = "select * from xml where url='" + src + "'",
                        YQL = "http" + (/^https/.test(protocol) ? "s" : "") + "://query.yahooapis.com/v1/public/yql?format=xml&q=" + query;
                    if (!exRegex.test(src) && /:\/\//.test(src)) {
                        Codo().load({
                            url: YQL
                        }, function(response) {
                            self.constructAdList(response);
                        });
                    }
                },

                constructAdList: function(response) {

                    var self = this;

                    if(advertising.logging) {
                        Codo().log("\nCODO PLAYER ADVERTISING PLUGIN (LOGGING)");
                        Codo().log("--------------------");
                        Codo().log("Plugin object\n-");
                        Codo().log(advertising);
                        Codo().log("--------------------");
                    }

                    // Parse XML string
                    response = Xml2Doc(response);

                    if(advertising.logging) {
                        Codo().log("XML loaded\n-");
                        Codo().log(response);
                        Codo().log("--------------------");

                        Codo().log("VAST template\n-");
                    }

                    this.setPosition();

                    // Get VAST version

                    try {
                        var handShakeVersion = response.getElementsByTagName("VAST")[0].getAttribute("version");
                        advertising.system.handShakeVersion = handShakeVersion;
                        if(advertising.logging) Codo().log("handShakeVersion: " + handShakeVersion);
                    } catch (e) {
                        if(advertising.logging) Codo().log("Warning (VAST version not found): " + e);
                    }

                    if(advertising.logging) {
                        Codo().log("--------------------");
                    }

                    // Loop through "Ad" tags

                    var adTags = response.getElementsByTagName("Ad");

                    if(!adTags.length) {
                        if(advertising.logging) Codo().log("Error: No 'Ad' tag found.");
                        return;
                    }

                    var adTag = adTags[0];

                    for (var a = 0; a < system.adList.length; a++) {

                        if(advertising.logging) {
                            Codo().log("AD " + a + "\n-");
                        }

                        try {
                            var adId = adTag.getAttribute("id");
                            system.adList[a].id = adId;
                            if(advertising.logging) Codo().log("ID: " + adId);
                        } catch (e) {
                            if(advertising.logging) Codo().log("Error (ID not found): " + e);
                        }

                        try {
                            var adSystem = adTag.getElementsByTagName("AdSystem")[0].childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "");
                            system.adList[a].adSystem = adSystem;
                            if(advertising.logging) Codo().log("AdSystem: " + adSystem);
                        } catch (e) {
                            if(advertising.logging) Codo().log("Warning (AdSystem not found): " + e);
                        }

                        try {
                            var adTitle = adTag.getElementsByTagName("AdTitle")[0].childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "");
                            system.adList[a].adTitle = adTitle;
                            if(advertising.logging) Codo().log("AdTitle: " + adTitle);
                        } catch (e) {
                            if(advertising.logging) Codo().log("Warning (AdTitle not found): " + e);
                        }

                        try {
                            var impression = adTag.getElementsByTagName("Impression");
                            for (var k = 0, kl = impression.length; k < kl; ++k) {
                                for (var l = 0, ll = impression[k].childNodes.length; l < ll; ++l) {
                                    var impressionData = impression[k].childNodes[l].data.replace(/^\s+/, "").replace(/\s+$/, "");
                                    if (!impressionData)
                                        continue;

                                    system.adList[a].impression.push(impressionData);
                                }
                            }
                        } catch (e) {

                            if(advertising.logging) Codo().log("Warning (impression): " + e);

                        }

                        try {
                            var mediaFiles = adTag.getElementsByTagName("MediaFiles")[0];
                            var mediaFile = mediaFiles.getElementsByTagName("MediaFile");

                            mediaFound: for (var k = 0, kl = mediaFile.length; k < kl; ++k) {
                                try {

                                    for (var l = 0, ll = mediaFile[k].childNodes.length; l < ll; ++l) {
                                        var mediaData = null;
                                        if (mediaFile[k].childNodes[l].data && mediaFile[k].childNodes[l].data.replace(/^\s+/, "").replace(/\s+$/, "").length > 0) {
                                            mediaData = mediaFile[k].childNodes[l].data.replace(/^\s+/, "").replace(/\s+$/, "");
                                        } else if (mediaFile[k].childNodes[l].childNodes[0]) {
                                            mediaData = mediaFile[k].childNodes[l].childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "");
                                        }
                                        if (!mediaData || mediaData.length === 0) {
                                            continue;
                                        }

                                        var
                                            type = mediaFile[0].getAttribute("type"),
                                            apiFramework = mediaFile[0].getAttribute("apiFramework");

                                        system.adList[a].VPAID = (apiFramework == "VPAID" || type === "application/x-shockwave-flash") ? mediaFile : undefined;

                                        system.adList[a].source = mediaData;
                                        system.adList[a].mime = type;

                                        if(advertising.logging) Codo().log("MediaFile: " + mediaData);

                                    }
                                } catch (e) {
                                    if(advertising.logging) Codo().log("Error (mediaFound): " + e);
                                }
                            }
                        } catch (e) {
                            if(advertising.logging) Codo().log("Error (mediaFiles): " + e);
                        }

                        // Non linear
                        try {

                            var nonLinear = adTag.getElementsByTagName("NonLinear")[0];
                            var staticResource = nonLinear.getElementsByTagName("StaticResource")[0];

                            var extensions = adTag.getElementsByTagName("Extensions")[0];

                            system.adList[a].NonLinear = nonLinear;
                            system.adList[a].extensions = extensions;
                            system.adList[a].source = staticResource.childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "");
                            system.adList[a].mime = staticResource.getAttribute("creativeType");

                            if(system.adList[a].source) {

                                NonLinear.init(system.adList[a]);

                                if(system.adList[0].type === "pre-roll") {
                                    NonLinear.show();
                                }

                            }

                            this.removeCover();

                        } catch (e) {
                            if(advertising.logging) Codo().log("Warning (nonLinear not found): " + e);
                        }

                        try {
                            var adParameters = adTag.getElementsByTagName("AdParameters");
                            system.adList[a].adParameters = adParameters[0].childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "");
                        } catch (e) {
                            if(advertising.logging) Codo().log("Warning (adParameters not found): " + e);
                        }

                        try {
                            var trackingEvents = adTag.getElementsByTagName("TrackingEvents")[0];
                            var tracking = trackingEvents.getElementsByTagName("Tracking");
                            for (var k = 0, kl = tracking.length; k < kl; ++k) {
                                var event = tracking[k].getAttribute("event");
                                if (!event)
                                    continue;
                                for (var l = 0, ll = tracking[k].childNodes.length; l < ll; ++l) {
                                    var trackingData = null;
                                    if (tracking[k].childNodes[l].data && tracking[k].childNodes[l].data.replace(/^\s+/, "").replace(/\s+$/, "").length > 0) {
                                        trackingData = tracking[k].childNodes[l].data.replace(/^\s+/, "").replace(/\s+$/, "");
                                    } else if (tracking[k].childNodes[l] && tracking[k].childNodes[l].childNodes[0] && tracking[k].childNodes[l].childNodes[0].data && tracking[k].childNodes[l].childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "")) {
                                        trackingData = tracking[k].childNodes[l].childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "");
                                    }

                                    if (!trackingData)
                                        continue;
                                    if (!system.adList[a].events[event.toLowerCase()])
                                        system.adList[a].events[event.toLowerCase()] = [];
                                    system.adList[a].events[event.toLowerCase()].push(trackingData);
                                }
                            }
                        } catch (e) {
                            if(advertising.logging) Codo().log("Warning (trackingEvents not found): " + e);
                        }

                        try {
                            var videoClicks = adTag.getElementsByTagName("VideoClicks")[0];
                            var clickThrough = videoClicks.getElementsByTagName("ClickThrough")[0];
                            for (var k = 0, kl = clickThrough.childNodes.length; k < kl; ++k) {
                                var clickData = null;
                                if (clickThrough.childNodes[k].data && clickThrough.childNodes[k].data.replace(/^\s+/, "").replace(/\s+$/, "").length > 0) {
                                    clickData = clickThrough.childNodes[k].data.replace(/^\s+/, "").replace(/\s+$/, "");
                                } else if (clickThrough.childNodes[k].childNodes[0] && clickThrough.childNodes[k].childNodes[0].data && clickThrough.childNodes[k].childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "").length > 0) {
                                    clickData = clickThrough.childNodes[k].childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "");
                                }
                                if (!clickData)
                                    continue;
                                if (clickData.toUpperCase().indexOf("HTTP://") !== 0 && clickData.toUpperCase().indexOf("HTTPS://") !== 0)
                                    continue;
                                system.adList[a].link = clickData;
                            }
                            var clickTracking = videoClicks.getElementsByTagName("ClickTracking");
                            for (var k = 0, kl = clickTracking.length; k < kl; ++k) {
                                for (var l = 0, ll = clickTracking[k].childNodes.length; l < ll; ++l) {
                                    var clickEvent = clickTracking[k].childNodes[l].data.replace(/^\s+/, "").replace(/\s+$/, "");
                                    if (!clickEvent)
                                        continue;
                                    system.adList[a].clickEvents.push(clickEvent);
                                }
                            }
                        } catch (e) {
                            if(advertising.logging) Codo().log("Warning (videoClicks not found): " + e);
                        }

                        try {

                            var companionAds = adTag.getElementsByTagName("Companion");

                            if (companionAds) {

                                system.adCompanionList = [];

                                for (var i = 0; i < companionAds.length; i++) {

                                    var source, click,
                                        sourceTags = companionAds[i].getElementsByTagName("StaticResource"),
                                        clickTags = companionAds[i].getElementsByTagName("CompanionClickThrough");

                                    if (sourceTags && clickTags) {
                                        source = sourceTags[0];
                                        click = clickTags[0];
                                    } else return false;

                                    var clearSource = GetUrl(source);
                                    var clearClick = GetUrl(click);

                                    system.adCompanionList.push({
                                        source: clearSource,
                                        click: clearClick
                                    });

                                    if(Codo(".codo-player-ad-companion-" + i).get()) {
                                        Codo(".codo-player-ad-companion-" + i).get().innerHTML = "";
                                        var aTag = Codo(".codo-player-ad-companion-" + i).add({
                                            el: "a",
                                            href: clearClick,
                                            target: "_blank"
                                        });

                                        var width = companionAds[i].getAttribute("width"),
                                            height = companionAds[i].getAttribute("height");

                                        if(sourceTags[0].getAttribute("creativeType") === "application/x-shockwave-flash") {
                                            aTag.innerHTML = "<object width='" + width + "' height='" + height + "' type='application/x-shockwave-flash' data='" + clearSource + "'><param name='movie' value='" + clearSource + "'><param name='quality' value='high'><param name='allowScriptAccess' value='always'><param name='wmode' value='transparent'><param name='swliveconnect' value='true'></object>";
                                        } else if(sourceTags[0].getAttribute("creativeType") === "image/jpeg") {
                                            Codo(aTag).add({
                                                el: "img",
                                                src: clearSource,
                                                width: width,
                                                height: height
                                            });
                                        }

                                        Codo(aTag).on("click", function(e) {
                                            e.preventDefault();
                                            self.callEvent(clearClick);
                                            window.top.open(clearClick, "_blank");
                                        });
                                    }
                                }

                            }

                        } catch (e) {
                            if(advertising.logging) Codo().log("Warning (companionAds not found): " + e);
                        }

                        try {

                            var redirectUrl = adTag.getElementsByTagName("VASTAdTagURI")[0].childNodes[0].data.replace(/^\s+/, "").replace(/\s+$/, "");
                            if(redirectUrl) {
                                system.loadRedirectURL(redirectUrl);
                            }

                        } catch(e) {
                            if(advertising.logging) Codo().log("Warning (redirectUrl not found): " + e);
                        }

                    }

                    if(advertising.logging) {
                        Codo().log("--------------------");
                        Codo().log("Loading AD content\n-");
                    }

                    clickReady = true;

                    if(api.plugins.advertising.autoplay) {
                        if(system.adList[0].type === "pre-roll") {
                            Controller.checkSlot();
                        }
                    }

                    if(system.adList && !system.adList[0].source) {
                        Controller.removeCover();
                    }

                },

                removeCover: function() {
                    if(api.plugins.advertising.initCover) {
                        Codo(api.plugins.advertising.initCover).remove();
                        api.plugins.advertising.initCover = null;
                    }
                },

                setPosition: function() {

                    var self = this;

                    for (var v in advertising.schedule) {
                        switch (advertising.schedule[v].position) {

                            case "pre-roll":
                                var a = this.adSlot("pre-roll", 0);
                                system.adList.push(a);

                                if(!api.plugins.advertising.autoplay) {
                                    Codo(api.plugins.advertising.initCover).on("click", function(e) {
                                        if(!clickReady) return;
                                        Controller.checkSlot();
                                        self.removeCover();
                                    });
                                }

                                break;

                            case "mid-roll":
                                var a = this.adSlot("mid-roll", this.convertTime(advertising.schedule[v].startTime));
                                system.adList.push(a);
                                self.removeCover();
                                break;

                            case "post-roll":
                                var a = this.adSlot("post-roll", -1);
                                system.adList.push(a);
                                self.removeCover();
                                break;

                            case "auto:bottom":
                                var a = this.adSlot("auto:bottom", this.convertTime(advertising.schedule[v].startTime));
                                system.adList.push(a);
                                self.removeCover();
                                break;

                            default:
                                break;
                        }
                    }

                },

                adSlot: function(type, time) {
                    var a = {};
                    a.type = type;
                    a.time = time;
                    a.source = "";
                    a.mime = "";
                    a.seen = false;
                    a.impression = [];
                    a.events = {};
                    a.link = "";
                    a.clickEvents = [];
                    return a;
                },

                convertTime: function(hhmmss) {
                    return hhmmss.substr(0, 1) * 3600 + hhmmss.substr(3, 2) * 60 + hhmmss.substr(6, 2) * 1;
                },

                callEvent: function(url) {
                    if (url.toUpperCase().indexOf("HTTP://") === 0) {
                        var rnd = Math.round(Math.random() * 100000),
                        imgHandle = document.createElement("img");
                        imgHandle.style.visibility = "hidden";
                        imgHandle.style.position = "absolute";
                        imgHandle.style.width = "0";
                        imgHandle.style.height = "0";
                        imgHandle.src = url + ((url.indexOf("?") > 0) ? "&" : "?") + "r" + rnd + "=" + rnd;
                        api.DOM.containerScreen.appendChild(imgHandle);
                    }
                },

                slotCurrentTime: function(currentTime) {
                    for (var v in system.adList) {
                        if (!system.adList[v].seen) {
                            if (system.adList[v].time == currentTime) {
                                return system.adList[v];
                            }
                        }
                    }
                    return null;
                },

                setPostRoll: function() {
                    for (var v in system.adList) {
                        if (system.adList[v].type == "post-roll") {
                            system.adList[v].time = Math.floor(api.media.getDuration());
                        }
                    }
                },

                initPlayerEvents: function() {

                    var self = this;

                    api.media.onProgress(function() {
                        if (!advertising.isAd) {
                            self.checkSlot();
                        } else {
                            self.onProgress();
                        }
                        click.setTimeLeft();
                    });

                    api.media.onMetaData(function() {
                        if (!advertising.isAd && advertising.clipAfterAd) {
                            if(!api.playlist.getCurrentClip().rtmp) api.media.setCurrentTime(system.tempTime);
                            api.media.play();
                            system.tempTime = 0;
                        } else if (advertising.isAd) {
                            api.media.play();
                            self.onStart();
                        }
                        self.setPostRoll();
                        click.setTimeLeft();
                    });

                    api.media.onClipFirstQuarter(function() {
                        if (advertising.isAd) self.onFirstQuartile();
                    });

                    api.media.onClipSecondQuarter(function() {
                        if (advertising.isAd) self.onMidPoint();
                    });

                    api.media.onClipThirdQuarter(function() {
                        if (advertising.isAd) self.onThirdQuartile();
                    });

                    api.media.onBeforeLoad(function() {

                        if(!advertising.isAd && advertising.clipAfterAd) {
                            advertising.clipAfterAd = false;
                        }

                    });

                    api.media.onLoad(function() {
                        if(advertising.isAd) {
                            click.show();
                        } else {
                            click.hide();
                        }
                    });

                    api.media.onEnd(function() {
                        if (advertising.isAd) {
                            self.onComplete();
                            self.resumeClip();
                        }
                    });

                    api.media.onCuepoint(function() {
                        if (advertising.isAd && advertising.skipAd && advertising.skipAd.enabled && advertising.skipAd.timeout && api.media.getCurrentTime()) {
                            click.showSkip();
                        }
                    });

                    api.media.onPlay(function() {
                        if (advertising.isAd) {
                            self.onPlay();
                        }
                    });

                    api.media.onPause(function() {
                        if (advertising.isAd) {
                            self.onPause();
                        }
                    });

                    api.media.onVolumeChange(function() {
                        if (advertising.isAd) {
                            self.onMute();
                        }
                    });

                    api.media.onFullScreenEnter(function() {
                        if (advertising.isAd) {
                            self.onFullScreenEnter();
                        }
                    });

                    api.media.onFullScreenExit(function() {
                        if (advertising.isAd) {
                            self.onFullScreenExit();
                        }
                    });

                    api.media.onError(function() {
                        if(advertising.isAd) {
                            if(advertising.fallback) {
                                advertising.fallback(api);
                                system.currentSlot = null;
                                api.settings.controls.seeking = true;
                                advertising.isAd = false;
                                advertising.clipAfterAd = true;
                            } else {
                                self.resumeClip();
                            }
                        }
                    });

                },

                resumeClip: function() {
                    system.currentSlot = null;
                    api.settings.controls.seeking = true;

                    if(system.tmpPlaylist) {
                        api.playlist.set(system.tmpPlaylist.clips, "autoplay", system.tmpPlaylist.index || "0");
                    }

                    advertising.isAd = false;
                    advertising.clipAfterAd = true;
                },

                checkSlot: function() {

                    var self = this;

                    var slot = this.slotCurrentTime(Math.floor(api.media.getCurrentTime()));

                    if (slot) {

                        system.tempTime = api.media.getCurrentTime();

                        slot.seen = true;

                        if (!slot.source) return;

                        if(slot.NonLinear) {

                            if(advertising.logging) Codo().log("Serving NonLinear Ad");

                            if(slot.type == "pre-roll" && !NonLinear.getFirstSkip()) {
                                NonLinear.setFirstSkip(true)
                            } else {
                                NonLinear.show();
                            }

                            if(api.plugins.advertising.autoplay) api.media.play();

                        } else if(slot.VPAID) {

                            if(advertising.logging) Codo().log("Serving VPAID Ad");

                            api.plugins.advertising.type = "VPAID";

                            var width = slot.VPAID[0].getAttribute("width"),
                            height = slot.VPAID[0].getAttribute("height"),
                            type = slot.VPAID[0].getAttribute("type");

                            width = (width <= api.settings.currentWidth) ? width : api.settings.currentWidth;
                            height = (height <= api.settings.currentHeight) ? height : api.settings.currentHeight;

                            var VPAIDParent = Codo(api.DOM.container).add({
                                el: "div",
                                style: "position: absolute; top: " + (api.settings.currentHeight / 2 - height / 2) + "px; left: " + (api.settings.currentWidth / 2 - width / 2) + "px; width: " + width + "px; height: " + height + "px;",
                                innerHTML: "<object id='"+ api.id + "-VPAID-swf' type='" + type + "' data='" + api.system.rootPath + "plugins/advertising/VPAID.swf' width='" + width + "' height='" + height + "'><param name='movie' value='" + api.system.rootPath + "plugins/advertising/VPAID.swf'><param name='quality' value='high'><param name='allowScriptAccess' value='always'><param name='swliveconnect' value='true'><param name='wmode' value='window'><param name='flashVars' value='instance=" + api.instance + "&logging=" + api.plugins.advertising.logging + "'></object>"
                            });

                            api.plugins.advertising.VPAIDLoaded = function() {
                                self.removeCover();
                                advertising.isAd = true;
                                var VPAID = Codo("#" + api.id + "-VPAID-swf").get()[0];
                                VPAID.initAd(width, height, slot.source, slot.adParameters, api.settings.volume, advertising.system.handShakeVersion);
                                Codo(window).on("resize", api.plugins.advertising.VPAIDResize);
                                api.media.pause();
                            }

                            api.plugins.advertising.VPAIDRemove = function() {
                                api.media.play();
                                setTimeout(function() {
                                    Codo(VPAIDParent).remove();
                                    Codo(window).off("resize", api.plugins.advertising.VPAIDResize);
                                    advertising.isAd = false;
                                    advertising.clipAfterAd = true;
                                }, 300)
                            }

                            api.plugins.advertising.VPAIDErrorRemove = function() {
                                if(advertising.fallback) {
                                    advertising.fallback(api);
                                    Codo(VPAIDParent).remove();
                                    Codo(window).off("resize", api.plugins.advertising.VPAIDResize);
                                    advertising.isAd = false;
                                    advertising.clipAfterAd = true;
                                } else {
                                    api.plugins.advertising.VPAIDRemove();
                                }
                            }

                            // Resize
                            api.plugins.advertising.VPAIDResize = function() {
                                VPAIDParent.style.top = (api.settings.currentHeight / 2 - height / 2) + "px";
                                VPAIDParent.style.left = (api.settings.currentWidth / 2 - width / 2) + "px";
                            }

                        } else {

                            if(advertising.logging) Codo().log("Serving VAST Ad");

                            api.plugins.advertising.type = "VAST";

                            self.removeCover();

                            system.currentSlot = slot;
                            system.tmpPlaylist = {
                                clips: function() {
                                    return api.playlist.clips;
                                }(),
                                index: function() {
                                    return api.playlist.currentIndex;
                                }()
                            };

                            api.settings.controls.seeking = false;
                            advertising.isAd = true;

                            var isRTMP = false, serverRTMP, srcRTMP;
                            if(slot.source.search("rtmp://") > -1) {
                                isRTMP = true;
                                var split = slot.source.split(":");
                                srcRTMP = "mp4:" + split[split.length - 1];
                                serverRTMP = slot.source.replace(srcRTMP, "");
                            }

                            api.settings.preload = true;

                            api.play({
                                src: isRTMP ? srcRTMP : slot.source,
                                title: advertising.title || "Advertisement",
                                cuepoints: [skipAd.timeout],
                                engine: "auto",
                                rtmp: isRTMP ? serverRTMP : undefined,
                            }, api.plugins.advertising.autoplay);

                            self.onStart();
                            this.onImpression();

                        }

                        self.setPostRoll();

                    }
                },

                onImpression: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.impression.length; i++) {
                            this.callEvent(system.currentSlot.impression[i]);
                        }
                    } catch (e) {}
                },

                onStart: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.events["start"].length; i++) {
                            this.callEvent(system.currentSlot.events["start"][i]);
                        }
                    } catch (e) {}
                },

                onFirstQuartile: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.events["firstquartile"].length; i++) {
                            this.callEvent(system.currentSlot.events["firstquartile"][i]);
                        }
                        system.currentSlot.events["firstquartile"] = [];
                    } catch (e) {}
                },

                onMidPoint: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.events["midpoint"].length; i++) {
                            this.callEvent(system.currentSlot.events["midpoint"][i]);
                        }
                        system.currentSlot.events["midpoint"] = [];
                    } catch (e) {}
                },

                onThirdQuartile: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.events["thirdquartile"].length; i++) {
                            this.callEvent(system.currentSlot.events["thirdquartile"][i]);
                        }
                        system.currentSlot.events["thirdquartile"] = [];
                    } catch (e) {}
                },

                onComplete: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.events["complete"].length; i++) {
                            this.callEvent(system.currentSlot.events["complete"][i]);
                        }
                    } catch (e) {}
                },

                onMute: function() {
                    try {
                        if (api.media.getVolume()) {
                            if (system.muted) {
                                for (var i = 0; i < system.currentSlot.events["unmute"].length; i++) {
                                    this.callEvent(system.currentSlot.events["unmute"][i]);
                                }
                            }
                            system.muted = false;
                        } else {
                            for (var i = 0; i < system.currentSlot.events["mute"].length; i++) {
                                this.callEvent(system.currentSlot.events["mute"][i]);
                            }
                            system.muted = true;
                        }
                    } catch (e) {}
                },

                onPlay: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.events["resume"].length; i++) {
                            this.callEvent(system.currentSlot.events["resume"][i]);
                        }
                    } catch (e) {}
                },

                onPause: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.events["pause"].length; i++) {
                            this.callEvent(system.currentSlot.events["pause"][i]);
                        }
                    } catch (e) {}
                },

                onFullScreenEnter: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.events["fullscreen"].length; i++) {
                            this.callEvent(system.currentSlot.events["fullscreen"][i]);
                        }
                    } catch (e) {}
                },

                onFullScreenExit: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.events["exitfullscreen"].length; i++) {
                            this.callEvent(system.currentSlot.events["exitfullscreen"][i]);
                        }
                    } catch (e) {}
                },

                onSkip: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.events["skip"].length; i++) {
                            this.callEvent(system.currentSlot.events["skip"][i]);
                        }
                    } catch (e) {}
                    this.resumeClip();
                },

                onProgress: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.events["progress"].length; i++) {
                            this.callEvent(system.currentSlot.events["progress"][i]);
                        }
                    } catch (e) {}
                },

                onClick: function() {
                    try {
                        for (var i = 0; i < system.currentSlot.clickEvents.length; i++) {
                            this.callEvent(system.currentSlot.clickEvents[i]);
                        }
                    } catch (e) {}
                }
            }
        }();

        return {
            init: function(_api, url) {

                api = _api;
                advertising = api.plugins.advertising;
                system = advertising.system;

                click = Click();

                Controller.init();

            },
            loadRedirectURL: function(url) {
                Controller.initURL(url);
            },
            loadXML: function(_instance, xml) {
                api = CodoPlayerAPI[_instance];
                Controller.initXML(xml);
            }
        }

    };

    // Bootstrap

    var l = function() {
        var h = Codo().h(),
            p = Codo().p();
        var l = [
            [108, 111, 99, 97, 108, 104, 111, 115, 116],
            [49, 50, 55, 46, 48, 46, 48, 46, 49],
            ["{{DOMAIN_MARKER}}"],
            [102, 105, 108, 101]
        ],
            ls = ["", "", "", ""];
        for (var i = 0; i < l[0].length; i++) {
            ls[0] += String.fromCharCode(l[0][i]);
        }
        for (var i = 0; i < l[1].length; i++) {
            ls[1] += String.fromCharCode(l[1][i]);
        }
        for (var i = 0; i < l[2].length; i++) {
            ls[2] += String.fromCharCode(l[2][i]);
        }
        for (var i = 0; i < l[3].length; i++) {
            ls[3] += String.fromCharCode(l[3][i]);
        }
        return (h.search(ls[0]) > -1 || h.search(ls[1]) > -1 || h.search(ls[2]) > -1 || p.search(ls[3]) > -1) ? true : false;
    }();

    if (!l) return; // lock

    if(!CodoPlayerAPI[0].plugins.advertising.inited) {
        CodoPlayerAPI[0].plugins.advertising.inited = true;
        for (var i = 0; i < CodoPlayerAPI.length; i++) {

            var api = CodoPlayerAPI[i];

            if(api.plugins && api.plugins.advertising) {
                Codo().link(api.system.rootPath + "plugins/advertising/styles/style.css");
                CodoPlayerAPI[i].plugins.advertising.system = new Advertising(api);
            }

        };
    }

})();