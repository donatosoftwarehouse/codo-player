package {

    import flash.events.*;
    import flash.display.*;
    import flash.ui.*;
    import flash.media.*;
    import flash.net.*;
    import flash.external.*;
    import adobe.utils.*;
    import flash.accessibility.*;
    import flash.errors.*;
    import flash.filters.*;
    import flash.geom.*;
    import flash.printing.*;
    import flash.system.*;
    import flash.text.*;
    import flash.utils.*;

    [SWF(backgroundColor="0x000000")]
    public class VPAID extends MovieClip {

        private var instance:String;
        private var logging:String;

        public function VPAID():void {

            Security.allowInsecureDomain("*");
            Security.allowDomain("*");

            instance = LoaderInfo(this.root.loaderInfo).parameters.instance;
            logging = LoaderInfo(this.root.loaderInfo).parameters.logging;

            stage.align = StageAlign.TOP_LEFT;
            stage.scaleMode = StageScaleMode.NO_SCALE;

            function InitAd(w:Number, h:Number, d:String, params:String, vol:Number, handShakeVersion:String):void {

                // Loader
                var vpaidLoader:Loader = new Loader();
                vpaidLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadCompleteHandler);
                var vpaidContainerURLRequest:URLRequest = new URLRequest(d);
                vpaidLoader.load(vpaidContainerURLRequest);

                //~~~~~ Grab VPAID <AdParameters> as a String ~~~~~//
                var vpaidAdParameters:String = params;

                // Set Up VPAID Ad //

                var vpaidAd:Object;

                //~~~~~ Create Strings for VPAID ~~~~~//
                var vpaidCreativeData:String;
                var vpaidEnvironmentVars:String;

                //~~~~~ Set Width and Height for VPAID Container ~~~~~//
                var vpaidWidth:Number = w;
                var vpaidHeight:Number = h;

                function loadCompleteHandler(lcEvent:Event):void {

                    if(logging == "true") ExternalInterface.call("Codo().log", "loadCompleteHandler");

                    //~~~~~ Add VPAID Ad Object to the Stage ~~~~~//
                    vpaidAd = Object(lcEvent.target.content);
                    addChild(vpaidAd as DisplayObject);

                    if(String(vpaidAd.handshakeVersion) == "undefined") {

                        if(logging == "true") ExternalInterface.call("Codo().log", "Error: handshakeVersion not found");

                        ExternalInterface.call("CodoPlayerAPI[" + instance + "].plugins.advertising.VPAIDErrorRemove()");

                    } else {

                        vpaidAd.handshakeVersion(handShakeVersion);

                        vpaidAd.addEventListener("AdLoaded", adLoadedHandler);
                        vpaidAd.addEventListener("AdStopped", adStoppedHandler);
                        vpaidAd.addEventListener("AdError", adErrorHandler);
                        vpaidAd.addEventListener("AdClickThru", adClickHandler);
                        vpaidAd.addEventListener("AdLog", adLogHandler);

                        //~~~~~ Set AdParameters from XML ~~~~~//
                        vpaidCreativeData = vpaidAdParameters;

                        //~~~~~ Set Any EnvironmentVars ~~~~~//
                        //vastEnvironmentVars = "hide_skin=1";

                        if(logging == "true") ExternalInterface.call("Codo().log", "InitAd started");

                        vpaidAd.initAd(vpaidWidth, vpaidHeight, "normal", 0, vpaidCreativeData, vpaidEnvironmentVars);

                    }
                }

                function adLoadedHandler(alEvent:Event):void{
                    if(logging == "true") ExternalInterface.call("Codo().log", "adLoadedHandler");
                    vpaidAd.startAd();
                    vpaidAd.adVolume = vol/100;
                }

                function adStoppedHandler(asEvent:Event):void{
                    if(logging == "true") ExternalInterface.call("Codo().log", "adStoppedHandler");
                    removeChild(vpaidAd as DisplayObject);
                    ExternalInterface.call("CodoPlayerAPI[" + instance + "].plugins.advertising.VPAIDRemove()");
                }

                function adErrorHandler(aeEvent:Event):void{
                    if(logging == "true") ExternalInterface.call("Codo().log", "adErrorHandler");
                    //trace("AdError: " + Object(aeEvent).message);
                    removeChild(vpaidAd as DisplayObject);
                    ExternalInterface.call("CodoPlayerAPI[" + instance + "].plugins.advertising.VPAIDErrorRemove()");
                }

                function adClickHandler(url:String, id:String, playerEvent:Boolean):void{
                    if(logging == "true") ExternalInterface.call("Codo().log", "adClickHandler");
                    ExternalInterface.call("console.log", "adClickHandler");
                }

                function adLogHandler(id:String):void{
                    if(logging == "true") ExternalInterface.call("Codo().log", "adLogHandler");
                }

            }

            if(logging == "true") ExternalInterface.call("Codo().log", "VPAID started");

            ExternalInterface.addCallback("initAd", InitAd);
            ExternalInterface.call("CodoPlayerAPI[" + instance + "].plugins.advertising.VPAIDLoaded()");

        }

    }
}