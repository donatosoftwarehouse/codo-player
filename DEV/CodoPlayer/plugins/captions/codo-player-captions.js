/*
	Codo Player "captions" plugin
	Copyright (C) 2012-2013 Donato Software House Limited
*/

(function() {
	var file = "codo-player-captions.js";
	Codo().link(Codo().getScriptTag(file).src.split("?")[0].replace(file, "") + "styles/style.css");
	for(var i = 0; i < CodoPlayerAPI.length; i++) {
		if (CodoPlayerAPI[i].plugins && CodoPlayerAPI[i].plugins.captions && CodoPlayerAPI[i].platformName == "VideoHTML5") {
			var api = CodoPlayerAPI[i];
			api.API.onLoad(function(player) {
				if(typeof player.plugins.captions == "object") player.plugins.captions = Codo(player.plugins.captions).remove();
				if(player.playlist.API.getClip().track) {
					player.plugins.captions = Codo(player.API.getParent()).add({el: "track", kind: "subtitles", srclang: "en", src: player.playlist.API.getClip().track});
				}
			})
		}
	};
})();
