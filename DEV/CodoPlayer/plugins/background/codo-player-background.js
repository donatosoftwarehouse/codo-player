/*
	Codo Player "background" plugin
	Copyright (C) 2012-2013 Donato Software House
*/

(function() {
	for (var i = 0; i < CodoPlayerAPI.length; i++) {
		if (CodoPlayerAPI[i].plugins && CodoPlayerAPI[i].plugins.background) {
			var api = CodoPlayerAPI[i];
			api.DOM.parent.style.display = "none";
			api.loop = true;
			api.API.onLoad(function(player) {
				Codo("#" + api.id + "-controls-wrap").get().style.visibility = "hidden";
				Codo("#" + api.id + "-cover-wrap").get().style.visibility = "hidden";
				api.DOM.parent.style.position = "fixed";
				api.DOM.parent.style.top = 0;
				api.DOM.parent.style.left = 0;
				api.DOM.parent.style.margin = 0 + "px!important";
				api.API.resize(window.innerWidth, window.innerHeight);
				Codo(window).on("resize", function() {
					api.API.resize(window.innerWidth, window.innerHeight);
				})
				api.DOM.parent.style.display = "block";
			})
			api.API.onMetaData(function(player) {
				api.API.play();
			})
		}
	};
})();