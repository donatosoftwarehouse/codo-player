/*
    Codo Player
    codoplayer.com

    Copyright (C) Donato Software House
*/

package {
    import flash.events.*;
    import flash.display.*;
    import flash.ui.*;
    import flash.media.*;
    import flash.net.*;
    import flash.external.*;
    import adobe.utils.*;
    import flash.accessibility.*;
    import flash.errors.*;
    import flash.filters.*;
    import flash.geom.*;
    import flash.printing.*;
    import flash.system.*;
    import flash.text.*;
    import flash.utils.*;
    import flash.xml.*;
    import flash.display.Sprite;

    import flash.system.Security;
    import flash.external.ExternalInterface;

    [SWF(backgroundColor="0x000000")]
    public class module extends MovieClip {

        private var instance:String;
        private var mediaType:String;

        public function module():void {

            Security.allowInsecureDomain("*");
            Security.allowDomain("*");

            stage.align = StageAlign.TOP_LEFT;
            stage.scaleMode = StageScaleMode.NO_SCALE;

            instance = LoaderInfo(this.root.loaderInfo).parameters.instance;
            mediaType = LoaderInfo(this.root.loaderInfo).parameters.mediaType;

            switch(mediaType) {
                case "video":
                    VideoSWF();
                break;
                case "audio":
                    AudioSWF();
                break;
            }

            ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onSwfLoaded()");
        };

        private function VideoSWF():void {

            var video:Video, connection:NetConnection, stream:NetStream, nsst:SoundTransform, newMeta:Object;
            var videoDuration:Number;
            var initVolume:Number;
            var progressTimer:Number;
            var pauseFix:Boolean = false;
            var configObj:Object;
            var clipObj:Object;
            var metaObj:Object = {};
            var rtmpPreloadFix:Boolean = false;;

            function Init(config:Object, clip:Object):void {

                configObj = config;
                clipObj = clip;

                video = new Video();
                video.x = 0;
                video.y = 0;
                video.width = configObj.width;
                video.height = configObj.height;
                addChild(video);

                initVolume = configObj.volume;

                rtmpPreloadFix = false;

                nsst = new SoundTransform();

                newMeta = new Object();
                newMeta.onMetaData = function(metaData:Object):void {
                    if(!rtmpPreloadFix) {
                        metaObj.width = metaData.width;
                        metaObj.height = metaData.height;
                        metaObj.duration = videoDuration = metaData.duration;
                        ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onMetaData", metaObj);
                        progressTimer = setInterval(Progress, 500);
                        rtmpPreloadFix = true;
                    }
                };

                connection = new NetConnection();
                if(clipObj.rtmp) {
                    connection.addEventListener(NetStatusEvent.NET_STATUS, function(event:NetStatusEvent):void {
                        switch (event.info.code) {
                            case "NetConnection.Connect.Success":
                                if(connection) connection.close();
                                if(stream) stream.close();
                                stream = new NetStream(connection);
                                stream.bufferTime = 1;
                                stream.client = metaObj;
                                SetEnvs();
                                ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onRtmpLoaded()");
                            break;
                        }
                    });
                    connection.connect(clipObj.rtmp);
                } else {
                    connection.connect(null);
                    if(stream) stream.close();
                    stream = new NetStream(connection);
                    video.attachNetStream(stream);
                    stream.bufferTime = 1;
                    stream.client = newMeta;
                    SetEnvs();
                }
            };

            function SetEnvs():void {
                stream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, function(event:AsyncErrorEvent):void {
                    ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onError()");
                });
                stream.addEventListener(IOErrorEvent.IO_ERROR, function(event:IOErrorEvent):void {
                    ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onError()");
                });
                stream.addEventListener(NetStatusEvent.NET_STATUS, function(event:NetStatusEvent):void {
                    switch (event.info.code) {
                        case "NetStream.Play.Start":
                        break;
                        case "NetStream.Buffer.Empty":
                            //ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onWaiting()");
                        break;
                        case "NetStream.Buffer.Full":
                        break;
                        case "NetStream.Buffer.Flush":

                        break;
                        case "NetStream.Unpause.Notify":
                            ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onPlay()");
                        break;
                        case "NetStream.Pause.Notify":
                            if(pauseFix) {
                                ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onPause()");
                            }
                            pauseFix = true;
                        break;
                        case "NetStream.Play.Stop":
                            ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onEnd()");
                            clearInterval(progressTimer);
                        break;
                        case "NetStream.SeekStart.Notify":
                            ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onSeekStart()");
                        break;
                        case "NetStream.Seek.Notify":

                            ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onSeekEnd()");
                        break;
                        case "NetStream.Play.StreamNotFound":
                            ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onError()");
                        break;
                        case "NetStream.Seek.InvalidTime":

                        break;
                    }
                });
            };

            function Progress():void {
                var buffer:Number = stream.time + stream.bufferLength;
                ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onBuffer", stream.time+stream.bufferLength);
                if(buffer >= (videoDuration-0.5)) {
                    clearInterval(progressTimer);
                }
            };

            ExternalInterface.addCallback("initClip", function(config:Object, clip:Object):void {
                Init(config, clip);
            });

            ExternalInterface.addCallback("setSrc", function(src:String):void {
                if(src) {
                    if(stream) stream.close();
                    stream = new NetStream(connection);
                    video.attachNetStream(stream);
                    stream.bufferTime = 1;
                    stream.client = newMeta;
                    SetEnvs();
                    pauseFix = false;
                    stream.play(src);
                    stream.pause();
                }
            });

            ExternalInterface.addCallback("playClip", function():void {
                stream.resume();
            });

            ExternalInterface.addCallback("pauseClip", function():void {
                stream.pause();
            });

            ExternalInterface.addCallback("setCurrentTime", function(seekVal:Number):void {
                if(seekVal) {
                    stream.seek(seekVal);
                }
            });

            ExternalInterface.addCallback("getCurrentTime", function():Number {
                return stream.time;
            });

            ExternalInterface.addCallback("getDuration", function():Number {
                return videoDuration;
            });

            ExternalInterface.addCallback("setVolume", function(vol:Number):void {
                nsst.volume = initVolume = vol;
                stream.soundTransform = nsst;
            });

            ExternalInterface.addCallback("getVolume", function():Number {
                return nsst.volume;
            });

            ExternalInterface.addCallback("resize", function(w:int, h:int):void {
                if(w && h) {
                    video.width = w;
                    video.height = h;
                }
            });

        };

        private function AudioSWF():void {

            var audioRequest:URLRequest, audio:Sound, audioChannel:SoundChannel, audioTransform:SoundTransform, audioContext:SoundLoaderContext, lastPosition:Number;
            var audioDuration:Number;
            var initVolume:Number;

            function Init(config:Object, clip:Object):void {

                audioRequest = new URLRequest();
                audio = new Sound();
                audioChannel = new SoundChannel();
                audioTransform = new SoundTransform();
                audioContext = new SoundLoaderContext(3000, false);
                initVolume = config.volume;
                lastPosition = 0.1;
            };

            ExternalInterface.addCallback("initClip", function(config:Object, clip:Object):void {
                Init(config, clip);
            });

            ExternalInterface.addCallback("setSrc", function(src:String):void {
                if(src) {
                    if(audio) {
                        audioChannel.stop();
                        try {
                            audio.close();
                        } catch(error:Error) {}
                    }
                    audio = new Sound();
                    lastPosition = 0.1;
                    audioRequest = new URLRequest(src);
                    audio.load(audioRequest, audioContext);
                    audio.addEventListener(Event.COMPLETE, function(event:Event):void {
                        audioChannel = audio.play(lastPosition * 1000);
                        audioChannel.stop();
                        var metaObj:Object = {};
                        metaObj.duration = audioDuration = event.target.length/1000;
                        ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onMetaData", metaObj);
                    });
                    audio.addEventListener(IOErrorEvent.IO_ERROR, function(event:Event):void {
                        ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onError()");
                    });
                    audio.addEventListener(ProgressEvent.PROGRESS, function(event:ProgressEvent):void {
                        audioDuration = int(event.currentTarget.length/1000);
                        ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onBuffer", audioDuration);
                    });
                }
            });

            ExternalInterface.addCallback("playClip", function():void {
                audioChannel = audio.play(lastPosition * 1000);
                audioTransform.volume = initVolume;
                audioChannel.soundTransform = audioTransform;
                audioChannel.addEventListener(Event.SOUND_COMPLETE, function(event:Event):void {
                    ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onEnd()");
                });
                ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onPlay()");
            });

            ExternalInterface.addCallback("pauseClip", function():void {
                lastPosition = audioChannel.position / 1000;
                audioChannel.stop();
                ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onPause()");
            });

            ExternalInterface.addCallback("setCurrentTime", function(seekVal:Number):void {
                if(seekVal) {
                    ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onSeekStart()");
                    lastPosition = seekVal;
                    audioChannel.stop();
                    var timer:Number = setInterval(function():void {
                        if(!audio.isBuffering) {
                            ExternalInterface.call("CodoPlayerAPI[" + instance + "].media.system.onSeekEnd()");
                            clearInterval(timer);
                        }
                    }, 100)
                }
            });

            ExternalInterface.addCallback("getCurrentTime", function():Number {
                return (audioChannel.position / 1000);
            });

            ExternalInterface.addCallback("getDuration", function():Number {
                return audioDuration;
            });

            ExternalInterface.addCallback("setVolume", function(vol:Number):void {
                audioTransform.volume = initVolume = vol;
                audioChannel.soundTransform = audioTransform;
            });

            ExternalInterface.addCallback("getVolume", function():Number {
                return initVolume;
            });

        };

    }
}