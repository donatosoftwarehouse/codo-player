This package contains of Codo Player Pro system files. For setup instructions, go to codoplayer.com.

The use of software is based on Codo Player Pro software license agreement, found under www.codoplayer.com/policy/license/pro.

Codo Player
Copyright (C) Donato Software House