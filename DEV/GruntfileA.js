module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON("package_advertising.json"),

        replace: {
            license: {
                src: ["CodoPlayer/plugins/advertising/LICENSE.txt"],
                dest: "advertising/LICENSE.txt",
                replacements: [{
                    from: "{{version}}",
                    to: "<%= pkg.version %>"
                }]
            },
            createadpkg: {
                src: ["Website/createadpkg/index.php"],
                dest: "../account/res/php/createadpkg/index.php",
                replacements: [{
                    from: "{{version}}",
                    to: "<%= pkg.version %>"
                }]
            }
        },

        uglify: {
            plugin: {
                options: {
                    banner: "/*\n    <%= pkg.name %> <%= pkg.version %>\n    codoplayer.com\n\n    Licensed domain: \"{{LICENSED_DOMAIN}}\"\n\n    You should have received a copy of Software License Agreement along with <%= pkg.name %>.\n    If not, see <http://www.codoplayer.com/policy/license/advertising>.\n\n    Date: <%= grunt.template.today(\"dd mm yyyy\") %>\n\n    Copyright (C) Donato Software House\n*/\n\n"
                },
                src: "CodoPlayer/plugins/advertising/codo-player-advertising.js",
                dest: "advertising/codo-player-advertising.js"
            }
        },

        copy: {
            main: {
                files: [{
                    expand: true,
                    cwd: "advertising/",
                    src: ["**"],
                    dest: "../account/res/php/createadpkg/advertising/"
                }]
            }
        }

    });

    grunt.loadNpmTasks("grunt-text-replace");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-copy");

    grunt.registerTask("default", ["replace", "uglify", "copy"]);

};