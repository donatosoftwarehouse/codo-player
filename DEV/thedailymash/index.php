<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="robots" content="noindex, nofollow">

	<title>Player example</title>

</head>

<body>


    <script src=" https://s3-eu-west-1.amazonaws.com/codosyndicated/thedailymash/CodoPlayerPro/CodoPlayer.js" type="text/javascript"></script>

    <!-- STANDARD -->
    <script type="text/javascript">
        CodoPlayer([{
            title:"",
            src: "http://d3n2p3oiq12uha.cloudfront.net/ToastMP4/Toast The Phone Prank Low (MP4).mp4",
            poster: "fallback.jpg"
        },{
            title:"",
            src: "http://d3n2p3oiq12uha.cloudfront.net/ToastMP4/Toast Cheesy Chat Up Lines Low (MP4).mp4"
        },{
            title:"",
            src: "http://d3n2p3oiq12uha.cloudfront.net/ToastMP4/Toast Awkward Directions Low (MP4).mp4"
        }], {
            width: 300,
            height: 250,
            autoplay: false,
            volume: 0,
            controls: {
                title: false,
                fullscreen: false
            },
            plugins: {
                advertising: {
                    servers: [{
                        apiAddress: "http://konnect-test.videoplaza.tv/proxy/distributor/v2?tt=p&rt=vast_2.0&rnd={random}&pf=fl_11&s=desktop&t=VP2"
                    }],
                    schedule: [{
                        position: "pre-roll"
                    }],
                    fallback: function(api) {
                        api.DOM.container.innerHTML = "<img src='fallback.jpg' />";
                    }
                }
            }
        });
    </script>

    <!-- AUTOPLAY -->
    <script type="text/javascript">
        CodoPlayer([{
            title:"",
            src: "http://d3n2p3oiq12uha.cloudfront.net/ToastMP4/Toast The Phone Prank Low (MP4).mp4",
            poster: "fallback.jpg"
        },{
            title:"",
            src: "http://d3n2p3oiq12uha.cloudfront.net/ToastMP4/Toast Cheesy Chat Up Lines Low (MP4).mp4"
        },{
            title:"",
            src: "http://d3n2p3oiq12uha.cloudfront.net/ToastMP4/Toast Awkward Directions Low (MP4).mp4"
        }], {
            width: 300,
            height: 250,
            autoplay: true,
            volume: 0,
            controls: {
                title: false,
                fullscreen: false
            },
            plugins: {
                advertising: {
                    servers: [{
                        apiAddress: "http://konnect-test.videoplaza.tv/proxy/distributor/v2?tt=p&rt=vast_2.0&rnd={random}&pf=fl_11&s=desktop&t=VP2"
                    }],
                    schedule: [{
                        position: "pre-roll"
                    }],
                    fallback: function(api) {
                        api.DOM.container.innerHTML = "<img src='fallback.jpg' />";
                    }
                }
            }
        });
    </script>

</body>
</html>