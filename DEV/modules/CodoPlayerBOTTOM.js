        // System start
        var system = new System(settingsObj);
        var util = new Util(system);
        //  Playlist API
        system.playlist = new Playlist(system);

        // Add style
        Codo().link(system.system.rootPath + "styles/" + system.settings.style + "/style.css");

        // Parent style
        var parentStyle = "position: relative; width: 100%; height: 100%; cursor: default; -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; -webkit-font-smoothing: antialiased; visibility: hidden; overflow: hidden;";
        if (!placeHolder) {
            //  Inline
            document.write("<div id='" + system.id + "' class='" + system.className + "' style='" + parentStyle + "'></div>");
        } else {
            //  Dynamic
            if(!Codo(placeHolder).get()[0]) return;

            Codo(placeHolder).add({
                el: "div",
                id: system.id,
                className: system.className,
                style: parentStyle
            });
        }

        // Assign parent container
        var parent = system.DOM.parent = document.getElementById(system.id);

        //  Post system fix
        system.settings.width = system.settings.currentWidth = system.settings.width || Codo(parent).getWidth();
        system.settings.height = system.settings.currentHeight = system.settings.height || Codo().getVideoHeight(system.settings.width, system.settings.ratio[0], system.settings.ratio[1]);

        parent.style.width = system.settings.width + "px";
        parent.style.minHeight = system.settings.height + "px";

        //  Containers
        var container = Codo(parent).add({
            el: "div",
            className: system.className + "-container",
            style: "position: relative; margin: 0; padding: 0; width: " + system.settings.width + "px; height: " + system.settings.height + "px;"
        });
        system.DOM.container = container;

        var containerScreen = Codo(container).add({
            el: "div",
            className: system.className + "-container-screen",
            style: "position: absolute; top: 0; left: 0; width: 100%; height: 100%; margin: 0; padding: 0; overflow: hidden;"
        });
        system.DOM.containerScreen = containerScreen;

        system.DOM.containerScreenCanvas = Codo(containerScreen).add({
            el: "div",
            className: system.className + "-container-screen-canvas",
            style: "position: absolute; top: 0; left: 0; width: 100%; height: 100%; margin: 0; padding: 0;"
        });

        // Add logo
        if (l && system.settings.logo) {
            Codo(container).add({
                el: "img",
                src: system.settings.logo,
                className: system.className + "-container-logo",
                style: "position: absolute; top: 20px; right: 20px;"
            });
        }

        //  Config
        var loader = true;
        var overlay = true;

        //  Overlay is dependant on loader, so if loader is off, overlay must be triggered explicitly
        if (loader) loader = new Loader(system);
        if (overlay) overlay = new Overlay(system);
        if(!loader && overlay) overlay.on();

        //  Must haves'
        var controller = new Controller(system);
        var controls = new Controls(system);
        var errorCtrl = new ErrorCtrl(system);
        var fullscreen = new FullScreen(system);

        //  Context menu
        Codo(system.DOM.parent).on("contextmenu", function(e) {
            if(e.preventDefault) e.preventDefault();
            else e.returnValue;
            if(overlay) overlay.menu();
        });

        if(!system.plugins.advertising) {
            system.playlist.set(playlistObj, system.settings.autoplay);
        }

        var keys = 0, currKey = 0;
        for(var key in system.plugins) keys++;

        //  Load plugins
        for (var key in system.plugins) {
            if(system.plugins[key]) {
                Codo().script(system.system.rootPath + "plugins/" + key + "/codo-player-" + key + ".js", function(success, key) {
                    if(key == "advertising") {
                        if(!success) {
                            if(system.plugins[key].fallback) {
                                system.plugins[key].fallback(system);
                            } else {
                                system.playlist.set(playlistObj, system.settings.autoplay);
                            }
                        } else {
                            system.plugins[key].initCover = Codo(system.DOM.container).add({
                                el: "div",
                                className: system.className + "-advertising-init-cover",
                                style: "position: absolute; top: 0; left: 0; width: 100%; height: 100%; visibility: visible; cursor: pointer; opacity: 0; filter: alpha(opacity=0);"
                            });
                            system.plugins[key].autoplay = system.settings.autoplay;
                            system.settings.preload = system.settings.autoplay = false;
                            system.playlist.set(playlistObj, system.settings.autoplay);
                            if(system.plugins[key].system) system.plugins[key].system.init(system);
                        }
                    }
                    currKey++;
                    if(keys == currKey) if(system.onReady) system.onReady(system);
                }, key);
            }
        }

        if(!keys) {
            if(system.onReady) system.onReady(system);
        }

        return system;
    };

    if(!CodoPlayerAPI.length) {
        var keyboardEv = keyboardEv || function(e) {
            if(CodoPlayerAPI.length === 1) {
                if(e.target.nodeName == "INPUT" || e.target.nodeName == "TEXTAREA") return;
                e = e || window.event;
                var pl = CodoPlayerAPI[0];
                var volume = pl.media.getVolume();
                var seek = pl.media.getCurrentTime();
                switch (e.keyCode) {
                    case 70:
                        e.preventDefault()
                        pl.media.toggleFullScreen(e);
                    break;
                    case 32:
                        e.preventDefault()
                        pl.media.toggle();
                    break;
                    case 38:
                        e.preventDefault()
                        if(volume <= 100) {
                            volume = volume + 10;
                            if(volume > 100) volume = 100;
                            pl.media.setVolume(volume);
                        }
                    break;
                    case 40:
                        e.preventDefault()
                        if(volume >= 0) {
                            volume = volume - 10;
                            if(volume < 0) volume = 0;
                            pl.media.setVolume(volume);
                        }
                    break;
                    case 39:
                        e.preventDefault()
                        if(pl.settings.controls.seeking && seek <= pl.media.getDuration()) {
                            seek = seek + 5;
                            pl.media.setCurrentTime(seek);
                        }
                    break;
                    case 37:
                        e.preventDefault()
                        if(pl.settings.controls.seeking && seek >= 0) {
                            seek = seek - 5;
                            pl.media.setCurrentTime(seek);
                        }
                    break;
                }
            }
        };
        Codo(document).off("keydown", keyboardEv);
        Codo(document).on("keydown", keyboardEv);
    };

    Codo(window).on("resize", function(e) {
        for (var i = 0; i < CodoPlayerAPI.length; i++) {
            var pl = CodoPlayerAPI[i];
            if(!pl.system.isFullScreen && pl.settings.responsive) {
                pl.resize();
            }
        };
    });

    // Current instance
    var player = new Instance(playlistObj, settingsObj, placeHolder);

    // Push new instance into array
    CodoPlayerAPI.push(player);

    return player;

};

window.YTiframeReady = false;
window.onYouTubeIframeAPIReady = function() {
    YTiframeReady = true;
};