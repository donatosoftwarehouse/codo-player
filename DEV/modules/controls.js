var Controls = function(system) {

    var controlsTimer, delayTimer;
    var isMouseOverScr = false;
    var mouseOverControls = false;
    var controlsAlpha = 100;
    var controlsInOut = function(act) {
        switch (act) {
            case "in":
                clearTimeout(controlsTimer);
                clearTimeout(delayTimer);
                controlsAlpha = 100;
                wrap.style.opacity = controlsAlpha / 100;
                wrap.style.filter = "alpha(opacity=" + controlsAlpha + ")";
                break;
            case "out":
                delayTimer = setTimeout(function() {
                    controlsTimer = setInterval(function() {
                        if (controlsAlpha >= 0 && isMouseOverScr == false) {
                            wrap.style.opacity = controlsAlpha / 100;
                            wrap.style.filter = "alpha(opacity=" + controlsAlpha + ")";
                            controlsAlpha -= 10;
                        } else {
                            clearInterval(controlsTimer);
                            clearTimeout(delayTimer);
                        }
                    }, 20);
                }, system.settings.controls.hideDelay * 1000);
                break;
        }
    };


    var wrap = system.DOM.controls = Codo(system.DOM.container).add({
        el: "div",
        className: system.className + "-controls-wrap",
        style: "display: none;"
    });

    if(system.settings.controls.show != "never") wrap.style.display = "block";

    Codo(wrap).on("mouseover", function() {
        mouseOverControls = true;
    });
    Codo(wrap).on("mouseout", function() {
        mouseOverControls = false;
    });

    var controlShadeDiv = Codo(wrap).add({
        el: "div",
        id: system.id + "-controls-shade",
        className: system.className + "-controls-shade"
    });
    var controlsDiv = Codo(wrap).add({
        el: "div",
        id: system.id + "-controls",
        className: system.className + "-controls"
    });


    // Play button
    if (system.settings.controls.play) {
        var playBtn = Codo(controlsDiv).add({
            el: "div",
            className: system.className + "-controls-play-button"
        });
        Codo(playBtn).on(clickType, function(e) {
            if(e.stopPropagation && e.preventDefault) {
                e.stopPropagation();
                e.preventDefault();
            }
            if(!system.media.toggle) return;
            if (!system.settings.preload) {
                if (!system.system.initClickMade) {
                    system.media.toggle();
                    if (Codo().isTouch()) {
                        system.media.getParent().play();
                    }
                } else {
                    system.media.toggle();
                }
                system.system.initClickMade = true;
            } else {
                system.media.toggle();
            }
        });
    }
    // eo Play button


    // Title text
    if (system.settings.controls.title) {
        var controlTitle = Codo(controlsDiv).add({
            el: "div",
            id: system.id + "-controls-title-text",
            className: system.className + "-controls-title-text"
        });
    }
    // eo Title text


    // Fullscreen button
    if (system.settings.controls.fullscreen) {
        var fullScrBtn = Codo(controlsDiv).add({
            el: "div",
            className: system.className + "-controls-fullscreen-off-button"
        });

        Codo(fullScrBtn).on(clickType, function(e) {
            if(e.stopPropagation && e.preventDefault) {
                e.stopPropagation();
                e.preventDefault();
            }
            if (system.media.toggleFullScreen)  system.media.toggleFullScreen(e);
        });
    }
    // eo Fullscreen button


    // Volume slider
    var volumeActive = false;
    if (system.settings.controls.volume) {
        var volumeWrap = Codo(controlsDiv).add({
            el: "div",
            className: system.className + "-controls-volume-" + system.settings.controls.volume
        });
        var volumeBar = Codo(volumeWrap).add({
            el: "div",
            className: system.className + "-controls-volume-" + system.settings.controls.volume + "-bar"
        });
        var volumeHandle = Codo(volumeBar).add({
            el: "div",
            className: system.className + "-controls-volume-handle"
        });
        Codo(volumeWrap).on("mousedown", function(e) {

            volumeActive = true;
            MouseVolume(e);
        });
        Codo(volumeHandle).on("dblclick", function(e) {
            ToggleMute();
        });
    }

    var isMute, volumeMute;
    function ToggleMute() {
        if(!isMute) {
            Mute();
            isMute = true;
        } else {
            Unmute();
            isMute = false;
        }
    }

    function Mute() {
        volumeMute = system.media.getVolume();
        system.media.setVolume("0");
    }

    function Unmute() {
        system.media.setVolume(volumeMute);
    }

    function MouseVolume(e) {
        var fakeVol, realVol;
        if (system.settings.controls.volume == "horizontal") {
            var mousePos = Codo().mouse(e).x;
            var wrapPos = Codo(volumeWrap).getLeft();
            var wrapSize = Codo(volumeWrap).getWidth();
            var fakeVol = Math.round(mousePos - wrapPos);
        } else if (system.settings.controls.volume == "vertical") {
            var mousePos = Codo().mouse(e).y;
            var wrapPos = Codo(volumeWrap).getTop();
            var wrapSize = Codo(volumeWrap).getHeight();
            var fakeVol = Math.round(wrapPos - mousePos + wrapSize);
        }
        if (fakeVol >= 0 && fakeVol <= wrapSize) {
            realVol = Math.round(100 * fakeVol / wrapSize);
            if (system.media.setVolume) {
                system.media.setVolume(realVol);
            }
        }
    };

    function SetVolume(vol) {
        if(system.media.isMetaDataLoaded && system.media.isMetaDataLoaded()) {
            if (system.settings.controls.volume && !Codo().isTouch()) {
                if (system.settings.controls.volume == "horizontal") {
                    var wrapSize = Codo(volumeWrap).getWidth();
                } else if (system.settings.controls.volume == "vertical") {
                    var wrapSize = Codo(volumeWrap).getHeight();
                }
                if (vol >= 0 && vol <= 100) {
                    var slider = Math.round(wrapSize * vol / 100);
                    if (system.settings.controls.volume == "horizontal") {
                        volumeBar.style.width = slider + "px";
                    } else if (system.settings.controls.volume == "vertical") {
                        volumeBar.style.marginTop = wrapSize - slider + "px";
                        volumeBar.style.height = slider + "px";
                    }
                }
            }
        } else {
            if (system.settings.controls.volume == "horizontal") {
                volumeBar.style.width = vol + "%";
            } else if (system.settings.controls.volume == "vertical") {
                volumeBar.style.marginTop = wrapSize - vol + "%";
                volumeBar.style.height = vol + "%";
            }
        }
    }
    // eo Volume slider


    // HD text
    if (system.settings.controls.hd) {
        var HD = function() {

            var state = system.settings.priority === "srcHD" ? true: false;

            var hdText = Codo(controlsDiv).add({
                el: "div",
                className: system.className + "-controls-hd-text",
                innerHTML: "HD",
                style: "display: none;"
            });
            Codo(hdText).on(clickType, function(e) {
                if(e.stopPropagation && e.preventDefault) {
                    e.stopPropagation();
                    e.preventDefault();
                }
                if(!state) {
                    SetHD(true);
                    state = true
                } else {
                    SetHD(false);
                    state = false;
                }
            });

            function SetHD(_state) {
                if(_state) {
                    Codo(hdText).addClass("active");
                    system.playlist.getCurrentClip().priority = "srcHD";
                    system.playlist.same("srcHD");
                    state = _state;
                } else {
                    Codo(hdText).removeClass("active");
                    system.playlist.getCurrentClip().priority = "src";
                    system.playlist.same("src");
                    state = _state;
                }
            }
            return {
                on: function() {
                    state = true;
                    Codo(hdText).addClass("active");
                },
                off: function() {
                    state = false;
                    Codo(hdText).removeClass("active");
                },
                show: function() {
                    hdText.style.display = "block";
                },
                hide: function() {
                    hdText.style.display = "none";
                },
                setHD: function(_state) {
                    SetHD(_state);
                }
            }
        }();
    }
    // eo HD text

    // Time text
    if (system.settings.controls.time) {
        var timeText = Codo(controlsDiv).add({
            el: "div",
            className: system.className + "-controls-time-text",
            innerHTML: Codo().secsToTime(0) + " / " + Codo().secsToTime(0)
        });
    }
    // eo Time text


    // Seek slider
    var seekActive = false;
    if (system.settings.controls.seek) {
        var seekWrap = Codo(controlsDiv).add({
            el: "div",
            className: system.className + "-controls-seek"
        });
        var seekBufferBar = Codo(seekWrap).add({
            el: "div",
            className: system.className + "-controls-seek-buffer-bar",
            style: "width: 0px;"
        });
        var seekProgressBar = Codo(seekWrap).add({
            el: "div",
            className: system.className + "-controls-seek-progress-bar",
            style: "width: 0px;"
        });
        var seekHandle = Codo(seekProgressBar).add({
            el: "div",
            className: system.className + "-controls-seek-handle"
        });
        Codo(seekWrap).on("mousedown", function(e) {
            if (system.media.isMetaDataLoaded && system.media.isMetaDataLoaded() && system.settings.controls.seeking) {
                seekActive = true;
                if (system.media.setCurrentTime) {
                    system.media.setCurrentTime(controls.seek(e));
                }
            }
        });
    }
    // eo Seek slider



    Codo(document).on("mouseup", function(e) {
        volumeActive = false;
        if (seekActive && system.settings.controls.seeking) {
            if (system.media.setCurrentTime) {
                system.media.setCurrentTime(controls.seek(e));
            }
            seekActive = false;
        }
    });
    var thread;
    Codo(document).on("mousemove", function(e) {
        if (seekActive) {
            controls.seek(e);
        }
        if (volumeActive) {
            MouseVolume(e);
        }

        var mouseX = Codo().mouse(e).x - Codo().scrollX();
        var mouseY = Codo().mouse(e).y - Codo().scrollY();
        var top, left, right, bottom;
        if (fullscreen.getState()) {
            top = 0;
            left = 0;
            right = Codo().screen().width;
            bottom = Codo().screen().height;
        } else {
            top = Codo(system.DOM.container).getTop();
            left = Codo(system.DOM.container).getLeft();
            right = left + system.settings.currentWidth;
            bottom = top + system.settings.currentHeight;
        }
        if ((mouseX >= left) && (mouseX <= right) && (mouseY >= top) && (mouseY <= bottom)) {
            if(system.settings.controls.seek) {
                Codo(seekWrap).addClass("hover");
            }
            if (!isMouseOverScr && system.system.initClickMade) {
                isMouseOverScr = true;
                controlsInOut("in");
            }
            clearTimeout(thread);
            if (system.settings.controls.show != "always" && !mouseOverControls && fullscreen.getState()) {
                thread = setTimeout(function() {
                    isMouseOverScr = false;
                    controlsInOut("out");
                }, 500);
            }
        } else {
            if(system.settings.controls.seek) {
                Codo(seekWrap).removeClass("hover");
            }
            if (isMouseOverScr && !mouseOverControls && system.system.initClickMade) {
                if (system.playlist.getCurrentClip()) {
                    if (system.settings.controls.show != "always" && system.playlist.getCurrentClip().mediaType != "audio") {
                        isMouseOverScr = false;
                        clearTimeout(thread);
                        controlsInOut("out");
                    }
                }
            }
        }

    });

    function SetValues() {
        if (system.settings.controls.time) {
            if(system.playlist.getCurrentClip() && system.playlist.getCurrentClip().rtmp || system.playlist.getCurrentClip() && system.playlist.getCurrentClip().m3u8)
                timeText.innerHTML = "LIVE";
            else if(system.media.getCurrentTime() && system.media.getDuration()) {
                timeText.innerHTML = Codo().secsToTime(system.media.getCurrentTime()) + " / " + Codo().secsToTime(system.media.getDuration());
            }
        }
    }

    return {
        reset: function() {
            if(seekProgressBar) seekProgressBar.style.width = 0;
            if(seekBufferBar) seekBufferBar.style.width = 0;
        },
        on: function() {
            if (system.settings.controls.show != "never") {
                controlsInOut("in");
                if (system.settings.controls.show != "always" && system.system.initClickMade) {
                    controlsInOut("out");
                }
            }
        },
        off: function(noFade) {
            if (noFade != "nofade") {
                controlsInOut("out");
            } else {
                wrap.style.display = "none";
            }
        },
        play: function() {
            if (system.settings.controls.play) {
                Codo(playBtn).addClass(system.className + "-controls-pause-button");
            }
        },
        pause: function() {
            if (system.settings.controls.play) {
                Codo(playBtn).removeClass(system.className + "-controls-pause-button");
            }
        },
        title: function(text) {
            if (system.settings.controls.title) {
                if (controlTitle) {
                    if (text) {
                        controlTitle.innerHTML = text;
                    } else {
                        return controlTitle.innerHTML;
                    }
                }
            }
        },
        time: function() {
            SetValues();
        },
        hd: system.settings.controls.hd ? HD : undefined,
        progress: function(progress) {
            if (system.settings.controls.seek) {
                if (!seekActive) {
                    var dur = progress ? system.media.getDuration() : .1;
                    if (seekProgressBar && dur && system.playlist.getCurrentClip()) {
                        var w = !system.playlist.getCurrentClip().rtmp ? Codo(seekWrap).getWidth() * (progress || 0) / dur : 0;
                        seekProgressBar.style.width = w + "px";
                    }
                    SetValues();
                }
            }
        },
        buffer: function(buffer) {
            if (system.settings.controls.seek) {
                var dur = (buffer) ? system.media.getDuration() : .1;
                if (seekBufferBar && dur) {
                    seekBufferBar.style.width = Codo(seekWrap).getWidth() * (buffer || 0) / dur + "px";
                }
            }
        },
        seek: function(e) {
            if (system.settings.controls.seek && !system.playlist.getCurrentClip().rtmp) {
                var seekVal;
                if (system.media.getDuration) {
                    var inputX = Codo().mouse(e).x - Codo(seekWrap).getLeft();
                    seekVal = system.media.getDuration() * inputX / Codo(seekWrap).getWidth();
                    if (inputX >= 0 && inputX <= seekWrap.offsetWidth) {
                        seekProgressBar.style.width = inputX + "px";
                        if (system.settings.controls.time && seekVal && system.media.getDuration()) {
                            timeText.innerHTML = Codo().secsToTime(seekVal) + " / " + Codo().secsToTime(system.media.getDuration());
                        }
                    }
                }
                return seekVal;
            }
        },
        setVolume: function(val) {
            if (system.settings.controls.volume) {
                SetVolume(val || "0");
            }
        },
        showFullScreen: function() {
            if (system.settings.controls.fullscreen) {
                Codo(fullScrBtn).addClass(system.className + "-controls-fullscreen-on-button");
            }
        },
        hideFullScreen: function() {
            if (system.settings.controls.fullscreen) {
                Codo(fullScrBtn).removeClass(system.className + "-controls-fullscreen-on-button");
            }
        }
    }
};