var FullScreen = function(system) {
    var isFullScr = false;
    var pre;
    if (system.DOM.container.requestFullScreen) {
        pre = "f";
    } else if (system.DOM.container.mozRequestFullScreen) {
        pre = "moz";
    } else if (system.DOM.container.webkitRequestFullScreen) {
        pre = "webkit";
    }
    if (pre) {
        Codo(document).on(pre + "fullscreenchange", function(e) {
            var dom = false;
            if (document.fullscreenElement) {
                dom = true;
            } else if (document.mozFullScreenElement) {
                dom = true;
            } else if (document.webkitFullscreenElement) {
                dom = true;
            }
            if (!dom) {
                system.media.fullScreenExit();
            }
        })
    }

    Codo(window).on("orientationchange", function(e) {
        Set(true, e);
    });

    function Set(on, e) {
        if (on) {
            isFullScr = true;
            if (system.DOM.container.requestFullScreen) {
                system.DOM.container.requestFullScreen();
            } else if (system.DOM.container.mozRequestFullScreen) {
                system.DOM.container.mozRequestFullScreen();
            } else if (system.DOM.container.webkitRequestFullScreen) {
                system.DOM.container.webkitRequestFullScreen();
            }
            if (pre && e) {
                system.settings.currentWidth = screen.width;
                system.settings.currentHeight = screen.height;
            } else {
                system.settings.currentWidth = Codo().screen().width;
                system.settings.currentHeight = Codo().screen().height;
            }
            system.DOM.container.style.position = "fixed";
            system.DOM.container.style.top = 0 + "px";
            system.DOM.container.style.left = 0 + "px";
            system.DOM.container.style.width = 100 + "%";
            system.DOM.container.style.height = 100 + "%";
            system.DOM.container.style.zIndex = 999999999;
            if(controls) controls.showFullScreen();
        } else {
            isFullScr = false;
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
            system.settings.currentWidth = system.settings.width;
            system.settings.currentHeight = system.settings.height;
            system.DOM.container.style.position = "relative";
            system.DOM.container.style.width = system.settings.currentWidth + "px";
            system.DOM.container.style.height = system.settings.currentHeight + "px";
            system.DOM.container.style.zIndex = 0;
            if(controls) controls.hideFullScreen();
        }
        if (!system.settings.preload && !system.system.initPlayMade || system.playlist.getCurrentClip().mediaType == "audio") {
            util.resize(system.media.getPoster(), system.settings.mediaWidth, system.settings.mediaHeight, "fullscreen");
        } else if (system.playlist.getCurrentClip().mediaType == "video") {
            util.resize(system.media.getParent(), system.settings.mediaWidth, system.settings.mediaHeight, "fullscreen");
        }
        system.system.isFullScreen = isFullScr;
    };
    return {
        on: function(e) {
            Set(true, e);
        },
        off: function(e) {
            Set(false, e);
        },
        getState: function() {
            return isFullScr;
        }
    }
};