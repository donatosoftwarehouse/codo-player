var onBeforeLoadCallBk = [];
var onLoadCallBk = [];
var onMetaDataCallBk = [];
var onPlayCallBk = [];
var onPauseCallBk = [];
var onEndCallBk = [];
var onProgressCallBk = [];
var onBufferCallBk = [];
var onSeekStartCallBk = [];
var onSeekEndCallBk = [];
var onVolumeChange = [];
var onFullScreenEnterCallBk = [];
var onFullScreenExitCallBk = [];
var onErrorCallBk = [];
var onClipBegin = [];
var onClipFirstQuarter = [];
var onClipSecondQuarter = [];
var onClipThirdQuarter = [];
var onClipEnd = [];
var onCuepoint = [];

var Controller = function() {
    function Set(_clip, _autoplay, _key) {

        if(!_clip) return;

        _key = _key || _clip.priority;

        if (errorCtrl) errorCtrl.off();
        if (controls) {
            controls.reset();
            controls.pause();
            controls.title(system.settings.controls.loadingText);
            controls.setVolume(system.settings.volume);
            if (system.settings.controls.hd) {
                if(!_clip["src"] || !_clip["srcHD"]) {
                    _key = "src";
                    controls.hd.off();
                    controls.hd.hide();
                } else if(_clip["src"] && _clip["srcHD"]) {
                    controls.hd.show();
                    if(_key === "srcHD") {
                        controls.hd.on();
                    } else {
                        controls.hd.off();
                    }
                }
            }
            controls.on();
        }

        if (loader) loader.on();

        if (_clip) {
            _clip.engine = _clip.engine || "auto";
            if (_clip[_key]) {
                if (_clip[_key].length > 0) {
                    for (var i = 0; i < _clip[_key].length; i++) {
                        var src = _clip[_key][i];
                        if (_clip.engine == "youtube") {
                            _clip.activeUrl = src;
                            _clip.platformName = "YOUTUBE";
                            _clip.mediaType = "video";
                            if (_clip.platformName != system.media.platformName) {
                                if (system.media.destroy) {
                                    system.media.destroy();
                                }
                                system.media = new YOUTUBE(_clip, _clip.mediaType, _autoplay, _key);
                            } else {
                                system.media.play(_clip, _autoplay, _key);
                            }
                            return;
                        }
                        if (_clip.rtmp) {
                            _clip.engine = "flash";
                            _clip.activeUrl = src;
                            _clip.platformName = "videoSWF";
                            _clip.mediaType = "video";
                            if (_clip.platformName != system.media.platformName) {
                                if (system.media.destroy) {
                                    system.media.destroy();
                                }
                                system.media = new SWF(_clip, _clip.mediaType, _autoplay, _key);
                            } else {
                                system.media.play(_clip, _autoplay, _key);
                            }
                            return;
                        }
                        var v = document.createElement("video");
                        var s = document.createElement("audio");
                        var ext = (src).match(/\.[0-9a-z]+$/i);
                        ext = ext ? ext[0].replace(".", "") : "mp4";

                        if (v.canPlayType) {
                            if (v.canPlayType("video/" + ext).length > 0 || ext == "m3u8") {
                                if (_clip.engine == "html5" || _clip.engine == "auto") {
                                    _clip.activeUrl = src;
                                    _clip.platformName = "videoHTML5";
                                    _clip.mediaType = "video";
                                    if(ext == "m3u8") _clip.m3u8 = true;
                                    if (_clip.platformName != system.media.platformName) {
                                        if (system.media.destroy) {
                                            system.media.destroy();
                                        }
                                        system.media = new HTML5(_clip, _clip.mediaType, _autoplay, _key);
                                    } else {
                                        system.media.play(_clip, _autoplay, _key);
                                    }
                                    return;
                                }
                            } else if (s.canPlayType("audio/" + ext).length > 0) {
                                if (_clip.engine == "html5" || _clip.engine == "auto") {
                                    _clip.activeUrl = src;
                                    _clip.platformName = "audioHTML5";
                                    _clip.mediaType = "audio";
                                    if (_clip.platformName != system.media.platformName) {
                                        if (system.media.destroy) {
                                            system.media.destroy();
                                        }
                                        system.media = new HTML5(_clip, _clip.mediaType, _autoplay, _key);
                                    } else {
                                        system.media.play(_clip, _autoplay, _key);
                                    }
                                    return;
                                }
                            }
                        }
                        if (ext == "mp4" || ext == "flv") {
                            if (_clip.engine == "flash" || _clip.engine == "auto") {
                                _clip.activeUrl = src;
                                _clip.platformName = "videoSWF";
                                _clip.mediaType = "video";
                                system.media = new SWF(_clip, _clip.mediaType, _autoplay, _key);
                                // if(_clip.platformName != system.media.platformName) {
                                //     if (system.media.destroy) {
                                //         system.media.destroy();
                                //     }
                                //     system.media = new SWF(_clip, _clip.mediaType, _autoplay, _key);
                                // } else {
                                //     system.media.play(_clip, _autoplay, _key);
                                // }
                                return;
                            }
                        } else if (ext == "mp3" || ext == "wav") {
                            if (_clip.engine == "flash" || _clip.engine == "auto") {
                                _clip.activeUrl = src;
                                _clip.platformName = "audioSWF";
                                _clip.mediaType = "audio";
                                if (_clip.platformName != system.media.platformName) {
                                    if (system.media.destroy) {
                                        system.media.destroy();
                                    }
                                    system.media = new SWF(_clip, _clip.mediaType, _autoplay, _key);
                                } else {
                                    system.media.play(_clip, _autoplay, _key);
                                }
                                return;
                            }
                        }
                    }
                }
            }
        }
        errorCtrl.on("source not recognized");
        for (var i = 0; i < onErrorCallBk.length; i++) {
            if (onErrorCallBk[i]) onErrorCallBk[i]();
        }
    }
    return {
        set: function(_clip, _autoplay, _key) {
            Set(_clip, _autoplay, _key);
        }
    }
};