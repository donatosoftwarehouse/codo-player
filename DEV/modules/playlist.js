var Playlist = function(system) {

    var playlist = {
        breakTime: "0",
        currentIndex: "0",
        set: function(_playlistObj, _autoplay, _index) {
            if(_index) {
                this.currentIndex = _index;
            } else {
                this.currentIndex = "0";
            }
            this.clips = Parse(_playlistObj);
            for(var i = 0; i < this.clips.length; i++) {
                if(this.clips[i].srcHD) this.clips[i].srcHD = ParseHD(this.clips[i].srcHD);
            }
            this.next(this.currentIndex, _autoplay);
        },
        next: function(_currentIndex, _autoplay) {
            this.breakTime = "0";
            if (!l && loader && loader.getImage().src.length != 4670) {
                errorCtrl.on();
                return;
            }
            if (_currentIndex && _currentIndex >= 0 && _currentIndex < this.clips.length) {
                this.currentIndex = _currentIndex;
            } else {
                if(this.currentIndex < (this.clips.length - 1)) {
                    this.currentIndex++;
                } else {
                    this.currentIndex = "0";
                }
            }

            if(system.settings.playlist) Draw();
            controller.set(this.clips[this.currentIndex], _autoplay);
        },
        same: function(key) {
            this.breakTime = system.media.getCurrentTime ? system.media.getCurrentTime() : "0";
            controller.set(this.clips[this.currentIndex], "autoplay", key);
        },
        getCurrentClip: function() {
            return this.clips[this.currentIndex];
        }
    }

    function Parse(source) {
        if (!source) {
            return;
        } else {

            var clips = [];

            if (typeof source == "string") {
                clips.push({
                    src: [source]
                });
            } else if (typeof source == "object") {
                if (source[0]) {
                    for (var i = 0; i < source.length; i++) {
                        if (typeof source[i] == "string") {
                            clips.push(source[i]);
                        } else if (typeof source[i] == "object") {
                            if (source[i].src) {
                                if (typeof source[i].src == "string") {
                                    clips.push(source[i]);
                                    clips[clips.length - 1].src = [source[i].src];

                                } else if (source[i].src[0]) {
                                    clips.push(source[i]);
                                }
                            }
                        }
                    }
                } else {
                    if (source.src) {
                        if (typeof source.src == "string") {
                            clips.push(source);
                            clips[clips.length - 1].src = [source.src];
                        } else if (source.src[0]) {
                            clips.push(source);
                        }
                    }
                }
            }

            for (i = 0; i < clips.length; i++) {
                clips[i].id = i;
                clips[i].hasPrevious = (i !== 0) ? true : false;
                clips[i].hasNext = (i < (clips.length - 1)) ? true : false;
                clips[i].poster = clips[i].poster || system.settings.poster;
                clips[i].engine = clips[i].engine || system.settings.engine;
                clips[i].rtmp = clips[i].rtmp || system.settings.rtmp;
                clips[i].cuepoints = clips[i].cuepoints || system.settings.cuepoints;
                clips[i].priority = clips[i].priority || system.settings.priority;
            }

            return clips;
        }
    }

    function ParseHD(source) {
        if(!source) {
            return false;
        } else {
            var tempArr = [], i;
            if(typeof source == "string") {
                tempArr.push(source);
            } else if(typeof source == "object") {
                if(source[0]) {
                    for(i = 0; i < source.length; i++) {
                        if(typeof source[i] == "string") {
                            tempArr.push(source[i]);
                        } else if(typeof source[i] == "object") {
                            if(source[i].src) {
                                if(typeof source[i].src == "string") {
                                    tempArr.push(source[i]);
                                    tempArr[tempArr.length-1].src = [source[i].src];
                                } else if(source[i].src[0]) {
                                    tempArr.push(source[i]);
                                }
                            }
                        }
                    }
                } else {

                    if(source.src) {
                        if(typeof source.src == "string") {
                            tempArr.push(source);
                            tempArr[tempArr.length-1].src = [source.src];
                        } else if(source.src[0]) {
                            tempArr.push(source);
                        }
                    }
                }
            }
            return tempArr;
        }
    }

    var playlistWrap;
    function Draw(_currentIndex) {
        if (playlistWrap) Codo(playlistWrap).remove();
        playlistWrap = Codo(system.DOM.parent).add({
            el: "div",
            className: system.className + "-playlist-wrap"
        });
        var ul = Codo(playlistWrap).add({
            el: "ul",
            className: system.className + "-playlist-ul",
            style: "position: relative; width: 100%;"
        });
        for (var i = 0; i < playlist.clips.length; i++) {
            var ulLi = Codo(ul).add({
                el: "li",
                style: "cursor: pointer; overflow: auto;"
            });
            var ulLiSpan1 = Codo(ulLi).add({
                el: "span",
                className: system.className + "-playlist-ul-id",
                style: "float: left;",
                innerHTML: playlist.clips[i].id + 1
            });
            var ulLiSpan2 = Codo(ulLi).add({
                el: "span",
                className: system.className + "-playlist-ul-title",
                style: "float: left;",
                innerHTML: playlist.clips[i].title || ""
            });
            // IE8 click fix
            ulLi.setAttribute("data-row", i);
            ulLiSpan1.setAttribute("data-row", i);
            ulLiSpan2.setAttribute("data-row", i);
            Codo(ulLi).on(clickType, function(e) {
                if(e.stopPropagation && e.preventDefault) {
                    e.stopPropagation();
                    e.preventDefault();
                }
                e = e || window.event;
                var target = e.target || e.srcElement;

                // If Advertising Plugin plays AD, skip loading another clip
                if(system.plugins && system.plugins.advertising && system.plugins.advertising.isAd) return;

                system.system.initClickMade = true;
                system.playlist.next(target.getAttribute("data-row"), "autoplay");
                if (Codo().isTouch()) {
                    system.media.play();
                }
            });
        }
        var liArr = ul.getElementsByTagName("li");
        for (var i = 0; i < liArr.length; i++) {
            Codo(liArr[i]).removeClass(system.className + "-playlist-currentClip");
            if (liArr[i].getAttribute("data-row") == playlist.currentIndex) {
                Codo(liArr[i]).addClass(system.className + "-playlist-currentClip");
            }
        }

    }

    return playlist;
};