window.Codo = function(sel) {
    "use strict";
    var el = [];
    if (sel) {
        if (typeof sel === "string") {
            el = document.querySelectorAll(sel);
        } else el.push(sel);
    }
    return {
        get: function() {
            return el;
        },
        domReady: function(cb) {
            if (!cb) return;
            if (document.addEventListener) {
                document.addEventListener("DOMContentLoaded", cb);
            } else {
                document.attachEvent("onreadystatechange", function() {
                    if (document.readyState === "interactive") cb();
                });
            }
        },
        script: function(src, cb, passObj) {
            var isLoaded = false;
            var s = document.createElement("script");
            s.type = "text/javascript";
            s.async = true;
            s.onreadystatechange = function(isLoaded) {
                if ((this.readyState == "complete" || this.readyState == "loaded") && !isLoaded) {
                    if (cb) {
                        cb(true, passObj);
                    }
                    isLoaded = true;
                }
            };
            s.onload = function() {
                if (cb) cb(true, passObj);
            };
            s.onerror = function() {
                if (cb) cb(false, passObj);
            };
            s.src = src;
            document.getElementsByTagName("head")[0].appendChild(s);
        },
        link: function(src) {
            var l = document.createElement("link");
            l.rel = "stylesheet";
            l.type = "text/css";
            l.href = src;
            document.getElementsByTagName("head")[0].appendChild(l);
        },
        load: function(obj, cb, passObj) {
            var request = new XMLHttpRequest();
            request.open(obj.action || "GET", obj.url, true);
            request.onreadystatechange = function() {
                if (this.readyState === 4) {
                    if (this.status >= 200 && this.status < 400) {
                        // Success!
                        if (el[0]) {
                            el[0].innerHTML = this.responseText
                        } else {
                            cb(this.responseText, passObj);
                            return this.responseText
                        }
                    } else {
                        cb("error", passObj);
                    }
                }
            };
            if(obj.contentType) request.setRequestHeader("Content-Type", obj.contentType);
            request.send();
        },
        on: function(act, cb, bbl) {
            if (!el[0]) return;
            for (var i = 0; i < el.length; i++) {
                if (el[i].addEventListener) {
                    el[i].addEventListener(act, cb, bbl || false);
                } else {
                    el[i].attachEvent("on" + act, cb);
                }
            }
            return el;
        },
        off: function(act, cb) {
            if (!el[0]) return;
            for (var i = 0; i < el.length; i++) {
                if (el[i].removeEventListener) {
                    el[i].removeEventListener(act, cb);
                } else {
                    el[i].detachEvent("on" + act, cb);
                }
            }
            return el;
        },
        add: function(obj) {
            if (!el[0]) return;
            var elem = [];
            for (var i = 0; i < el.length; i++) {
                var newEl = document.createElement(obj.el);
                for (var key in obj) {
                    if (key != "el") {
                        if (newEl.key) {
                            newEl.key = obj[key];
                        } else if (key == "className") {
                            newEl.className = obj[key];
                        } else if (key == "style") {
                            newEl.style.cssText = obj[key];
                        } else if (key == "innerHTML") {
                            newEl.innerHTML = obj[key];
                        } else {
                            newEl.setAttribute(key, obj[key]);
                        }
                    }
                }
                if (el[i]) {
                    el[i].appendChild(newEl);
                }
            }
            return newEl;
        },
        remove: function() {
            if (!el[0]) return;
            for (var i = 0; i < el.length; i++) {
                el[i].parentNode.removeChild(el[i]);
                el[i] = undefined;
            }
        },
        addClass: function(clsName) {
            if (!el[0]) return;
            for (var i = 0; i < el.length; i++) {
                if (el[i].classList) el[i].classList.add(clsName);
                else el[i].className += ' ' + clsName;
            }
        },
        removeClass: function(clsName) {
            if (!el[0]) return;
            for (var i = 0; i < el.length; i++) {
                if (el[i].classList) el[i].classList.remove(clsName);
                else el[i].className = el[i].className.replace(new RegExp('(^|\\b)' + clsName.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
            }
        },
        toggle: function() {
            if (!el[0]) return;
            if (el[0].style.display == "block") {
                el[0].style.display = "none";
            } else {
                el[0].style.display = "block";
            }
        },
        getTop: function() {
            if (!el[0]) return;
            return el[0].getBoundingClientRect().top;
        },
        getLeft: function() {
            if (!el[0]) return;
            return el[0].getBoundingClientRect().left;
        },
        getWidth: function() {
            if (!el[0]) return;
            return el[0].clientWidth || el[0].offsetWidth;
        },
        getHeight: function() {
            if (!el[0]) return;
            return el[0].clientHeight || el[0].offsetHeight;
        },
        screen: function() {
            var obj = {};
            obj.width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            obj.height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
            return obj;
        },
        scrollX: function() {
            return (window.pageXOffset !== undefined) ? window.pageXOffset : (document.documentElement || document.parentNode || document).scrollLeft;
        },
        scrollY: function() {
            return (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.parentNode || document).scrollTop;
        },
        mouse: function(e) {
            e = e || window.event;
            var pageX = e.pageX;
            var pageY = e.pageY;
            if (pageX === undefined) {
                pageX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                pageY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            }
            var obj = {};
            obj.x = pageX;
            obj.y = pageY
            return obj;
        },
        fadeIn: function(len, pos) {
            if (!el[0]) return;
            var cnt = 0;
            len = len || 2;
            pos = pos || 100;
            el[0].style.display = "block";
            el[0].style.visibility = "visible";
            var tick = function() {
                cnt += len;
                el[0].style.opacity = cnt/100;
                el[0].style.filter = "alpha(opacity=" + cnt + ")";
                if (cnt < pos) {
                    (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
                }
            };
            tick();
        },
        fadeOut: function(len, pos) {
            if (!el[0]) return;
            var cnt = 100;
            len = len || 2;
            pos = pos || 0;
            var tick = function() {
                cnt -= len;
                el[0].style.opacity = cnt/100;
                el[0].style.filter = "alpha(opacity=" + cnt + ")";
                if (cnt > pos) {
                    (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
                } else el[0].style.display = "none";
            };
            tick();
        },
        log: function(msg) {
            if (window.console) console.log(msg);
        },
        isTouch: function() {
            return !!('ontouchstart' in window);
        },
        isMobile: function() {
            if(/webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) return true;
        },
        isIphone: function() {
            return navigator.userAgent.match(/iPhone|iPod/i);
        },
        // isAndroidOrWindows: function() {
        //     return navigator.userAgent.match(/Android|IEMobile/i);
        // },
        isFlash: function() {
            var support = false;
            if ("ActiveXObject" in window) {
                try {
                    support = !! (new ActiveXObject("ShockwaveFlash.ShockwaveFlash"));
                } catch (e) {
                    support = false;
                }
            } else {
                support = !! navigator.mimeTypes["application/x-shockwave-flash"];
            }
            return support;
        },
        p: function() {
            return location.protocol;
        },
        h: function() {
            return location.hostname;
        },
        getScriptTag: function(file) {
            var arr = document.scripts;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].attributes.src) {
                    if (arr[i].attributes.src.value.search(file) > -1) {
                        return arr[i];
                    }
                }
            }
        },
        getVideoHeight: function(nW, vW, vH) {
            return nW / (vW / vH);
        },
        secsToTime: function(secs) {
            var seper1 = ":",
                seper2 = ":";
            var hours = Math.floor(secs / (60 * 60));
            if (hours < 10) hours = "0" + hours;
            if (hours === "00") {
                hours = "";
                seper1 = "";
            }
            var divisor_for_minutes = secs % (60 * 60);
            var minutes = Math.floor(divisor_for_minutes / 60);
            if (minutes < 10) minutes = "0" + minutes;
            var divisor_for_seconds = divisor_for_minutes % 60;
            var seconds = Math.round(divisor_for_seconds);
            if (seconds < 10) seconds = "0" + seconds;
            return hours + seper1 + minutes + seper2 + seconds;
        }
    };
};
if (!document.querySelectorAll)(function(d, s) {
    d = document, s = d.createStyleSheet();
    d.querySelectorAll = function(r, c, i, j, a) {
        a = d.all, c = [], r = r.replace(/\[for\b/gi, '[htmlFor').split(',');
        for (i = r.length; i--;) {
            s.addRule(r[i], 'k:v');
            for (j = a.length; j--;) a[j].currentStyle.k && c.push(a[j]);
            s.removeRule(0)
        }
        return c
    }
})()