var Util = function() {
    return {
        resize: function(el, w, h, f) {
            var videoRatio = w / h;
            var targetRatio = system.settings.currentWidth / system.settings.currentHeight;
            var videoWidth = system.settings.mediaWidth = targetRatio > videoRatio ? system.settings.currentHeight * videoRatio : system.settings.currentWidth;
            var videoHeight = system.settings.mediaHeight =  targetRatio > videoRatio ? system.settings.currentHeight : system.settings.currentWidth / videoRatio;
            if (!f && !fullscreen.getState()) {
                system.DOM.parent.style.width = system.DOM.container.style.width = system.settings.currentWidth + "px";
                system.DOM.parent.style.minHeight = system.DOM.container.style.height = system.settings.currentHeight + "px";
            }
            if (el) {
                el.width = videoWidth;
                el.height = videoHeight;
                el.style.width = videoWidth + "px";
                el.style.height = videoHeight + "px";
                el.style.top = system.settings.currentHeight / 2 - videoHeight / 2 + "px";
                el.style.left = system.settings.currentWidth / 2 - videoWidth / 2 + "px";
                if (el.resize) el.resize(videoWidth, videoHeight);
            }
            if (loader) loader.resize(system.settings.currentWidth, system.settings.currentHeight);
            if(overlay) overlay.resize(system.settings.currentWidth, system.settings.currentHeight);
        }
    }
};