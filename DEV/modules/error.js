var ErrorCtrl = function(system) {
    var errorWrap = Codo(system.DOM.container).add({
        el: "div",
        id: system.id + "-error-wrap",
        className: system.className + "-error-wrap",
        style: "position: absolute; top: 0; left: 0; width: 100%; height: 100%; display: none;"
    });
    return {
        on: function(text) {
            text = text || "";
            Codo().log("Error: " + text);
            controls.title("Error: " + text);
            if(l && loader) loader.off();
            //errorWrap.style.display = "block";
            var curClip = system.playlist.getCurrentClip();
            if (curClip && curClip.hasNext) {
                if(!system.system.initPlayMade) {
                    var timer = setTimeout(function() {
                        if(curClip.id == system.playlist.getCurrentClip().id) {
                            system.system.firstClipOver = true;
                            system.playlist.next();
                            clearTimeout(timer);
                        }
                    }, 3000)
                }
            }
        },
        off: function() {
            //errorWrap.style.display = "none";
        }
    }
};