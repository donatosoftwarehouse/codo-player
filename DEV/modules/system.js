var System = function(settingsObj) {
    // ID, Class
    var id, className = "codo-player";
    if (settingsObj.id) id = settingsObj.id;
    else id = "codo-player-" + CodoPlayerAPI.length;
    var rootPath = Codo().getScriptTag("CodoPlayer.js").src.replace("CodoPlayer.js", "");
    // System obj
    var system = {
        instance: CodoPlayerAPI.length,
        id: id,
        className: className,
        DOM: {
            parent: undefined,
            container: undefined,
            containerScreen: undefined,
            containerScreenCanvas: undefined,
            overlay: undefined,
            controls: undefined
        },
        settings: {
            responsive: function() {
                return (!settingsObj.width) ? true : false;
            }(),
            style: (settingsObj.style || "standard"),
            ratio: settingsObj.ratio || [16, 9],
            width: settingsObj.width,
            height: settingsObj.height,
            currentWidth: undefined,
            currentHeight: undefined,
            mediaWidth: undefined,
            mediaHeight: undefined,
            autoplay: settingsObj.autoplay,
            poster: settingsObj.poster,
            volume: function() {
                return (settingsObj.volume === 0) ? 0 : function() {
                    if (!settingsObj.volume) {
                        return 80;
                    } else {
                        return settingsObj.volume;
                    }
                }();
            }(),
            loop: settingsObj.loop,
            preload: function() {
                if(Codo().isTouch()) {
                    return true;
                } else {
                    return (settingsObj.preload === false) ? false : true;
                }
            }(),
            engine: (settingsObj.engine || "auto"),
            loader: settingsObj.loader || rootPath + "loader.gif",
            logo: settingsObj.logo,
            cuepoints: settingsObj.cuepoints,
            playlist: settingsObj.playlist,
            priority: settingsObj.priority || "src"
        },
        playlist: {},
        about: {
            product: "{{name}} {{kind}} {{version}}" // pro
        },
        media: {},
        system: {
            initClickMade: false,
            firstClipOver: false,
            initPlayMade: false,
            isFullScreen: false,
            rootPath: rootPath
        },
        plugins: settingsObj.plugins || {},
        play: function(_playlistObj, _autoplay) {
            if(_playlistObj) {
                system.playlist.set(_playlistObj, _autoplay);
            } else {
                if (!system.settings.preload) {
                    if (!system.system.initClickMade) {
                        system.media.toggle();
                        if (Codo().isTouch()) {
                            system.media.getParent().play();
                        }
                    } else {
                        system.media.toggle();
                    }
                    system.system.initClickMade = true;
                } else {
                    system.media.toggle();
                }
            }
        },
        pause: function() {
            system.media.pause();
        },
        resize: function(w, h) {
            if(w && h) {
                system.settings.width = system.settings.currentWidth = w;
                system.settings.height = system.settings.currentHeight = h;
            } else {
                if(system.settings.mediaWidth && system.settings.mediaHeight) {
                    system.settings.width = system.settings.currentWidth = Codo(system.DOM.parent.parentNode).getWidth();
                    system.settings.height = system.settings.currentHeight = Codo().getVideoHeight(system.settings.width, system.settings.mediaWidth, system.settings.mediaHeight);
                }
            }
            if(system.media.getPoster || system.media.getParent) {
                if(system.settings.mediaWidth && system.settings.mediaHeight) {
                    util.resize(system.media.getPoster(), system.settings.mediaWidth, system.settings.mediaHeight);
                    util.resize(system.media.getParent(), system.settings.mediaWidth, system.settings.mediaHeight);
                }
            } else {
                util.resize(null, system.settings.currentWidth, system.settings.currentHeight);
            }
        },
        destroy: function() {
            system.media.destroy();
            Codo(parent).remove();
            for (var i = 0; i < CodoPlayerAPI.length; i++) {
                if(CodoPlayerAPI[i].instance === system.instance) CodoPlayerAPI.splice(system.instance, 1);
            };
        },
        onReady: settingsObj.onReady
    };
    // System controls obj
    system.settings.controls = {
        hideDelay: settingsObj.controls.hideDelay || 5,
        fadeDelay: settingsObj.controls.fadeDelay || 20,
        show: function() {
            var show = settingsObj.controls.show || "auto";
            if(Codo().isTouch()) show = "never";
            if(Codo().isMobile()) show = "never";
            if(Codo().isTouch() && !Codo().isMobile()) show = "always";
            return show;
        }(),
        all: function() {
            return (settingsObj.controls.all === false) ? false : true;
        }(),
        play: function() {
            return (settingsObj.controls.play === false) ? false : true;
        }(),
        seek: function() {
            return (settingsObj.controls.seek === false) ? false : true;
        }(),
        seeking: function() {
            return (settingsObj.controls.seeking === false) ? false : true;
        }(),
        volume: function() {
            if(Codo().isTouch()) return;
            if (settingsObj.controls.volume) {
                return settingsObj.controls.volume;
            } else if (settingsObj.controls.volume !== false) {
                return "horizontal";
            }
        }(),
        fullscreen: function() {
            return (settingsObj.controls.fullscreen === false || Codo().isMobile()) ? false : true;
        }(),
        title: function() {
            return (settingsObj.controls.title === false) ? false : true;
        }(),
        time: function() {
            return (settingsObj.controls.time === false) ? false : true;
        }(),
        hd: function() {
            return (settingsObj.controls.hd === false) ? false : true;
        }(),
        playBtn: function() {
            if (Codo().isMobile()) {
                return false;
            } else {
                return (settingsObj.controls.playBtn === false) ? false : true;
            }
        }(),
        loadingText: settingsObj.controls.loadingText || "Loading...",
        foreColor: settingsObj.controls.foreColor || "white",
        backColor: settingsObj.controls.backColor || "#454545",
        bufferColor: settingsObj.controls.bufferColor || "#666666",
        progressColor: settingsObj.controls.progressColor || "#ff0000"
    };

    return system;
};