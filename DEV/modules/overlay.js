var Overlay = function() {
    var isScreenOn = false;
    var playBtnW, playBtnH, menuAreaW, menuAreaH;

    var alpha = 0;
    var step = 25;

    function Toggle() {
        if(!system.media.toggle) return;
        if (!system.settings.preload) {
            if (!system.system.initClickMade) {
                system.media.toggle();
                overlay.off();
                if (loader) loader.on();
                if (Codo().isTouch()) {
                    system.media.getParent().play();
                }
            } else {
                system.media.toggle();
            }
            system.system.initClickMade = true;
        } else {
            system.media.toggle();
        }
    }

    var screenWrap = system.DOM.overlay = Codo(system.DOM.container).add({
        el: "div",
        className: system.className + "-overlay-wrap",
        style: "position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
    });

    Codo(screenWrap).on(clickType, function(e) {
        if(e.stopPropagation && e.preventDefault) {
            e.stopPropagation();
            e.preventDefault();
        }
        Toggle();
    });

    var screenWrapBg = Codo(screenWrap).add({
        el: "div",
        className: system.className + "-overlay-wrap-bg",
        style: "position: absolute; top: 0; left: 0; width: 100%; height: 100%; opacity:0; filter: alpha(opacity=0); visibility: hidden; cursor: pointer;"
    });
    if (system.settings.controls.playBtn) {
        var playBtn = Codo(screenWrapBg).add({
            el: "div",
            className: system.className + "-overlay-play-button",
            style: "cursor: pointer;"
        });

        var menuArea = Codo(system.DOM.container).add({
            el: "div",
            className: system.className + "-overlay-menu",
            style: "position: absolute; min-width: 200px; max-width: 80%; max-height: 60%; vertical-align: middle; font-size: 20px; text-shadow: 0px 0px 1px #000; background: black; background: rgba(0,0,0,.8); visibility: hidden; text-align: center;"
        });

        var menuTitle = Codo(menuArea).add({
            el: "div",
            className: system.className + "-overlay--menu-title",
            style: "line-height: 20px; padding: 0 2px; background: #454545; text-align: right; cursor: pointer;",
            innerHTML: "&#10006;"
        });

        Codo(menuTitle).on(clickType, function(e) {
            if(e.stopPropagation && e.preventDefault) {
                e.stopPropagation();
                e.preventDefault();
            }
            Codo(menuArea).fadeOut(20);
        });

        var menuContext = Codo(menuArea).add({
            el: "a",
            href: "http://codoplayer.com/?ref=" + location.href,
            target: "_blank",
            style: "position: relative; width: 100%; color: white; background: url(" + logoSrc + ") no-repeat center 20px; margin: 0; padding: 50px 0 20px; text-decoration: none; display: block;",
            innerHTML: "{{version}} {{kind}}"
        });

        var timer = setInterval(function() {
            playBtnW = Codo(playBtn).getWidth();
            playBtnH = Codo(playBtn).getHeight();
            menuAreaW = Codo(menuArea).getWidth();
            menuAreaH = Codo(menuArea).getHeight();
            if (playBtnW > 0 && playBtnH > 0) {
                playBtn.style.top = (system.settings.currentHeight - playBtnH) / 2 + "px";
                playBtn.style.left = (system.settings.currentWidth - playBtnW) / 2 + "px";
                menuArea.style.top = (system.settings.currentHeight - menuAreaH) / 2 + "px";
                menuArea.style.left = (system.settings.currentWidth - menuAreaW) / 2 + "px";
                clearInterval(timer);
            }
        }, 20);
        setTimeout(function() {
            clearInterval(timer);
        }, 30000);
    }

    var timer2;

    return {
        resize: function(w, h) {
            if (playBtn && playBtnW && playBtnH) {
                playBtn.style.top = (h - playBtnH) / 2 + "px";
                playBtn.style.left = (w - playBtnW) / 2 + "px";
                menuArea.style.top = (h - menuAreaH) / 2 + "px";
                menuArea.style.left = (w - menuAreaW) / 2 + "px";
            }
        },
        menu: function() {
            Codo(menuArea).fadeIn(20);
        },
        on: function() {
            clearInterval(timer2);
            isScreenOn = true;
            var max = 100;
            screenWrapBg.style.opacity = alpha / 100;
            screenWrapBg.style.filter = "alpha(opacity=" + alpha + ")";
            screenWrapBg.style.visibility = "visible";
            timer2 = setInterval(function() {
                if (alpha < max) {
                    alpha += step;
                    screenWrapBg.style.opacity = alpha / 100;
                    screenWrapBg.style.filter = "alpha(opacity=" + alpha + ")";
                } else {
                    screenWrapBg.style.opacity = max / 100;
                    screenWrapBg.style.filter = "alpha(opacity=" + max + ")";
                    clearInterval(timer2);
                }
            }, 20)
        },
        off: function() {
            clearInterval(timer2);
            isScreenOn = false;
            var max = 0;
            screenWrapBg.style.opacity = alpha / 100;
            screenWrapBg.style.filter = "alpha(opacity=" + alpha + ")";
            timer2 = setInterval(function() {
                if (alpha > max) {
                    alpha -= step;
                    screenWrapBg.style.opacity = alpha / 100;
                    screenWrapBg.style.filter = "alpha(opacity=" + alpha + ")";
                } else {
                    screenWrapBg.style.opacity = max / 100;
                    screenWrapBg.style.filter = "alpha(opacity=" + max + ")";
                    screenWrapBg.style.visibility = "hidden";
                    clearInterval(timer2);
                }
            }, 20)
        },
        getState: function() {
            return isScreenOn;
        }
    }
}