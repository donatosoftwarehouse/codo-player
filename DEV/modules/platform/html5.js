var HTML5 = function(clip, mediaType, autoplay) {

    var isPlaying = false,
    isMetaDataLoaded = false,
    metaObj = {},
    platform,
    poster,
    buffering = false,
    clipBegin = false,
    clipQuarter = false,
    clipCuepoint = false,
    containerScreenCanvas = system.DOM.containerScreenCanvas;
    containerScreenCanvas.innerHTML = "";
    platform = Codo(containerScreenCanvas).add({
        el: mediaType,
        style: "position: absolute; top: 0; left: 0;"
    });
    Codo(containerScreenCanvas).add({
        el: "div",
        style: "position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
    });
    if (!system.settings.preload && !system.system.initClickMade) {
        if (clip.poster) {
            var img = new Image();
            img.src = clip.poster;
            img.onload = function() {
                poster = Codo(containerScreenCanvas).add({
                    el: "img",
                    src: img.src,
                    style: "position: absolute; top: 0; left: 0;"
                });
                util.resize(poster, img.width, img.height);
                if(system.settings.responsive) system.resize();
            }
        }
        controls.title(clip.title || " ");
        if (loader) loader.off("cover");
    } else {
        Load();
    }

    function Load() {
        OnBeforeLoad();
        isMetaDataLoaded = false;
        clipBegin = false;
        clipQuarter = false;
        clipCuepoint = false;
        if (loader) loader.on();
        controls.title(system.settings.controls.loadingText);
        controls.setVolume(system.settings.volume || "0");
        setTimeout(function() {
            if (clip.activeUrl.search("relative://") > -1) {
                platform.src = system.system.rootPath + clip.activeUrl.replace("relative://", "");
            } else {
                platform.src = clip.activeUrl;
            }
            platform.load();
            OnLoad();
            if(Codo().isTouch()) {
                OnMetaData();
            }
        }, 500)
    };
    var Update = function() {
        var timer;
        return {
            start: function() {
                clearInterval(timer);
                var curQ;
                var curC;
                timer = setInterval(function() {
                    OnBuffer();
                    OnProgress();
                    var dur = Math.round(platform.duration);
                    var cur = Math.round(platform.currentTime);
                    var q1 = Math.round(dur / 4);
                    var q2 = Math.round(dur / 2);
                    var q3 = Math.round(dur - dur / 4);
                    if (clipQuarter) {
                        if (cur > (curQ || q1)) clipQuarter = false;
                    }
                    if (clipCuepoint) {
                        if (cur > curC) clipCuepoint = false;
                    }
                    switch (cur) {
                        case Math.round(dur / 4):
                            if (!clipQuarter) {
                                clipQuarter = true;
                                curQ = q1;
                                OnClipFirstQuarter();
                            }
                            break;
                        case Math.round(dur / 2):
                            if (!clipQuarter) {
                                clipQuarter = true;
                                curQ = q2;
                                OnClipSecondQuarter();
                            }
                            break;
                        case Math.round(dur - dur / 4):
                            if (!clipQuarter) {
                                clipQuarter = true;
                                curQ = q3;
                                OnClipThirdQuarter();
                            }
                            break;
                    }
                    if (clip.cuepoints && !clipCuepoint) {
                        if (clip.cuepoints.indexOf(cur) != -1) {
                            clipCuepoint = true;
                            curC = clip.cuepoints[clip.cuepoints.indexOf(cur)];
                            OnCuepoint();
                        }
                    }
                }, 20);
            },
            end: function() {
                clearInterval(timer);
            },
            once: function() {
                OnBuffer();
                OnProgress();
            }
        }
    }();

    function Toggle() {
        if (!isPlaying) {
            Play();
        } else {
            Pause();
        }
    };

    function Play() {
        // if (Codo().isAndroidOrWindows()) {
        //     location.href = clip.activeUrl;
        //     return;
        // }
        if (isMetaDataLoaded) {
            system.system.initClickMade = system.system.initPlayMade = isPlaying = true;
            platform.play();
            controls.play();
        } else if (!system.settings.preload) {
            Load(system.playlist.getCurrentClip())
        }
    };

    function Pause() {
        if (isMetaDataLoaded) {
            isPlaying = false;
            platform.pause();
            controls.pause();
        }
    };

    function SetVolume(vol) {
        if (isMetaDataLoaded) {
            platform.volume = vol / 100;
        } else {
            system.settings.volume = vol;
            controls.setVolume(vol || "0");
        }
    };

    function GetVolume() {
        if (isMetaDataLoaded) {
            return Math.round(platform.volume * 100);
        }
    };

    function GetDuration() {
        if (isMetaDataLoaded) {
            return platform.duration;
        }
    };

    function SetCurrentTime(newTime) {
        if (isMetaDataLoaded) {
            platform.currentTime = newTime;
        }
    };

    function GetCurrentTime() {
            return platform.currentTime || "0";
    };

    function ToggleFullScreen(e) {
        if (!fullscreen.getState()) {
            FullScreenEnter(e);
        } else {
            FullScreenExit(e);
        }
    };

    function FullScreenEnter(e) {
        fullscreen.on(e);
        Update.once();
        OnFullScreenEnter();
    };

    function FullScreenExit(e) {
        fullscreen.off(e);
        Update.once();
        OnFullScreenExit();
    };

    function Destroy() {
        Update.end();
        system.media = {};
        Codo(platform).off("loadedmetadata", OnMetaData);
        Codo(platform).off("play", OnPlay);
        Codo(platform).off("pause", OnPause);
        Codo(platform).off("ended", OnEnd);
        Codo(platform).off("progress", OnBuffer);
        Codo(platform).off("seeking", OnSeekStart);
        Codo(platform).off("seeked", OnSeekEnd);
        Codo(platform).off("volumechange", OnVolumeChange);
        Codo(platform).off("error", OnError);
        if (poster) Codo(poster).remove();
        if (platform) Codo(platform).remove();
    };

    function OnBeforeLoad() {
        for (var i = 0; i < onBeforeLoadCallBk.length; i++) {
            if (onBeforeLoadCallBk[i]) onBeforeLoadCallBk[i]();
        }
    };

    function OnLoad() {
        for (var i = 0; i < onLoadCallBk.length; i++) {
            if (onLoadCallBk[i]) onLoadCallBk[i](system);
        }
    };

    function OnPlay() {
        if (isMetaDataLoaded) {
            isPlaying = true;
            if(overlay) overlay.off();
            if (!buffering) {
                controls.play();
            }
            Update.start();
            if (!clipBegin) {
                clipBegin = true;
                OnClipBegin();
            }
            for (var i = 0; i < onPlayCallBk.length; i++) {
                if (onPlayCallBk[i]) onPlayCallBk[i](platform.currentTime);
            }
        }
    };

    function OnPause() {
        if (isMetaDataLoaded && (metaObj.duration - platform.currentTime) > .1) {
            isPlaying = false;
            if (!buffering) {
                if(overlay) overlay.on();
                controls.pause();
            }
            for (var i = 0; i < onPauseCallBk.length; i++) {
                if (onPauseCallBk[i]) onPauseCallBk[i](platform.currentTime);
            }
        }
    };

    function OnEnd() {
        isPlaying = false;
        system.system.firstClipOver = true;
        controls.pause();
        Update.end();
        if (loader) loader.on();
        OnClipEnd();
        system.playlist.next();
        for (var i = 0; i < onEndCallBk.length; i++) {
            if (onEndCallBk[i]) onEndCallBk[i]();
        }
    };

    function OnVolumeChange() {
        system.settings.volume = GetVolume();
        controls.setVolume(system.settings.volume || "0");
        for (var i = 0; i < onVolumeChange.length; i++) {
            if (onVolumeChange[i]) onVolumeChange[i](system.settings.volume);
        }
    };

    function OnBuffer() {
        if (isMetaDataLoaded) {
            try {
                controls.buffer(platform.buffered.end(0));
                for (var i = 0; i < onBufferCallBk.length; i++) {
                    if (onBufferCallBk[i]) onBufferCallBk[i](platform.buffered.end(0));
                }
            } catch(e) {}
        }
    };

    function OnProgress() {
        if (platform.currentTime) {
            controls.progress(platform.currentTime);
            for (var i = 0; i < onProgressCallBk.length; i++) {
                if (onProgressCallBk[i]) onProgressCallBk[i](platform.currentTime);
            }
        }
    };

    function OnSeekStart() {
        Update.end();
        for (var i = 0; i < onSeekStartCallBk.length; i++) {
            if (onSeekStartCallBk[i]) onSeekStartCallBk[i](platform.currentTime);
        }
    };

    function OnSeekEnd() {
        Update.start();
        for (var i = 0; i < onSeekEndCallBk.length; i++) {
            if (onSeekEndCallBk[i]) onSeekEndCallBk[i](platform.currentTime);
        }
    };

    function OnError() {
        isPlaying = false;
        Update.end();
        var errorMsg = mediaType + " not found";
        errorCtrl.on(errorMsg);
        for (var i = 0; i < onErrorCallBk.length; i++) {
            if (onErrorCallBk[i]) onErrorCallBk[i](errorMsg);
        }
    };

    function OnMetaData() {
        isMetaDataLoaded = true;
        if (mediaType == "video") {
            metaObj.width = platform.videoWidth || system.settings.currentWidth;
            metaObj.height = platform.videoHeight || system.settings.currentHeight;
            metaObj.duration = platform.duration;
            if (poster) poster = Codo(poster).remove();
            util.resize(platform, metaObj.width, metaObj.height);
        } else if (mediaType == "audio") {
            metaObj.duration = platform.duration;
            if (clip.poster) {
                var img = new Image();
                img.src = clip.poster;
                img.onload = function() {
                    metaObj.width = img.width;
                    metaObj.height = img.height;
                    if (poster) poster = Codo(poster).remove();
                    poster = Codo(containerScreenCanvas).add({
                        el: "img",
                        src: img.src,
                        style: "position: absolute; top: 0; left: 0;"
                    });
                    util.resize(poster, img.width, img.height);
                }
            }
        }

        if(system.playlist.breakTime !== "0") {
            SetCurrentTime(system.playlist.breakTime);
        }

        if (autoplay) {
            Play()
        } else {
            if (!system.system.firstClipOver) {
                if (!system.settings.preload || system.settings.autoplay) {
                    Play()
                }
            } else {
                if (clip.hasPrevious || system.settings.loop) {
                    Play()
                }
            }
        }

        if (isPlaying) {
            if (loader) loader.off()
        } else {
            if (loader) loader.off("cover")
        }
        controls.title(clip.title || " ");
        controls.time();
        platform.volume = system.settings.volume / 100;
        Update.once();
        for (var i = 0; i < onMetaDataCallBk.length; i++) {
            if (onMetaDataCallBk[i]) onMetaDataCallBk[i](metaObj);
        }
    };

    function OnFullScreenEnter() {
        for (var i = 0; i < onFullScreenEnterCallBk.length; i++) {
            if (onFullScreenEnterCallBk[i]) onFullScreenEnterCallBk[i]();
        }
    };

    function OnFullScreenExit() {
        for (var i = 0; i < onFullScreenExitCallBk.length; i++) {
            if (onFullScreenExitCallBk[i]) onFullScreenExitCallBk[i]();
        }
    };

    function OnClipBegin() {
        for (var i = 0; i < onClipBegin.length; i++) {
            if (onClipBegin[i]) onClipBegin[i]();
        }
    };

    function OnClipFirstQuarter() {
        for (var i = 0; i < onClipFirstQuarter.length; i++) {
            if (onClipFirstQuarter[i]) onClipFirstQuarter[i]();
        }
    };

    function OnClipSecondQuarter() {
        for (var i = 0; i < onClipSecondQuarter.length; i++) {
            if (onClipSecondQuarter[i]) onClipSecondQuarter[i]();
        }
    };

    function OnClipThirdQuarter() {
        for (var i = 0; i < onClipThirdQuarter.length; i++) {
            if (onClipThirdQuarter[i]) onClipThirdQuarter[i]();
        }
    };

    function OnClipEnd() {
        for (var i = 0; i < onClipEnd.length; i++) {
            if (onClipEnd[i]) onClipEnd[i]();
        }
    };

    function OnCuepoint() {
        for (var i = 0; i < onCuepoint.length; i++) {
            if (onCuepoint[i]) onCuepoint[i]();
        }
    };
    Codo(platform).on("loadedmetadata", OnMetaData);
    Codo(platform).on("play", OnPlay);
    Codo(platform).on("pause", OnPause);
    Codo(platform).on("ended", OnEnd);
    Codo(platform).on("progress", OnBuffer);
    Codo(platform).on("seeking", OnSeekStart);
    Codo(platform).on("seeked", OnSeekEnd);
    Codo(platform).on("volumechange", OnVolumeChange);
    Codo(platform).on("error", OnError);
    return {
        platformName: mediaType == "video" ? "videoHTML5" : "audioHTML5",
        isPlaying: function() {
            return isPlaying;
        },
        isMetaDataLoaded: function() {
            return isMetaDataLoaded;
        },
        onBeforeLoad: function(callBk) {
            if (callBk) {
                onBeforeLoadCallBk.push(callBk)
            }
        },
        onLoad: function(callBk) {
            if (callBk) {
                onLoadCallBk.push(callBk)
            }
        },
        onMetaData: function(callBk) {
            if (callBk) {
                onMetaDataCallBk.push(callBk)
            }
        },
        onPlay: function(callBk) {
            if (callBk) {
                onPlayCallBk.push(callBk)
            }
        },
        onPause: function(callBk) {
            if (callBk) {
                onPauseCallBk.push(callBk)
            }
        },
        onEnd: function(callBk) {
            if (callBk) {
                onEndCallBk.push(callBk)
            }
        },
        onBuffer: function(callBk) {
            if (callBk) {
                onBufferCallBk.push(callBk)
            }
        },
        onProgress: function(callBk) {
            if (callBk) {
                onProgressCallBk.push(callBk)
            }
        },
        onSeekStart: function(callBk) {
            if (callBk) {
                onSeekStartCallBk.push(callBk)
            }
        },
        onSeekEnd: function(callBk) {
            if (callBk) {
                onSeekEndCallBk.push(callBk)
            }
        },
        onVolumeChange: function(callBk) {
            if (callBk) {
                onVolumeChange.push(callBk)
            }
        },
        onFullScreenEnter: function(callBk) {
            if (callBk) {
                onFullScreenEnterCallBk.push(callBk)
            }
        },
        onFullScreenExit: function(callBk) {
            if (callBk) {
                onFullScreenExitCallBk.push(callBk)
            }
        },
        onError: function(callBk) {
            if (callBk) {
                onErrorCallBk.push(callBk)
            }
        },
        getParent: function() {
            return platform;
        },
        getPoster: function() {
            return poster;
        },
        toggle: function() {
            Toggle();
        },
        play: function(_clip, _autoplay) {
            if(_clip) {
                clip = _clip;
                autoplay = _autoplay;
                Load();
            } else {
                Play();
            }
        },
        pause: function() {
            Pause();
        },
        setVolume: function(volume) {
            SetVolume(volume);
        },
        getVolume: function() {
            return GetVolume();
        },
        getDuration: function() {
            return GetDuration();
        },
        setCurrentTime: function(newTime) {
            SetCurrentTime(newTime);
        },
        getCurrentTime: function() {
            return GetCurrentTime();
        },
        toggleFullScreen: function(e) {
            ToggleFullScreen(e);
        },
        fullScreenEnter: function() {
            FullScreenEnter();
        },
        fullScreenExit: function() {
            FullScreenExit();
        },
        destroy: function() {
            Destroy();
        },
        onClipBegin: function(callBk) {
            if (callBk) {
                onClipBegin.push(callBk)
            }
        },
        onClipFirstQuarter: function(callBk) {
            if (callBk) {
                onClipFirstQuarter.push(callBk)
            }
        },
        onClipSecondQuarter: function(callBk) {
            if (callBk) {
                onClipSecondQuarter.push(callBk)
            }
        },
        onClipThirdQuarter: function(callBk) {
            if (callBk) {
                onClipThirdQuarter.push(callBk)
            }
        },
        onClipEnd: function(callBk) {
            if (callBk) {
                onClipEnd.push(callBk)
            }
        },
        onCuepoint: function(callBk) {
            if (callBk) {
                onCuepoint.push(callBk)
            }
        }
    }
};