var SWF = function(clip, mediaType, autoplay) {

    var isPlaying = false,
    isMetaDataLoaded = false,
    metaObj = {},
    platform,
    poster,
    buffering = false,
    clipBegin = false,
    clipQuarter = false,
    clipCuepoint = false,
    containerScreenCanvas = system.DOM.containerScreenCanvas;

    containerScreenCanvas.innerHTML = "";
    if (!Codo().isFlash()) errorCtrl.on("Flash plugin not found");
    containerScreenCanvas.innerHTML = "<object id='" + system.id + "-" + mediaType + "-swf' name='" + system.id + "-" + mediaType + "-swf' width='" + system.settings.currentWidth + "' height='" + system.settings.currentHeight + "' type='application/x-shockwave-flash' data='" + system.system.rootPath + "module.swf' style='position: absolute; top: 0; left: 0;'><param name='movie' value='" + system.system.rootPath + "module.swf'><param name='quality' value='high'><param name='allowScriptAccess' value='always'><param name='swliveconnect' value='true'><param name='wmode' value='transparent'><param name='flashVars' value='instance=" + system.instance + "&mediaType=" + mediaType + "'></object>";
    platform = Codo("#" + system.id + "-" + mediaType + "-swf").get()[0];
    Codo(containerScreenCanvas).add({
        el: "div",
        style: "position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: black; opacity: 0; filter: alpha(opacity=0);"
    });

    function OnSwfLoaded() {
        platform.initClip(system.settings, system.playlist.getCurrentClip());
        if (!clip.rtmp) swfLoaded();
    };

    function OnRtmpLoaded() {
        swfLoaded();
    };

    function swfLoaded() {
        if (!system.settings.preload && !system.system.initClickMade) {
            if (clip.poster) {
                var img = new Image();
                img.src = clip.poster;
                img.onload = function() {
                    poster = Codo(containerScreenCanvas).add({
                        el: "img",
                        src: img.src,
                        style: "position: absolute; top: 0; left: 0;"
                    });
                    util.resize(poster, img.width, img.height);
                    if(system.settings.responsive) system.resize();
                    //overlay.on();
                }
            }
            controls.title(clip.title || " ");
            if (loader) loader.off("cover");
        } else {
            Load();
        }
    };

    function Load() {
        OnBeforeLoad();
        isMetaDataLoaded = false;
        clipBegin = false;
        clipQuarter = false;
        clipCuepoint = false;
        if (loader) loader.on();
        controls.title(system.settings.controls.loadingText);
        controls.setVolume(system.settings.volume || "0");
        if (clip.activeUrl.search("relative://") > -1) {
            platform.setSrc(system.system.rootPath + clip.activeUrl.replace("relative://", ""));
        } else {
            platform.setSrc(clip.activeUrl);
        }
        OnLoad();
        if(Codo().isTouch()) {
            OnMetaData();
        }
    };
    var Update = function() {
        var timer;
        return {
            start: function() {
                clearInterval(timer);
                var curQ;
                var curC;
                timer = setInterval(function() {
                    if (platform.getDuration) {
                        var dur = Math.round(platform.getDuration());
                    } else {
                        return;
                    };
                    if (platform.getCurrentTime) {
                        var cur = Math.round(platform.getCurrentTime());
                    } else {
                        return;
                    };
                    OnProgress();
                    var q1 = Math.round(dur / 4);
                    var q2 = Math.round(dur / 2);
                    var q3 = Math.round(dur - dur / 4);
                    if (clipQuarter) {
                        if (cur > (curQ || q1)) clipQuarter = false;
                    }
                    if (clipCuepoint) {
                        if (cur > curC) clipCuepoint = false;
                    }
                    switch (cur) {
                        case Math.round(dur / 4):
                            if (!clipQuarter) {
                                clipQuarter = true;
                                curQ = q1;
                                OnClipFirstQuarter();
                            }
                            break;
                        case Math.round(dur / 2):
                            if (!clipQuarter) {
                                clipQuarter = true;
                                curQ = q2;
                                OnClipSecondQuarter();
                            }
                            break;
                        case Math.round(dur - dur / 4):
                            if (!clipQuarter) {
                                clipQuarter = true;
                                curQ = q3;
                                OnClipThirdQuarter();
                            }
                            break;
                    }
                    if (clip.cuepoints && !clipCuepoint) {
                        if (clip.cuepoints.indexOf(cur) != -1) {
                            clipCuepoint = true;
                            curC = clip.cuepoints[clip.cuepoints.indexOf(cur)];
                            OnCuepoint();
                        }
                    }
                }, 100);
            },
            end: function() {
                clearInterval(timer);
            },
            once: function() {
                OnProgress();
            }
        }
    }();

    function Toggle() {
        if (!isPlaying) {
            Play();
        } else {
            Pause();
        }
    };

    function Play(_clip) {
        if (isMetaDataLoaded) {
            system.system.initClickMade = system.system.initPlayMade = isPlaying = true;
            platform.playClip();
            if (clip.rtmp) {
                OnPlay();
            }
            controls.play();
        } else if (!system.settings.preload) {
            Load(system.playlist.getCurrentClip());
        }
    };

    function Pause() {
        if (isMetaDataLoaded) {
            isPlaying = false;
            platform.pauseClip();
            if (clip.rtmp) {
                OnPause();
            }
            controls.pause();
        }
    };

    function SetVolume(vol) {
        if (isMetaDataLoaded) {
            platform.setVolume(vol / 100 || "0");
        } else {
            system.settings.volume = vol;
            controls.setVolume(vol || "0");
        }
        OnVolumeChange(vol);
    };

    function GetVolume() {
        if (isMetaDataLoaded) {
            return Math.round(platform.getVolume() * 100);
        }
    };

    function GetDuration() {
        if (isMetaDataLoaded && platform.getDuration) {
            return platform.getDuration();
        }
    };

    function SetCurrentTime(newTime) {
        if (isMetaDataLoaded) {
            platform.setCurrentTime(newTime);
        }
    };

    function GetCurrentTime() {
        if (!platform.getCurrentTime) return;
        return platform.getCurrentTime() || "0";
    };

    function ToggleFullScreen(e) {
        if (!fullscreen.getState()) {
            FullScreenEnter(e);
        } else {
            FullScreenExit(e);
        }
    };

    function FullScreenEnter(e) {
        fullscreen.on(e);
        Update.once();
        OnFullScreenEnter();
    };

    function FullScreenExit(e) {
        fullscreen.off(e);
        Update.once();
        OnFullScreenExit();
    };

    function Destroy() {
        Update.end();
        system.API = {};
        if (poster) Codo(poster).remove();
        if (platform) {
            Codo(platform).remove();
        }
    };

    function OnBeforeLoad() {
        for (var i = 0; i < onBeforeLoadCallBk.length; i++) {
            if (onBeforeLoadCallBk[i]) onBeforeLoadCallBk[i]();
        }
    };

    function OnLoad() {
        for (var i = 0; i < onLoadCallBk.length; i++) {
            if (onLoadCallBk[i]) onLoadCallBk[i]();
        }
    };

    function OnPlay() {
        if (isMetaDataLoaded) {
            isPlaying = true;
            if(overlay) overlay.off();
            if (!buffering) {
                controls.play();
            }
            Update.start();
            if (!clipBegin) {
                clipBegin = true;
                OnClipBegin();
            }
            for (var i = 0; i < onPlayCallBk.length; i++) {
                if (onPlayCallBk[i]) onPlayCallBk[i](platform.getCurrentTime());
            }
        }
    };

    function OnPause() {
        if (isMetaDataLoaded) {
            isPlaying = false;
            if (!buffering) {
                if(overlay) overlay.on();
                controls.pause();
            }
            for (var i = 0; i < onPauseCallBk.length; i++) {
                if (onPauseCallBk[i]) onPauseCallBk[i](platform.getCurrentTime());
            }
        }
    };

    function OnEnd() {
        isPlaying = false;
        system.system.firstClipOver = true;
        controls.pause();
        Update.end();
        if (loader) loader.on();
        OnClipEnd();
        system.playlist.next();
        for (var i = 0; i < onEndCallBk.length; i++) {
            if (onEndCallBk[i]) onEndCallBk[i]();
        }
    };

    function OnBuffer(buffer) {
        if (isMetaDataLoaded) {
            controls.buffer(buffer);
            for (var i = 0; i < onBufferCallBk.length; i++) {
                if (onBufferCallBk[i]) onBufferCallBk[i](buffer);
            }
        }
    };

    function OnProgress() {
        if (platform.getCurrentTime && platform.getCurrentTime()) {
            controls.progress(platform.getCurrentTime());
            for (var i = 0; i < onProgressCallBk.length; i++) {
                if (onProgressCallBk[i] && platform.getCurrentTime) onProgressCallBk[i](platform.getCurrentTime());
            }
        }
    };

    function OnSeekStart() {
        Update.end();
        for (var i = 0; i < onSeekStartCallBk.length; i++) {
            if (onSeekStartCallBk[i]) onSeekStartCallBk[i](platform.getCurrentTime());
        }
    };

    function OnSeekEnd() {
        Update.start();
        for (var i = 0; i < onSeekEndCallBk.length; i++) {
            if (onSeekEndCallBk[i]) onSeekEndCallBk[i](platform.getCurrentTime());
        }
    };

    function OnVolumeChange(vol) {
        system.settings.volume = vol;
        controls.setVolume(vol || "0");
        for (var i = 0; i < onVolumeChange.length; i++) {
            if (onVolumeChange[i]) onVolumeChange[i](system.settings.volume);
        }
    };

    function OnError() {
        isPlaying = false;
        Update.end();
        var errorMsg = mediaType + " not found";
        errorCtrl.on(errorMsg);
        for (var i = 0; i < onErrorCallBk.length; i++) {
            if (onErrorCallBk[i]) onErrorCallBk[i](errorMsg);
        }
    };

    function OnMetaData(_metaObj) {
        isMetaDataLoaded = true;
        if (mediaType == "video") {
            metaObj = _metaObj;
            if (poster) poster = Codo(poster).remove();
            util.resize(platform, metaObj.width, metaObj.height);
        } else if (mediaType == "audio") {
            if (clip.poster) {
                var img = new Image();
                img.src = clip.poster;
                img.onload = function() {
                    metaObj.width = img.width;
                    metaObj.height = img.height;
                    if (poster) poster = Codo(poster).remove();
                    poster = Codo(containerScreenCanvas).add({
                        el: "img",
                        src: img.src,
                        style: "position: absolute; top: 0; left: 0;"
                    });
                    util.resize(poster, img.width, img.height);
                }
            }
        }

        if(system.playlist.breakTime !== "0") {
            SetCurrentTime(system.playlist.breakTime);
        }

        if (autoplay) {
            Play()
        } else {
            if (!system.system.firstClipOver) {
                if (!system.settings.preload || system.settings.autoplay) {
                    Play()
                }
            } else {
                if (clip.hasPrevious || system.settings.loop) {
                    Play()
                }
            }
        }

        if (isPlaying) {
            if (loader) loader.off()
        } else {
            if (loader) loader.off("cover")
        }
        controls.title(clip.title || " ");
        controls.time();
        platform.setVolume(system.settings.volume / 100 || "0");
        Update.once();
        for (var i = 0; i < onMetaDataCallBk.length; i++) {
            if (onMetaDataCallBk[i]) onMetaDataCallBk[i](metaObj);
        }
    };

    function OnFullScreenEnter() {
        for (var i = 0; i < onFullScreenEnterCallBk.length; i++) {
            if (onFullScreenEnterCallBk[i]) onFullScreenEnterCallBk[i]();
        }
    };

    function OnFullScreenExit() {
        for (var i = 0; i < onFullScreenExitCallBk.length; i++) {
            if (onFullScreenExitCallBk[i]) onFullScreenExitCallBk[i]();
        }
    };

    function OnClipBegin() {
        for (var i = 0; i < onClipBegin.length; i++) {
            if (onClipBegin[i]) onClipBegin[i]();
        }
    };

    function OnClipFirstQuarter() {
        for (var i = 0; i < onClipFirstQuarter.length; i++) {
            if (onClipFirstQuarter[i]) onClipFirstQuarter[i]();
        }
    };

    function OnClipSecondQuarter() {
        for (var i = 0; i < onClipSecondQuarter.length; i++) {
            if (onClipSecondQuarter[i]) onClipSecondQuarter[i]();
        }
    };

    function OnClipThirdQuarter() {
        for (var i = 0; i < onClipThirdQuarter.length; i++) {
            if (onClipThirdQuarter[i]) onClipThirdQuarter[i]();
        }
    };

    function OnClipEnd() {
        for (var i = 0; i < onClipEnd.length; i++) {
            if (onClipEnd[i]) onClipEnd[i]();
        }
    };

    function OnCuepoint() {
        for (var i = 0; i < onCuepoint.length; i++) {
            if (onCuepoint[i]) onCuepoint[i]();
        }
    };
    return {
        platformName: mediaType == "video" ? "videoSWF" : "audioSWF",
        isPlaying: function() {
            return isPlaying;
        },
        isMetaDataLoaded: function() {
            return isMetaDataLoaded;
        },
        onBeforeLoad: function(callBk) {
            if (callBk) {
                onBeforeLoadCallBk.push(callBk)
            }
        },
        onLoad: function(callBk) {
            if (callBk) {
                onLoadCallBk.push(callBk)
            }
        },
        onMetaData: function(callBk) {
            if (callBk) {
                onMetaDataCallBk.push(callBk)
            }
        },
        onPlay: function(callBk) {
            if (callBk) {
                onPlayCallBk.push(callBk)
            }
        },
        onPause: function(callBk) {
            if (callBk) {
                onPauseCallBk.push(callBk)
            }
        },
        onEnd: function(callBk) {
            if (callBk) {
                onEndCallBk.push(callBk)
            }
        },
        onBuffer: function(callBk) {
            if (callBk) {
                onBufferCallBk.push(callBk)
            }
        },
        onProgress: function(callBk) {
            if (callBk) {
                onProgressCallBk.push(callBk)
            }
        },
        onSeekStart: function(callBk) {
            if (callBk) {
                onSeekStartCallBk.push(callBk)
            }
        },
        onSeekEnd: function(callBk) {
            if (callBk) {
                onSeekEndCallBk.push(callBk)
            }
        },
        onVolumeChange: function(callBk) {
            if (callBk) {
                onVolumeChange.push(callBk)
            }
        },
        onFullScreenEnter: function(callBk) {
            if (callBk) {
                onFullScreenEnterCallBk.push(callBk)
            }
        },
        onFullScreenExit: function(callBk) {
            if (callBk) {
                onFullScreenExitCallBk.push(callBk)
            }
        },
        onError: function(callBk) {
            if (callBk) {
                onErrorCallBk.push(callBk)
            }
        },
        system: {
            onSwfLoaded: function() {
                OnSwfLoaded();
            },
            onRtmpLoaded: function() {
                OnRtmpLoaded();
            },
            onPlay: function() {
                OnPlay();
            },
            onPause: function() {
                OnPause();
            },
            onEnd: function() {
                OnEnd();
            },
            onWaiting: function() {
                OnWaiting();
            },
            onSeekStart: function() {
                OnSeekStart();
            },
            onSeekEnd: function() {
                OnSeekEnd();
            },
            onBuffer: function(buffer) {
                OnBuffer(buffer);
            },
            onMetaData: function(metaObj) {
                OnMetaData(metaObj);
            },
            onError: function() {
                OnError();
            }
        },
        getParent: function() {
            return platform;
        },
        getPoster: function() {
            return poster;
        },
        toggle: function() {
            Toggle();
        },
        play: function(_clip, _autoplay) {
            if(_clip) {
                clip = _clip;
                autoplay = _autoplay;
                Load();
            } else {
                Play();
            }
        },
        pause: function() {
            Pause();
        },
        setVolume: function(volume) {
            SetVolume(volume);
        },
        getVolume: function(val) {
            return GetVolume();
        },
        getDuration: function() {
            return GetDuration();
        },
        setCurrentTime: function(newTime) {
            SetCurrentTime(newTime);
        },
        getCurrentTime: function() {
            return GetCurrentTime();
        },
        toggleFullScreen: function(e) {
            ToggleFullScreen(e);
        },
        fullScreenEnter: function(e) {
            FullScreenEnter(e);
        },
        fullScreenExit: function(e) {
            FullScreenExit(e);
        },
        destroy: function() {
            Destroy();
        },
        onClipBegin: function(callBk) {
            if (callBk) {
                onClipBegin.push(callBk)
            }
        },
        onClipFirstQuarter: function(callBk) {
            if (callBk) {
                onClipFirstQuarter.push(callBk)
            }
        },
        onClipSecondQuarter: function(callBk) {
            if (callBk) {
                onClipSecondQuarter.push(callBk)
            }
        },
        onClipThirdQuarter: function(callBk) {
            if (callBk) {
                onClipThirdQuarter.push(callBk)
            }
        },
        onClipEnd: function(callBk) {
            if (callBk) {
                onClipEnd.push(callBk)
            }
        },
        onCuepoint: function(callBk) {
            if (callBk) {
                onCuepoint.push(callBk)
            }
        }
    }
};