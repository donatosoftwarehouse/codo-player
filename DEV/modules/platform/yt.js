

var YOUTUBE = function(clip, type, autoplay) {

    var isPlaying = false,
    isMetaDataLoaded = false,
    metaObj = {},
    platform,
    poster,
    buffering = false,
    clipBegin = false,
    clipQuarter = false,
    clipCuepoint = false,
    isPauseFix = false,
    playerReady = false,
    parentEl,
    containerScreenCanvas = system.DOM.containerScreenCanvas;

    containerScreenCanvas.innerHTML = "";

    Codo(containerScreenCanvas).add({
        el: "div",
        id: system.id + "-youtube-iframe",
        style: "position: absolute; top: 0; left: 0;"
    });
    Codo().script("//www.youtube.com/iframe_api");
    var loadTimer = setInterval(function() {
        if (YTiframeReady) {
            platform = new YT.Player(system.id + "-youtube-iframe", {
                width: system.settings.currentWidth,
                height: system.settings.currentHeight,
                playerVars: {
                    "controls": 0,
                    "showinfo": 0
                },
                events: {
                    "onReady": function(event) {
                        playerReady = true;
                        parentEl = Codo("#" + system.id + "-youtube-iframe").get()[0];
                        if (!system.settings.preload && !system.system.initClickMade) {
                            if (clip.poster) {
                                var img = new Image();
                                img.src = clip.poster;
                                img.onload = function() {
                                    poster = Codo(containerScreenCanvas).add({
                                        el: "img",
                                        src: img.src,
                                        style: "position: absolute; top: 0; left: 0;"
                                    });
                                    util.resize(poster, img.width, img.height);
                                    if(system.settings.responsive) system.resize();
                                    //overlay.on();
                                }
                            }
                            controls.title(clip.title || " ");
                            if (loader) loader.off("cover");
                        } else {
                            Load();
                        }
                    },
                    "onStateChange": function(event) {
                        switch (event.data) {
                            case YT.PlayerState.PLAYING:
                                if (!isMetaDataLoaded) {
                                    OnMetaData();
                                } else {
                                    OnPlay();
                                }
                                break;
                            case YT.PlayerState.PAUSED:
                                if (!isPauseFix) {
                                    isPauseFix = true;
                                } else {
                                    OnPause();
                                }
                                break;
                            case YT.PlayerState.ENDED:
                                OnEnd();
                                break;
                        }
                    },
                    "onError": function(code) {
                        OnError(code);
                    }
                }
            });
            clearInterval(loadTimer);
        }
    }, 100);

    function Load() {
        if (playerReady) {
            OnBeforeLoad();
            isMetaDataLoaded = false;
            isPauseFix = false;
            clipBegin = false;
            clipQuarter = false;
            clipCuepoint = false;
            if (loader) loader.on();
            controls.title(system.settings.controls.loadingText);
            controls.setVolume(system.settings.volume || "0");
            setTimeout(function() {
                platform.loadVideoById(clip.activeUrl);
                OnLoad();
                if(Codo().isTouch()) {
                    OnMetaData();
                }
            }, 500)
        }
    };
    var Update = function() {
        var timer;
        return {
            start: function() {
                clearInterval(timer);
                var curQ;
                var curC;
                timer = setInterval(function() {
                    OnBuffer();
                    OnProgress();
                    var dur = Math.round(platform.getDuration());
                    var cur = Math.round(platform.getCurrentTime());
                    var q1 = Math.round(dur / 4);
                    var q2 = Math.round(dur / 2);
                    var q3 = Math.round(dur - dur / 4);
                    if (clipQuarter) {
                        if (cur > (curQ || q1)) clipQuarter = false;
                    }
                    if (clipCuepoint) {
                        if (cur > curC) clipCuepoint = false;
                    }
                    switch (cur) {
                        case Math.round(dur / 4):
                            if (!clipQuarter) {
                                clipQuarter = true;
                                curQ = q1;
                                OnClipFirstQuarter();
                            }
                            break;
                        case Math.round(dur / 2):
                            if (!clipQuarter) {
                                clipQuarter = true;
                                curQ = q2;
                                OnClipSecondQuarter();
                            }
                            break;
                        case Math.round(dur - dur / 4):
                            if (!clipQuarter) {
                                clipQuarter = true;
                                curQ = q3;
                                OnClipThirdQuarter();
                            }
                            break;
                    }
                    if (clip.cuepoints && !clipCuepoint) {
                        if (clip.cuepoints.indexOf(cur) != -1) {
                            clipCuepoint = true;
                            curC = clip.cuepoints[clip.cuepoints.indexOf(cur)];
                            OnCuepoint();
                        }
                    }
                }, 100);
            },
            end: function() {
                clearInterval(timer);
            },
            once: function() {
                OnBuffer();
                OnProgress();
            }
        }
    }();

    function Toggle() {
        if (!isPlaying) {
            Play();
        } else {
            Pause();
        }
    };

    function Play(_clip) {
        if (isMetaDataLoaded) {
            system.system.initClickMade = system.system.initPlayMade = isPlaying = true;
            platform.playVideo();
            controls.play();
        } else if (!system.settings.preload) {
            Load(system.playlist.getCurrentClip())
        }
    };

    function Pause() {
        if (isMetaDataLoaded) {
            isPlaying = false;
            platform.pauseVideo();
            controls.pause();
        }
    };

    function SetVolume(vol) {
        if (isMetaDataLoaded) {
            platform.setVolume(vol);
        } else {
            system.settings.volume = vol;
            controls.setVolume(vol || "0");
        }
        OnVolumeChange(vol);
    };

    function GetVolume() {
        if (isMetaDataLoaded) {
            return platform.getVolume();
        }
    };

    function GetDuration() {
        if (isMetaDataLoaded) {
            return platform.getDuration();
        }
    };

    function SetCurrentTime(newTime) {
        if (isMetaDataLoaded) {
            platform.seekTo(newTime);
        }
    };

    function GetCurrentTime() {
        return platform.getCurrentTime() || "0";
    };

    function ToggleFullScreen(e) {
        if (!fullscreen.getState()) {
            FullScreenEnter(e);
        } else {
            FullScreenExit(e);
        }
    };

    function FullScreenEnter(e) {
        fullscreen.on(e);
        Update.once();
        OnFullScreenEnter();
    };

    function FullScreenExit(e) {
        fullscreen.off(e);
        Update.once();
        OnFullScreenExit();
    };

    function Destroy() {
        Update.end();
        system.API = {};
        if (poster) Codo(poster).remove();
        if (parentEl) Codo(parentEl).remove();
    };

    function OnBeforeLoad() {
        for (var i = 0; i < onBeforeLoadCallBk.length; i++) {
            if (onBeforeLoadCallBk[i]) onBeforeLoadCallBk[i]();
        }
    };

    function OnLoad() {
        for (var i = 0; i < onLoadCallBk.length; i++) {
            if (onLoadCallBk[i]) onLoadCallBk[i]();
        }
    };

    function OnPlay() {
        if (isMetaDataLoaded) {
            isPlaying = true;
            if(overlay) overlay.off();
            if (!buffering) {
                controls.play();
            }
            Update.start();
            if (!clipBegin) {
                clipBegin = true;
                OnClipBegin();
            }
            for (var i = 0; i < onPlayCallBk.length; i++) {
                if (onPlayCallBk[i]) onPlayCallBk[i](platform.getCurrentTime());
            }
        }
    };

    function OnPause() {
        if (isMetaDataLoaded && (platform.getDuration() - platform.getCurrentTime()) > .1) {
            isPlaying = false;
            if (!buffering) {
                if(overlay) overlay.on();
                controls.pause();
            }
            for (var i = 0; i < onPauseCallBk.length; i++) {
                if (onPauseCallBk[i]) onPauseCallBk[i](platform.getCurrentTime());
            }
        }
    };

    function OnEnd() {
        isPlaying = false;
        system.system.firstClipOver = true;
        controls.pause();
        Update.end();
        Loading.on();
        OnClipEnd();
        system.playlist.next();
        for (var i = 0; i < onEndCallBk.length; i++) {
            if (onEndCallBk[i]) onEndCallBk[i]();
        }
    };

    function OnVolumeChange(vol) {
        system.settings.volume = vol;
        controls.setVolume(vol || "0");
        for (var i = 0; i < onVolumeChange.length; i++) {
            if (onVolumeChange[i]) onVolumeChange[i](system.settings.volume);
        }
    };

    function OnBuffer() {
        if (isMetaDataLoaded) {
            controls.buffer(platform.getVideoLoadedFraction() * 100 * platform.getDuration() / 100);
            for (var i = 0; i < onBufferCallBk.length; i++) {
                if (onBufferCallBk[i]) onBufferCallBk[i](platform.getVideoLoadedFraction() * 100 * platform.getDuration() / 100);
            }
        }
    };

    function OnProgress() {
        if (platform.getCurrentTime && platform.getCurrentTime()) {
            controls.progress(platform.getCurrentTime());
            for (var i = 0; i < onProgressCallBk.length; i++) {
                if (onProgressCallBk[i]) onProgressCallBk[i](platform.getCurrentTime());
            }
        }
    };

    function OnSeekStart() {
        Update.end();
        for (var i = 0; i < onSeekStartCallBk.length; i++) {
            if (onSeekStartCallBk[i]) onSeekStartCallBk[i](platform.getCurrentTime());
        }
    };

    function OnSeekEnd() {
        Update.start();
        for (var i = 0; i < onSeekEndCallBk.length; i++) {
            if (onSeekEndCallBk[i]) onSeekEndCallBk[i](platform.getCurrentTime());
        }
    };

    function OnError() {
        isPlaying = false;
        Update.end();
        var errorMsg = type + " not found";
        errorCtrl.on(errorMsg);
        for (var i = 0; i < onErrorCallBk.length; i++) {
            if (onErrorCallBk[i]) onErrorCallBk[i](errorMsg);
        }
    };

    function OnMetaData() {
        isMetaDataLoaded = true;
        metaObj.width = system.settings.currentWidth; //platform.videoWidth;
        metaObj.height = system.settings.currentHeight; //platform.videoHeight;
        metaObj.duration = platform.getDuration();
        if (poster) poster = Codo(poster).remove();
        util.resize(parentEl, metaObj.width, metaObj.height);
        platform.pauseVideo();

        if(system.playlist.breakTime !== "0") {
            SetCurrentTime(system.playlist.breakTime);
        }

        if (autoplay) {
            Play()
        } else {
            if (!system.system.firstClipOver) {
                if (!system.settings.preload || system.settings.autoplay) {
                    Play()
                }
            } else {
                if (clip.hasPrevious || system.settings.loop) {
                    Play()
                }
            }
        }
        if (isPlaying) {
            if (loader) loader.off()
        } else {
            if (loader) loader.off("cover")
        }
        controls.title(clip.title || " ");
        controls.time();
        platform.setVolume(system.settings.volume);
        Update.once();
        for (var i = 0; i < onMetaDataCallBk.length; i++) {
            if (onMetaDataCallBk[i]) onMetaDataCallBk[i](metaObj);
        }
    };

    function OnFullScreenEnter() {
        for (var i = 0; i < onFullScreenEnterCallBk.length; i++) {
            if (onFullScreenEnterCallBk[i]) onFullScreenEnterCallBk[i]();
        }
    };

    function OnFullScreenExit() {
        for (var i = 0; i < onFullScreenExitCallBk.length; i++) {
            if (onFullScreenExitCallBk[i]) onFullScreenExitCallBk[i]();
        }
    };

    function OnClipBegin() {
        for (var i = 0; i < onClipBegin.length; i++) {
            if (onClipBegin[i]) onClipBegin[i]();
        }
    };

    function OnClipFirstQuarter() {
        for (var i = 0; i < onClipFirstQuarter.length; i++) {
            if (onClipFirstQuarter[i]) onClipFirstQuarter[i]();
        }
    };

    function OnClipSecondQuarter() {
        for (var i = 0; i < onClipSecondQuarter.length; i++) {
            if (onClipSecondQuarter[i]) onClipSecondQuarter[i]();
        }
    };

    function OnClipThirdQuarter() {
        for (var i = 0; i < onClipThirdQuarter.length; i++) {
            if (onClipThirdQuarter[i]) onClipThirdQuarter[i]();
        }
    };

    function OnClipEnd() {
        for (var i = 0; i < onClipEnd.length; i++) {
            if (onClipEnd[i]) onClipEnd[i]();
        }
    };

    function OnCuepoint() {
        for (var i = 0; i < onCuepoint.length; i++) {
            if (onCuepoint[i]) onCuepoint[i]();
        }
    };
    return {
        platformName: "YOUTUBE",
        isPlaying: function() {
            return isPlaying;
        },
        isMetaDataLoaded: function() {
            return isMetaDataLoaded;
        },
        onBeforeLoad: function(callBk) {
            if (callBk) {
                onBeforeLoadCallBk.push(callBk)
            }
        },
        onLoad: function(callBk) {
            if (callBk) {
                onLoadCallBk.push(callBk)
            }
        },
        onMetaData: function(callBk) {
            if (callBk) {
                onMetaDataCallBk.push(callBk)
            }
        },
        onPlay: function(callBk) {
            if (callBk) {
                onPlayCallBk.push(callBk)
            }
        },
        onPause: function(callBk) {
            if (callBk) {
                onPauseCallBk.push(callBk)
            }
        },
        onEnd: function(callBk) {
            if (callBk) {
                onEndCallBk.push(callBk)
            }
        },
        onBuffer: function(callBk) {
            if (callBk) {
                onBufferCallBk.push(callBk)
            }
        },
        onProgress: function(callBk) {
            if (callBk) {
                onProgressCallBk.push(callBk)
            }
        },
        onSeekStart: function(callBk) {
            if (callBk) {
                onSeekStartCallBk.push(callBk)
            }
        },
        onSeekEnd: function(callBk) {
            if (callBk) {
                onSeekEndCallBk.push(callBk)
            }
        },
        onVolumeChange: function(callBk) {
            if (callBk) {
                onVolumeChange.push(callBk)
            }
        },
        onFullScreenEnter: function(callBk) {
            if (callBk) {
                onFullScreenEnterCallBk.push(callBk)
            }
        },
        onFullScreenExit: function(callBk) {
            if (callBk) {
                onFullScreenExitCallBk.push(callBk)
            }
        },
        onError: function(callBk) {
            if (callBk) {
                onErrorCallBk.push(callBk)
            }
        },
        getParent: function() {
            return parentEl;
        },
        getPoster: function() {
            return poster;
        },
        toggle: function() {
            Toggle();
        },
        play: function(_clip, _autoplay) {
            if(_clip) {
                clip = _clip;
                autoplay = _autoplay;
                Load();
            } else {
                Play();
            }
        },
        pause: function() {
            Pause();
        },
        setVolume: function(volume) {
            SetVolume(volume);
        },
        getVolume: function() {
            return GetVolume();
        },
        getDuration: function() {
            return GetDuration();
        },
        setCurrentTime: function(newTime) {
            SetCurrentTime(newTime);
        },
        getCurrentTime: function() {
            return GetCurrentTime();
        },
        toggleFullScreen: function(e) {
            ToggleFullScreen(e);
        },
        fullScreenEnter: function(e) {
            FullScreenEnter(e);
        },
        fullScreenExit: function(e) {
            FullScreenExit(e);
        },
        destroy: function() {
            Destroy();
        },
        onClipBegin: function(callBk) {
            if (callBk) {
                onClipBegin.push(callBk)
            }
        },
        onClipFirstQuarter: function(callBk) {
            if (callBk) {
                onClipFirstQuarter.push(callBk)
            }
        },
        onClipSecondQuarter: function(callBk) {
            if (callBk) {
                onClipSecondQuarter.push(callBk)
            }
        },
        onClipThirdQuarter: function(callBk) {
            if (callBk) {
                onClipThirdQuarter.push(callBk)
            }
        },
        onClipEnd: function(callBk) {
            if (callBk) {
                onClipEnd.push(callBk)
            }
        },
        onCuepoint: function(callBk) {
            if (callBk) {
                onCuepoint.push(callBk)
            }
        }
    }
};