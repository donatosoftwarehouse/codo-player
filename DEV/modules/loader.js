var Loader = function() {
    var alpha = 0;
    var step = 10;
    var error = false;
    var loadingWrap = Codo(system.DOM.container).add({
        el: "div",
        id: system.id + "-loading-wrap",
        className: system.className + "-loading-wrap",
        style: "position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: #000; opacity: 0; filter: alpha(opacity=0); visibility: hidden;"
    });
    var imgW, imgH, loadingImg;
    var img = new Image();
    img.src = (l) ? system.settings.loader : logoSrc;
    img.onload = function() {
        imgW = img.width;
        imgH = img.height;
        loadingImg = Codo(loadingWrap).add({
            el: "img",
            src: img.src,
            style: "position: absolute; top: " + (system.settings.currentHeight / 2 - imgH / 2) + "px; left: " + (system.settings.currentWidth / 2 - imgW / 2) + "px;"
        });
    }
    img.onerror = function() {
        error = true;
        img = null;
        loadingImg = Codo(loadingImg).remove();
    }
    var timer;
    return {
        getImage: function() {
            return img;
        },
        resize: function(w, h) {
            if (loadingImg) {
                loadingImg.style.top = h / 2 - imgH / 2 + "px";
                loadingImg.style.left = w / 2 - imgW / 2 + "px";
            }
        },
        on: function() {
            clearInterval(timer);
            if (overlay && overlay.getState()) overlay.off();
            var max = 80;
            loadingWrap.style.opacity = alpha / 100;
            loadingWrap.style.filter = "alpha(opacity=" + alpha + ")";
            loadingWrap.style.visibility = "visible";
            timer = setInterval(function() {
                if (alpha < max) {
                    alpha += step;
                    loadingWrap.style.opacity = alpha / 100;
                    loadingWrap.style.filter = "alpha(opacity=" + alpha + ")"
                } else {
                    loadingWrap.style.opacity = max / 100;
                    loadingWrap.style.filter = "alpha(opacity=" + max + ")"
                    clearInterval(timer);
                }
            }, 20)
        },
        off: function(param) {
            clearInterval(timer);
            if (param == "cover") {
                if (overlay && !overlay.getState()) overlay.on();
            }
            var max = 0;
            loadingWrap.style.opacity = alpha / 100;
            loadingWrap.style.filter = "alpha(opacity=" + alpha + ")";
            timer = setInterval(function() {
                if (alpha > max) {
                    alpha -= step;
                    loadingWrap.style.opacity = alpha / 100;
                    loadingWrap.style.filter = "alpha(opacity=" + alpha + ")"
                } else {
                    loadingWrap.style.opacity = max / 100;
                    loadingWrap.style.filter = "alpha(opacity=" + max + ")"
                    loadingWrap.style.visibility = "hidden";
                    clearInterval(timer);
                }
            }, 20)
        }
    }
};