module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        concat: {
            dist: {
                src: ['modules/Codo.js', 'modules/CodoPlayerTOP.js', 'modules/system.js', 'modules/controller.js', 'modules/playlist.js', 'modules/controls.js', 'modules/loader.js', 'modules/overlay.js', 'modules/error.js', 'modules/util.js', 'modules/fullscreen.js', 'modules/platform/html5.js', 'modules/platform/swf.js', 'modules/platform/yt.js', 'modules/CodoPlayerBOTTOM.js'],
                dest: 'CodoPlayer/CodoPlayer.js'
            },
        },

        replace: {
            header: {
                src: ["Website/header.php"],
                dest: "../",
                replacements: [{
                    from: "{{version}}",
                    to: "<%= pkg.version %>"
                }]
            },
            download:{
                src: ["Website/download/index.php"],
                dest: "../download/CodoPlayerFree/index.php",
                replacements: [{
                    from: "{{version}}",
                    to: "<%= pkg.version %>"
                }]
            },
            createpkg: {
                src: ["Website/createpkg/index.php"],
                dest: "../account/res/php/createpkg/index.php",
                replacements: [{
                    from: "{{version}}",
                    to: "<%= pkg.version %>"
                }]
            },
            free: {
                src: ["CodoPlayer/CodoPlayer.js"],
                dest: "CodoPlayerFree/CodoPlayer.js",
                replacements: [{
                    from: "{{name}}",
                    to: "<%= pkg.name %>"
                }, {
                    from: "{{kind}}",
                    to: "<%= pkg.free %>"
                }, {
                    from: "{{version}}",
                    to: "<%= pkg.version %>"
                }, {
                    from: "function(){var h=Codo().h(),p=Codo().p();var l=[[108,111,99,97,108,104,111,115,116],[49,50,55,46,48,46,48,46,49],[\"{{DOMAIN_MARKER}}\"],[102,105,108,101]],ls=[\"\",\"\",\"\",\"\"];for(var i=0;i<l[0].length;i++)ls[0]+=String.fromCharCode(l[0][i]);for(var i=0;i<l[1].length;i++)ls[1]+=String.fromCharCode(l[1][i]);for(var i=0;i<l[2].length;i++)ls[2]+=String.fromCharCode(l[2][i]);for(var i=0;i<l[3].length;i++)ls[3]+=String.fromCharCode(l[3][i]);return h.search(ls[0])>-1||(h.search(ls[1])>-1||(h.search(ls[2])>-1||p.search(ls[3])>-1))?true:false}();",
                    to: "false;"
                }]
            },
            licenseFree: {
                src: ["CodoPlayer/LICENSEFREE.txt"],
                dest: "CodoPlayerFree/LICENSE.txt",
                replacements: [{
                    from: "{{version}}",
                    to: "<%= pkg.version %>"
                }]
            },
            pro: {
                src: ["CodoPlayer/CodoPlayer.js"],
                dest: "CodoPlayerPro/CodoPlayer.js",
                replacements: [{
                    from: "{{name}}",
                    to: "<%= pkg.name %>"
                }, {
                    from: "{{kind}}",
                    to: "<%= pkg.pro %>"
                }, {
                    from: "{{version}}",
                    to: "<%= pkg.version %>"
                }]
            },
            licensePro: {
                src: ["CodoPlayer/LICENSEPRO.txt"],
                dest: "CodoPlayerPro/LICENSE.txt",
                replacements: [{
                    from: "{{version}}",
                    to: "<%= pkg.version %>"
                }]
            },
        },

        uglify: {
            free: {
                options: {
                    banner: "/*\n    Codo Player Free <%= pkg.version %>\n    codoplayer.com\n\n    Codo Player Free is free software: you can redistribute it and/or modify it\n    under the terms of the GNU General Public License as published by the Free Software Foundation,\n    either version 3 of the License, or (at your option) any later version.\n    You should have received a copy of the GNU General Public License along with Codo Player.\n    If not, see <http://www.codoplayer.com/policy/license/free>.\n\n    Date: <%= grunt.template.today(\"dd mm yyyy\") %>\n\n    Copyright (C) Donato Software House\n*/\n\n"
                },
                src: "CodoPlayerFree/CodoPlayer.js",
                dest: "CodoPlayerFree/CodoPlayer.js"
            },
            pro: {
                options: {
                    banner: "/*\n    Codo Player Pro <%= pkg.version %>\n    codoplayer.com\n\n    Licensed domain: \"{{LICENSED_DOMAIN}}\"\n\n    You should have received a copy of Software License Agreement along with Codo Player.\n    If not, see <http://www.codoplayer.com/policy/license/pro>.\n\n    Date: <%= grunt.template.today(\"dd mm yyyy\") %>\n\n    Copyright (C) Donato Software House\n*/\n\n"
                },
                src: "CodoPlayerPro/CodoPlayer.js",
                dest: "CodoPlayerPro/CodoPlayer.js"
            }
        },

        compress: {
            main: {
                options: {
                    archive: "../download/CodoPlayerFree/CodoPlayerFree-<%= pkg.version %>.zip"
                },
                files: [{
                    src: ["**"],
                    dest: "CodoPlayerFree-<%= pkg.version %>/",
                    cwd: "CodoPlayerFree/",
                    expand: true,
                }]
            }
        },

        copy: {
            main: {
                files: [{
                    expand: true,
                    cwd: "CodoPlayerPro/",
                    src: ["**"],
                    dest: "../account/res/php/createpkg/CodoPlayerPro-<%= pkg.version %>/"
                }]
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks("grunt-text-replace");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-compress");
    grunt.loadNpmTasks("grunt-contrib-copy");

    grunt.registerTask("default", ["concat", "replace", "uglify", "compress", "copy"]);
};