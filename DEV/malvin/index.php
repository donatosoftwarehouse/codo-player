<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Codo Player</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <!-- <link href="" rel="stylesheet"> -->
    </head>
    <body>

        <h1>Codo Player</h1>

        <script src="CodoPlayerFree/CodoPlayer.js" type="text/javascript"></script>
        <script type="text/javascript">

            CodoPlayer("../../media/landing.mp4", {
                width: 400,
                height: 225,
                controls: {
                    seeking: false,
                    fullscreen: false,
                    volume: false
                },
                onReady: function(player) {
                    player.media.onEnd(function() {
                        document.getElementById("test-btn").innerHTML = "Mark Complete";
                    })
                }
            });

        </script>

        <br>

        <h3 id="test-btn"></h3>

    </body>
</html>