-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 31, 2014 at 10:37 AM
-- Server version: 5.5.37-log
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `donatoso_codoplayer`
--

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `domains` int(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `domains`
--

DROP TABLE IF EXISTS `domains`;
CREATE TABLE IF NOT EXISTS `domains` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `orderref` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `domains`
--

INSERT INTO `domains` (`id`, `user`, `name`, `orderref`, `time`) VALUES
(13, 'mehrraum', 'becode.com', '92J462713P670490F', '2013-05-08 09:05:44'),
(18, 'dankster', 'duncanshiels.net', '6VG34350EW068111L', '2013-12-06 02:37:06'),
(24, 'donatas', 'codoplayer.com', '08B01917W4421652N', '2014-01-26 22:00:45'),
(26, 'donatas', 'codoplayer.com', 'test_order_ref', '2014-03-09 19:38:16'),
(27, 'donatas', 'videocloud.com', 'test_order_ref', '2014-03-10 14:22:36'),
(28, 'dev8ent', 'perfectgonzo.com', '0C486956WN215553S', '2014-03-10 15:53:38'),
(29, 'dev8ent', 'allinternal.com', '0C486956WN215553S', '2014-03-10 15:53:42'),
(30, 'dev8ent', 'asstraffic.com', '0C486956WN215553S', '2014-03-10 15:53:45'),
(31, 'dev8ent', 'cumforcover.com', '0C486956WN215553S', '2014-03-10 15:53:48'),
(32, 'dev8ent', 'fistflush.com', '0C486956WN215553S', '2014-03-10 15:53:51'),
(33, 'dev8ent', 'givemepink.com', '0C486956WN215553S', '2014-03-10 15:53:54'),
(34, 'dev8ent', 'milfthing.com', '0C486956WN215553S', '2014-03-10 15:53:56'),
(35, 'dev8ent', 'primecups.com', '0C486956WN215553S', '2014-03-10 15:53:59'),
(36, 'dev8ent', 'spermswap.com', '0C486956WN215553S', '2014-03-10 15:54:01'),
(37, 'dev8ent', 'tamedteens.com', '0C486956WN215553S', '2014-03-10 15:54:04'),
(40, 'ivolanen', 'volanen.net', '27480586J4385803L', '2014-05-31 13:38:03');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `orderref` varchar(200) NOT NULL,
  `status` varchar(255) NOT NULL,
  `product` varchar(200) NOT NULL,
  `version` varchar(200) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `price` int(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `orderref`, `status`, `product`, `version`, `quantity`, `price`, `currency`, `email`, `time`, `user`) VALUES
(40, '2TG05827UB289905X', 'Completed', 'Codo Player Pro', '1.0', '1', 55, 'USD', 'jcerlenizza@gmail.com', '2013-05-05 00:58:54', 'flacocerle'),
(41, '92J462713P670490F', 'Completed', 'Codo Player Pro', '1.0', '1', 55, 'USD', 'm.mummel@gmx.net', '2013-05-08 07:37:54', 'mehrraum'),
(42, 'test_order_ref', 'Completed', 'Codo Player Pro', '1.1', '5', 195, '$', 'd.cereska@outlook.com', '2013-07-27 10:15:31', 'donatas'),
(43, '6VG34350EW068111L', 'Completed', 'Codo Player Pro', '1.6', '1', 65, 'USD', 'me@duncanshiels.net', '2013-12-04 19:36:20', 'dankster'),
(45, '08B01917W4421652N', 'Completed', 'Codo Player VAST', '1.0', '1', 325, 'USD', 'donato_c@hotmail.co.uk', '2014-01-26 22:00:20', 'donatas'),
(47, '0C486956WN215553S', 'Completed', 'Codo Player Pro', '1.8', '10', 375, 'USD', 'eman.inet+ppaxp@gmail.com', '2014-03-09 19:20:57', 'dev8ent'),
(48, '89161949J4714094V', 'Refunded', 'Codo Player Pro', '1.8', '1', -75, 'USD', 'eman.inet+ppaxp@gmail.com', '2014-03-17 10:10:59', 'dev8ent'),
(49, '27480586J4385803L', 'Completed', 'Codo Player Pro', '2.0', '1', 75, 'USD', 'ivolanen@saunalahti.fi', '2014-05-31 13:35:48', 'ivolanen');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `user` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `company` varchar(200) NOT NULL,
  `date_created` date NOT NULL,
  `last_signin` date NOT NULL,
  `times_used` int(255) NOT NULL DEFAULT '0',
  `active` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company` (`company`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=112 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ident`, `user`, `password`, `email`, `company`, `date_created`, `last_signin`, `times_used`, `active`) VALUES
(48, 'OnLZwqpMbfv0HQtG4Uv0yNulL0R2N7E3MpVDafpc', 'donatas', 'dbf317b85c513ec7d4067e66c32a186d', 'd.cereska@donatosoftwarehouse.com', '', '2013-07-21', '2014-05-31', 131, '1'),
(34, '6lm9SvBNS5hQdYFAItuLCSeujiBFkeDwEpvwGXe', 'comocatman', '51b3653a5370c5a3850bf3b3ad3c0f3a', 'comocatman@larrycatherder.com', '', '2013-04-18', '2013-04-18', 2, '1'),
(35, 'doBvLJs5WitmJ6bUB18Nm9bHITotaTXEcy0nCNky', 'flacocerle', '8f9d03b2e2ef3f6450110298b4842ca1', 'jcerlenizza@gmail.com', '', '2013-05-04', '2013-05-04', 2, '1'),
(36, '6aQDyet8Ovd7iXYprHAj0RzJlV70IT7fUoiNT5MC', 'mehrraum', '2f912abcafaebc921246c01c8b3bca3a', 'komm@mehrraum.net', '', '2013-05-08', '2013-05-08', 1, '1'),
(37, '6cuDdS9jHDvaEZizlOLBiLxzcPJ7pAnnDcqHVQq5', 'tyler1webdev', 'f3c2a3c799929c99fc815a1a50874674', 'tyler.andersen@henryschein.com', '', '2013-05-10', '0000-00-00', 0, '1'),
(38, 'hnEk8qHyxmKlroHdtXAQpRCdHE6UM73zcHyTOYM', 'adamhurst', '5cf3478abe1d381ea5a90ab7bc74747e', 'adamhurst@gmail.com', '', '2013-05-23', '2013-05-23', 2, '1'),
(39, 'ET79CL0gic4zjiuMcK2E7T2hA4zQkHu8ftaRohz', 'chandna', 'd8742283b9d86ba620634a138e774c59', 'info@c21digital.com', '', '2013-06-13', '2013-06-13', 1, '1'),
(40, 'fmmEmfYhJjGAsnOWhVdWYHdEzOZ8bHXhtelaKeIt', 'TrashChecker', '02f8168e9cebf5cc02e3ef9e5978a9d2', 'trash.checker@gmail.com', '', '2013-06-19', '2013-06-19', 1, '1'),
(41, '51b7KmoTLqsd56ifi2sqbyzKfDjcMZH17rImFT', '12Domanick', 'a9ee44ee8a31eec2971e4fbab14535ea', 'Strickland@Domanick.net', '', '2013-07-02', '2014-01-19', 5, '1'),
(42, '9ppngetysazb7mKKG6MNbhEkqGEzZxQYIAgoe0hX', 'Domanick', 'f32865cd8fc712b639d2856b8b8d0202', 'dj@domanick.net', '', '2013-07-05', '0000-00-00', 0, '1'),
(45, 'TWsD8XkQPZKdojKelHiaelnz4F0v8HmS6Av4xBmH', 'aanixi', '87e66d3b87862a9ca51aa526cf0b01c2', 'anna.katajainen@hitfactory.fi', '', '2013-07-18', '2013-07-18', 1, '1'),
(44, 'FcrYajxq7aA1tG3FYdkct4FyMlXVVxx00jT2tlMr', 'hogehoge', 'cd2afa6edc2845d934899739eaa3e0ca', 'registonly55@gmail.com', '', '2013-07-18', '2013-07-18', 1, '1'),
(46, 'liqKhEWy5QvDeD70cnr15iv9L29tcBBojSZrWpLt', 'jeffersonmeplo', '493993b6a791c49aae5ce485cd85face', 'jerfferson.guilherme1@gmail.com', '', '2013-07-20', '2013-07-20', 1, '1'),
(47, 'Iyue6IjWFpVHBj5XeT94QHVpETcoanuiHKneSX2x', 'kon137', '4be86d2120f1a09de67fb1c22e38e97a', 'pedro.1370@gmail.com', '', '2013-07-20', '2013-07-20', 1, '1'),
(62, 'wrEs5ITyPLfzsu0pW22GRfxneqYeZd3lIoiQCb', 'jonukas', 'dbf317b85c513ec7d4067e66c32a186d', 'support@codoplayer.com', '', '2013-08-18', '2014-05-27', 7, '1'),
(63, '50pzebQNWNT2vejjGR4VHPUdcg6I1D0Y6KsCMDpc', 'ayxano', 'a0c8451b1423cd32aea6b6cee587e0ed', 'ayxano@gmail.com', '', '2013-08-18', '2013-08-18', 1, '1'),
(64, 'bG7vOQ5DZQ94Hshaaar6xsm7k41aClmErLza2XEu', 'provannet', 'b60f86bbd8b16d3e30a2f1676cb0be77', 'provannet@gmail.com', '', '2013-08-24', '2013-08-24', 1, '1'),
(65, '0lILD02gUBM2i73YAKgCzgdfa40okXJWAUz0pBo', 'sachinbansal', '69c52176fbb97d4dc28f1beddbc6f66d', 'sachinbansal4545@gmail.com', '', '2013-09-15', '0000-00-00', 0, '1'),
(66, '8NYTmp3TXB9ltS9tvShEAYGQjupbZoorc2C8Pz', 'mosaso', '588263ec42da28a429a53463011efede', 'motocha44@gmail.com', '', '2013-09-17', '2013-09-17', 1, '1'),
(67, 'oih9jdfSBp4IDxXwC8EIQmt40GwuvspF3XEf2jx4', 'theycallmetune', 'bd0ce69b841db559539c999f39bee9c0', 'theycallmetune@gmail.com', '', '2013-09-20', '0000-00-00', 0, '1'),
(68, 'xVRIxqDjmS9NuFm0cLF6XCjlY1XPYFPq0BZsr448', 'tylercampbell', '08851f17be9e10fce11ae83727f181c4', 'tyler.campbell@gmail.com', '', '2013-09-23', '2013-09-23', 1, '1'),
(69, 'cuP5qHmh6tuMYNrRiIg8GzkMVBzfif7L0NiluvtS', 'azaryl', 'a2720e017f1b6434136a60ec769e1842', 'cyril.mennetrier@azaryl.com', '', '2013-09-26', '2013-09-26', 1, '1'),
(70, 'tHpiZ7c41ENs0wrn5e2OSMzHih2kFvIZ5YCWwFTr', 'prasad40', '59476060270d44a9c830aeb59c73eba2', 'prasadforsw@gmail.com', '', '2013-09-26', '2013-09-26', 1, '1'),
(71, 'Tvh137DAUnm3R0tER5JzFcTnuJ8OorOcIW6EUaeJ', 'colorey', '4b16be6d5a006ab1572a30f122a1d1c5', 'dosenes@live.com', '', '2013-10-12', '2013-11-30', 2, '1'),
(72, 'SPac6JEoip53a5oQ9j6vMENqhPeljK8ZLBXUFG6', 'gerard', '784f9804068983e58cdc90ad46e76eac', 'gerard@schoolwebdesign.net', '', '2013-10-24', '2013-10-24', 1, '1'),
(73, 'OTIT7vWeS504AZ1CLRVq7N0peqRs1sOo2bmRxrE', 'vishalbeersye', '8e92408829f654489473bc99de240d8d', 'vish@onlineoutsourcing.net', '', '2013-10-29', '2013-10-29', 1, '1'),
(74, 'W7g24JUgptkjB4bEjCOE0V7OBEITM5jKdFgNzn6', 'Jokerz', 'a10c7ece2e66f68e7ff99e92ae5e0eea', 'www.joker.96@gmail.com', '', '2013-11-07', '2013-11-07', 1, '1'),
(75, 'vn4SurVWBrKbAIyIeZg6pPSdabI2L8JhR8zHu2', 'VictorNunez', '9ca9d6394687c9b04496a7b4e0ef34cd', 'webmaster@uriangato.gob.mx', '', '2013-11-19', '2013-11-19', 1, '1'),
(76, 'vzPj4fmq6WDLWG7Y6oMTiK8Oo7kmJWQPJmuv6s', 'dankster', '09f2e3a312fa551684fa0a54da2f3444', 'me@duncanshiels.net', '', '2013-12-04', '2013-12-05', 9, '1'),
(77, '6oPyoK6g4erEwmP96zITMdjj38ZOsOMC7Fq6poe', 'rmucchielli', 'b6b12289c72febd2bd5c893d0eeb120b', 'raphael.mucchielli@enov-formation.com', '', '2013-12-05', '2014-02-12', 2, '1'),
(86, 'DNt5yjEGELfI81Kwo5YXaoplbfOvfEUiRIGKrKqW', 'sdmfiomdfiom', '4e5f610fae9b5c04262fd1190a9e8590', 'asfm@sdkfm.com', '', '2013-12-13', '0000-00-00', 0, '1'),
(85, '7tMh0oSR8iv5IEPjYKU1q9VKHIsVeztyg2TVOFU', 'DJboutit', '938de3f239920fbbe4885be83634caca', 'DJboutit@Inbox.com', '', '2013-12-11', '2013-12-11', 1, '1'),
(87, 'SQ8NjHywZeEDTis64NvG5mpqvYF4kOD69CkJJdaD', 'dukewa1', 'c394532493a1052abcac67c11ccab1a0', 'dukewa4658@gmail.com', '', '2014-01-02', '2014-01-02', 1, '1'),
(88, 'LiM5ssCBRifA7Q7N7Edg28wYyOgTokZ0TGXGyzhk', 'pear78', '6d0596eba45cb3e3589b6eb2f0e065b5', 'ruttakorn78@gmail.com', '', '2014-01-04', '2014-01-04', 1, '1'),
(89, 'ZAws1a8fQdANzVKz5ECFkavRJ4Dy7pLXQckLdJRt', 'anlequoc', 'f27320586982f1097a2a5110cdff0669', 'an.lequoc@yahoo.com', '', '2014-01-06', '2014-01-06', 1, '1'),
(90, '4fobddbUQIFzMsfWFhPXubqIPpgMtpuqvEszhUOY', 'ingokunzi', '0da7cda51982d1e1e97dfb2edbe9e25b', 'tandu@tandu.de', '', '2014-01-13', '2014-01-13', 1, '1'),
(91, '3uHNCuu4P1YajJ4ZV4ChgPSeZiPaUsqqIx8kNTh5', 'pramod', 'd2d6ec0560ae87baee472e9912f86240', 'pramodchakravarti@nesoteos.com', '', '2014-01-21', '2014-01-21', 1, '1'),
(92, 'g48wEqO4J4Zh5osUNnfBMd8mfbfpydwvCOzT4W4', 'nnedme', '581e3f2aeb367cdbced47ff90623a3e2', 'denn@itg5.com', '', '2014-02-04', '2014-02-04', 1, '1'),
(94, 'JsGnHsyCPgAEaGBt7Ui5Mp8x3i3TL4YUP8cWr2yB', 'mandarin55', 'f71698d29a8bba9001bd170d36294162', 'mandarinislove@gmail.com', '', '2014-02-07', '2014-02-09', 4, '1'),
(95, 'HRSSJJ1ONodoUJALjTE5oKwtLEIIES9ldR8m1Z2i', 'searchrbaker', '0cc6883d624b147828cdca482dbca066', 'grahatha@gmail.com', '', '2014-02-11', '2014-02-11', 1, '1'),
(96, 'f4eKCoBLcZ1DBFk6ioDCKzHQXAwb9wh23HG7vHJ', 'bortolazzie', '59c1d32cfd8d8bfbe7641d7a34f55606', 'bortolazzi.e@digitalcoat.org', '', '2014-02-20', '0000-00-00', 0, '1'),
(97, 'IviGnY9fzMXzN8O2iHfWkUnDtE9GsD02ZdbH3BmW', 'dev8ent', 'ec9b3d12ebcdc361a41ba836db13f859', 'info@dev8entertainment.com', '', '2014-03-09', '2014-03-10', 5, '1'),
(98, 'sY0wCL7i8ztiUEiccJ1rIFjGkAZAtpCgi47kI3tH', 'serikatil724', '0b8ecae8d45571a0bd917047744b0c03', 'kursattaner@outlook.com', '', '2014-03-26', '2014-03-26', 1, '1'),
(99, 'oS8SjJuL1W8aS9yVj50T24cJ9i3G8hVRgh2Q5gl', 'aayansaleem', '6f6995096e269eba9a4d558a9b3e5105', 'aayansaleem@gmail.com', '', '2014-03-27', '0000-00-00', 0, '1'),
(100, 'DaonJckXttQGdgRVHu0yk3cB9CLlxlv2MFJvhtN2', 'h3r0k0', '30e87949d264bac399573716defa43d0', 'h3r0k0@gmail.com', '', '2014-04-06', '0000-00-00', 0, '1'),
(101, 'DfBX4VN9RaAyuhiIqGTv9gNQ8shatzKwAlO5b34s', 'lunaweb', 'df9bb6d15e4faea652b50ee1ce20a503', 'support@lunaweb.com', '', '2014-04-07', '2014-04-07', 1, '1'),
(102, '8COAVOVoOvtxaETqBN8qA0b39x2OSWcRYRROH5O', 'FinetLtd', 'e3978217cfd95037b099696dc26b7431', 'gennielam@finet.com.hk', '', '2014-04-10', '2014-04-10', 1, '1'),
(103, 'E2e62EVy2q8bOYTBRl3jfSSlY0q0t5oYYTvUXLNT', 'mariussmit', '633e4d532ce4e96a48ecc5cd58bd771e', 'marius@fireflex.co.za', '', '2014-05-01', '2014-05-01', 1, '1'),
(104, 'nfZ95kDTugd9H986SzgLveX2pf4lpozviZsT5gI', 'naweekes', 'ad5fe1a0cece833228f5bf6d6f54fbc8', 'naweekes@gmail.com', '', '2014-05-04', '2014-05-04', 1, '1'),
(105, 'N3lsX5p1lPULm2uED7okXN2b3kE9mYf0urnMo6h1', 'SkullClever', '8b7c9d87f2d9a628f0e5039a1717dbd5', 'xhertz@bk.ru', '', '2014-05-14', '2014-05-14', 1, '1'),
(106, 'tRMHx5Wow0vYwbjFQjLdQRpuKhCq4IeOuMgSDVJ', 'vjoy10', '5933f64f444e492da85a69f156d3713d', 'binfahblogs@gmail.com', '', '2014-05-20', '2014-05-20', 1, '1'),
(107, 'JelGKiVFOiY3rUpnuFoTuKBqseSHYvJb0uhF66KM', 'mokman', '2eceac5100a5cb108c169f1c0470d7ba', 'mokmxn@gmail.com', '', '2014-05-21', '2014-05-21', 1, '1'),
(108, 'byXdh5MnBO6xPM6GGd8PpPMIqFy0bSrF0ReX7', 'FAILBOY', '7cf561a552844f394a34414577a08f6a', 'failboy@live.com', '', '2014-05-22', '0000-00-00', 0, '1'),
(109, 'JuSz37jJsSTQWjgLFRPYJ7d8SoKb9bCiWUDSsm3f', 'lore3578', '5e2131fe737c590ef638f05f8ed3b623', 'a110879@lazarocardenas.edu.mx', '', '2014-05-22', '2014-05-22', 1, '1'),
(110, 'KaedCuSvhztLx1umkFdmPb2DHDEUIeoSFTWHIJ5Q', 'Charles', '162309ef3bcf42682f062f7bef43d335', 'charles@mpoweredwebs.com', '', '2014-05-25', '2014-05-25', 1, '1'),
(111, 'JBB32mdSjPk9qoVJWfmDbBgLyJy04Yd7zAZHdM', 'ivolanen', '749c1a48804990c44f5c98c4f026d6a0', 'ivolanen@netti.fi', '', '2014-05-31', '2014-05-31', 2, '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
